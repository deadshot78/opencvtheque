<?php
define( 'ABSPATH', dirname(__FILE__) );

if(!file_exists(ABSPATH.'/conf/_connect.php')){
    ?>
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <head>
            <title>Opencvtheque NG<?php include_once 'conf/version.txt';?></title>
        </head>
        <body>
            le fichier de connexion à la base n'exite pas merci de le créer en
            copiant le fichier blank_connect.php --> _connect.php et de renseigner
            les différentes valeurs
        </body>
    </html>
<?php
exit();
}
include(ABSPATH.'/conf/_connect.php');

session_start() ;
$_SESSION["remote"]=$_SERVER['REMOTE_ADDR'];

if (!isset($Langue)) {
    $Langue = explode(',',$_SERVER['HTTP_ACCEPT_LANGUAGE']);
    $Langue = strtolower(substr(chop($Langue[0]),0,2));
    setlocale(LC_TIME, $Langue."_".strtoupper($Langue));
    $element_page = '';
}

if(empty($Langue)){
    $Langue='fr';
}

require_once (ABSPATH.'/includes/class/_inc_function.php');
//require_once('conf/_connect.php');
require_once(ABSPATH.'/includes/langs/lang-'.$Langue.'.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
    <head>
        <title>Opencvtheque NG<?php include_once 'conf/version.txt';?></title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <link type="text/css" href="css/default.css" rel="Stylesheet" />
        <link type="text/css" href="css/jquery-ui-1.8.5.custom.css" rel="Stylesheet" />
        <link type="text/css" href="css/fvalidate.css" rel="Stylesheet" />
        <link type="text/css" href="css/ui.jqgrid.css" rel="Stylesheet" />
        <link type="text/css" href="css/jquery.ui.autocomplete.css" rel="Stylesheet" />
        <link type="text/css" href="css/offres.css" rel="Stylesheet" />
        <link type="text/css" href="css/jquery.realperson.css" rel="Stylesheet" />
        <link type="text/css" href="css/menu.css" rel="stylesheet" media="screen" />
        <link type="text/css" href="css/ui.multiselect.css" rel="Stylesheet" />
        
        <script type="text/javascript" src="includes/js/jquery-1.4.2.min.js"></script>
        <script type="text/javascript" src="includes/js/jquery-ui-1.8.5.custom.min.js"></script>
        <script type="text/javascript" src="includes/js/jquery.ui.dialog.js"></script>
        <script type="text/javascript" src="includes/js/jquery.ui.button.js"></script>
        <script type="text/javascript" src="includes/js/jquery.ui.autocomplete.js"></script>
        <script type="text/javascript" src="includes/js/jquery.realperson.min.js"></script>
        <script type="text/javascript" src="includes/js/jquery.maskedinput-1.2.2.min.js"></script>
        <script type="text/javascript" src="includes/js/jquery.form.js"></script>
        <script type="text/javascript" src="includes/js/ajaxupload.js"></script>
        <script type="text/javascript" src="includes/js/grid.locale-<?php echo $Langue;?>.js"></script>
        <script type="text/javascript" src="includes/js/src/grid.loader.js"></script>
        <script type="text/javascript" src="includes/js/src/grid.subgrid.js"></script>
        <script type="text/javascript" src="includes/js/jquery.fvalidate.min.js"></script>
        <script type="text/javascript" src="includes/js/jquery.download.js"></script>

        <script type="text/javascript">

            var document_path = 'documents/';

            $(document).ready(function() {
		$('#menu-tabs').tabs();
            });

/* DEBUT MENU */

            $(function(){
/*
    * Dialogue box
    */
        <?php
            include(ABSPATH.'/menu/js/dialog-form.js');
         ?>
           /*
    * Fin dialogue box
    */

/*
* Start function Tabs
*/
                $('#menu-tabs').removeClass();
                $('#menu-tabs').addClass('ui-tabs');
                $('#menu-tabs div').removeClass('ui-tabs-panel');
                $('#menu-tabs div.ui-widget-content').css({
                    'padding': '0',
                    'border-style': 'none'
                });

        /*
         * Insère le nom de la page  nommée titre page dans la zone ul du menu
         */

                var $titreText=$('#titre_page').text();
                $('<div class="page_titre">'+$titreText+'</div>').appendTo('#menu-tabs ul');
                $('#titre_page').hide();

                $('#menu-tabs ul').removeClass('ui-tabs-nav ui-corner-all');
                $('#menu-tabs ul').addClass('menu-tabs-nav');
                $('#menu-tabs ul li').removeClass('ui-widget-header');
                $('.flechelink').click(function() {

        /*
         *Obtenir l'ID du parents pour ouvrir le formulaire correspondant
         */

                    var nomlien=($(this).prev().attr("href"));
                    var nomformulaire = ($(this).children('p').text());
                    $('#'+nomformulaire+' #nomlien').val(nomlien);
                    $('#'+nomformulaire).dialog('open');
                });

                $('.documentpdf a').click(function() {

                        var lien = $(this).attr('href');
                        var destination = lien.split('/');
                        $.download(
                            document_path+'document_pdf.php',
                            {'nom_module':destination[0],
                             'nom_document':destination[1]},
                             'get'
                            )
                        return false;
                    });

                $('.btnlink a').click(function(){
        //         Récupère le lien de la page appelée
                   var lien = $(this).attr('href');
        //         Crée un tableau afin d'extraire la destinaton de la page demandée
                   var destination = lien.split('_');
                   var element_page = destination[1];

                    $.get('redirecteur.php?dest=commun_testsess&page='+element_page,function(data){
                        if(data=='0'){
                            alert('Votre session est expirée, merci de vous reconnecter !');
                            $(location).attr('href',".");
                            return false;
                        }
                    });

                    

                   $("#contenu").load('redirecteur.php?dest='+$(this).attr('href'),
                        function(){
                            $("#tabs").tabs();
                            $("input:submit, button", '').button();
                            var $titreText=$('#titre_page').text();
                            $('.page_titre').remove();
                            $('<div class="page_titre">'+$titreText+'</div>').appendTo('#menu-tabs ul');
                            $('#titre_page').hide();
                           });
                   return false;
                });

/* FIN MENU */

/* Calendrier */
                $('#date').datepicker();

/* Onglets */
                $("#tabs").tabs();

/* Boutons */
                $("input:submit, button", '').button();
                $("#radio_concours").buttonset();
                $("#userfile").button();

            });


        </script>
    </head>

    <body>


    <?php
    //print_r($_SESSION);
    //Appel du menu
    if(isset($_SESSION["connected"]) && $_SESSION["connected"]==sha1('welcome'.$_SESSION["login"])){
       require_once (ABSPATH."/menu/menu.php");
       //echo "Bienvenue ".$_SESSION["contact"]."<br/>";
        $element_page = $_SESSION["connected"];
    } elseif (file_exists(ABSPATH.'/install.php')) {
        $element_page = 'install';
    }



    ?>

    <div id="contenu">

    <?php
    if(!isset($element_page)) $element_page = '';
    switch ($element_page){

        case '':
            require_once (ABSPATH."/commun/login.php");
            break;

        case 'install':
            require_once (ABSPATH."/install.php");
            break;

        default:
            require_once (ABSPATH."/commun/welcome.php");
            break;
    }

    ?>

    </div>

    </body>

</html>