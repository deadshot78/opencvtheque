<?php
/**
* blank_connect.php Created 2 janv. 2011 at 11:29:33 by fabrice under Ocv-NG
* $Id$
*/

/* Chemin d'exécution depuis le navigateur
 * sans http:// et sans / à la fin
 */
$installpath = "localhost/opencvtheque" ;

/* Paramètres de connexion à la base de données */
$serveur 	= "localhost";
$user    	= "user";
$password  	= "pwd";
$bdd     	= "cvtheque";

/* DON'T MODIFY ANYTHING BELOW !!! */

/* Définie le chemin de stockage des sessions */
session_save_path(ABSPATH."/session");

/* Supprime le cache de session */
session_cache_limiter('nocache');

/* Configure le délai d'expiration à 30 minutes */
session_cache_expire(30);

//récupère la langue du navigateur
if (!isset($Langue)) {
    $Langue = explode(',',$_SERVER['HTTP_ACCEPT_LANGUAGE']);
    $Langue = strtolower(substr(chop($Langue[0]),0,2));
}

require_once ABSPATH.'/includes/class/_inc_function.php';
require_once ABSPATH.'/includes/langs/lang-'.$Langue.'.php';
require_once ABSPATH.'/conf/include_tables.php';
require_once ABSPATH.'/includes/class/listes.class.php';
require_once ABSPATH.'/includes/class/formulaire.class.php';
require_once ABSPATH.'/includes/class/requetes.class.php';
require_once ABSPATH.'/includes/class/mail.class.php';
require_once ABSPATH.'/includes/class/securite.class.php';

?>