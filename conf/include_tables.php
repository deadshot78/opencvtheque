<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

//Liste des tables de la cvtheque
$param["table"]["contact"]="cv_contact";
$param["table"]["entreprise"]="cv_entreprise";
$param["table"]["candidat"]="cv_candidat";

$param["table"]["offre"]="cv_offre";
$param["table"]["offre_detail"]="cv_offre_details";

$param["table"]["statistiques"]="cv_statistiques";

$param["table"]["langues"]="cv_langue";


$param["table"]["data"]="cv_data";
$param["table"]["city"]="cv_city";
$param["table"]["formation"]="cv_formation";
$param["table"]["suivreformation"]="cv_suivre_formation";
$param["table"]["experience"]="cv_experience";
$param["table"]["realiserexperience"]="cv_realiser_experience";
$param["table"]["competence"]="cv_competence";
$param["table"]["acquerircompetence"]="cv_acquerir_competence";

$param["table"]["province"]="cv_province";
$param["table"]["state"]="cv_state";
$param["table"]["country"]="cv_country";
$param["table"]["type_offre"]="cfg_type_offre";
$param["table"]["entreprises_partenaire"]="cv_entreprises_partenaire";
$param["table"]["contacts_partenaire"]="cv_contacts_partenaire";

$param["view"]["localisation"]="cv_localisation";
$param["view"]["partenaires"]="cv_partenaires";
?>
