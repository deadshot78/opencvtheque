<?php

$contact = $_SESSION['contact'];

$cnx = new actionsdata();
$cnx->connect();

$securite=new securite;
$id=$securite->verif_GetPost($_POST['id']);
$action=$securite->verif_GetPost($_POST['action']);
$type=$securite->verif_GetPost($_POST['type']);

//echo $req_select_partenaire;

?>

<script type="text/javascript">
var jours = <?php echo $param["datepicker"]["dayNamesMin"] ;?>;
var mois  =<?php echo $param["datepicker"]["monthNamesShort"] ;?>;

<?php include_once ("includes/js/form-fonctions.js"); ?>

$(document).ready(function() { 


        //$('#gestionoffreent').submit(function() {
        // inside event callbacks 'this' is the DOM element so we first
        // wrap it in a jQuery object and then invoke ajaxSubmit
        //        $(this).ajaxSubmit();

        // !!! Important !!!
        // always return false to prevent standard browser submit and page navigation
        //    return false;
        //});
    $('#offre_partenaire').click(function(){
        //alert('toto');
        $('#fldpartenaire').slideToggle("fast");
        $('#id_contact_partenaire').val(' ');
        $('.contact').val('');
    });

    if($('#id_contact_partenaire').val()==''){
        $('#fldpartenaire').hide();
    }

//});

    /*$('input:submit, button, .button', '').button();
    $("#bt_cancel").click(function(){
        $('#formulairePerso').dialog('close');
    });*/

    autocomplete_location('#id_city','#ville_offre','#cp_offre','#departement_offre','#region_offre','#pays_offre');
    autocomplete_location('#id_city_partenaire','#ville_partenaire','#cp_partenaire');
    autocomplete_partenaire('#id_contact_partenaire','#id_entreprise_partenaire','#raison_sociale_entreprise_partenaire','#nom_contact_partenaire','mix');

    $("#despossible,#entreprise_anonym,#offre_partenaire").button();
    $("#radio_genre").buttonset();
  
  initDatePicker("#date_debut_offre","dd/mm/yy",jours,mois);

});


</script>

<?php



    $form = new formulaire ();
    $result ="";

    switch($type){
        case 'entreprise':

            if ($id)
            {
                $req_select_partenaire = "SELECT * ";
                $req_select_partenaire .= "FROM ".$param['table']['entreprises_partenaire']." ";
                $req_select_partenaire .= "WHERE  id='$id' AND id_contact=".$contact;

                $res_select_partenaire = $cnx->requeteSelect($req_select_partenaire);
            }

            $result .= $form->form_init('entreprise','entreprises/maj-entreprise-partenaire.php','POST','formulaire-fiche');
            $result .= $form->creer_text('id','id','no','hidden',$id,'civilite','alphanumeric',2,60,'',1);
            $result .= $form->fieldset_new('');
            $result .= $form->creer_text('entreprise','Entreprise','yes','text',$res_select_partenaire[0]['RAISON_SOCIALE_ENTREPRISE'],'civilite','alpha',2,60,'',2);
            $result .= $form->fieldset_end();
            $result .= $form->fieldset_new('Coordonnées');
            $result .= $form->creer_text('adresse','Adresse','no','text',$res_select_partenaire[0]['adresse'],'civilite','alphanumeric',2,60,'',3);
            $result .= $form->creer_text('adresse2','.','no','text',$res_select_partenaire[0]['adresse2'],'civilite','alphanumeric',2,60,'',4);
            $result .= $form->creer_text('cp','Code Postal','no','text',$res_select_partenaire[0]['cp'],'civilite','alphanumeric',2,60,'',5);
            $result .= $form->creer_text('ville','Ville','no','text',$res_select_partenaire[0]['ville'],'civilite','alphanumeric',2,60,'',6);
            $result .= $form->creer_text('tel','Téléphone','no','text',$res_select_partenaire[0]['tel'],'civilite','alphanumeric',2,60,'',7);
            $result .= $form->creer_text('fax','Fax','no','text',$res_select_partenaire[0]['fax'],'civilite','alphanumeric',2,60,'',8);
            $result .= $form->creer_text('email','Email','no','text',$res_select_partenaire[0]['email'],'civilite','alphanumeric',2,60,'',9);
            $result .= $form->creer_text('site_web','Site web','no','text',$res_select_partenaire[0]['site_web'],'civilite','alphanumeric',2,60,'',10);

            $result .= $form->fieldset_end();

            $result .= "<br/>";

            $result .= $form->fieldset_new('');
            $Langue = "fr";
            $value = ABSPATH."includes/listes/sel-secteurs-".$Langue.".xml";
            $result .= $form->creer_select('secteur_activite_entreprise','Secteur d\'activité','no',$value,TRUE,$res_select_partenaire['secteur_activite_entreprise'],'','',11);

            $value=$cnx->select_liste('cfg_effectif',"id","nom",'',false);
            $result .= $form->creer_select('ideffectif','Effectif','no',$value,TRUE,$res_select_partenaire[0]['ideffectif'],200,12);


            $result .= $form->creer_textarea('description','Description de l\'entreprise','commentaires',$res_select_partenaire[0]['description'],'',2,50,13);
            $result .= $form->creer_text('naf','NAF','no','text',$res_select_partenaire[0]['naf'],'civilite','alphanumeric',2,60,'',14);
            $result .= $form->fieldset_end();


            break;
        case 'contact':

            if ($id)
            {
                $req_select_partenaire = "SELECT * ";
                $req_select_partenaire .= "FROM ".$param['table']['contacts_partenaire']." ";
                $req_select_partenaire .= "WHERE  id_contact_partenaire='$id' AND id_contact=".$contact;

                $res_select_partenaire = $cnx->requeteSelect($req_select_partenaire);
            }

            $result .= $form->form_init('entreprise','entreprises/maj-contact-partenaire.php','POST','formulaire-fiche');
            $result .= $form->fieldset_new('');
            $value=$param['civilite'];
            $result .= $form->creer_radio('genre','Genre','no',$value,$res_select_partenaire[0]['CIVILITE_CONTACT'],'genre','radio','radio',3);
            $result .= $form->creer_text('nom_contact','Nom','yes','text',$res_select_partenaire[0]['NOM_CONTACT'],'civilite','alpha',2,60,'',4);
            $result .= $form->creer_text('prenom_contact','Prénom','no','text',$res_select_partenaire[0]['PRENOM_CONTACT'],'civilite','alpha',2,60,'',5);
            $result .= $form->creer_text('fonction','Fonction','no','text',$res_select_partenaire[0]['FONCTION_CONTACT'],'civilite','alpha',2,60,'',5);
            $result .= $form->creer_text('adresse1','Adresse','no','text',$res_select_partenaire[0]['ADRESSE1_CONTACT'],'civilite','alpha',2,60,'',2);
            $result .= $form->creer_text('adresse2','.','no','text',$res_select_partenaire[0]['ADRESSE2_CONTACT'],'civilite',2,60,'',14);
            $result .= $form->creer_text('cp','Code Postal','no','text',$res_select_partenaire[0]['CP_CONTACT'],'civilite',2,60,'',14);
            $result .= $form->creer_text('ville','Ville','no','text',$res_select_partenaire[0]['VILLE_CONTACT'],'civilite',2,60,'',14);
            $result .= $form->creer_text('pays','Pays','no','text',$res_select_partenaire[0]['PAYS_CONTACT'],'civilite',2,60,'',14);
            $result .= $form->creer_text('tel','Téléphone','no','text',$res_select_partenaire[0]['TEL_CONTACT'],'civilite','alpha',2,60,'',2);
            $result .= $form->creer_text('portable','Portable','no','text',$res_select_partenaire[0]['GSM_CONTACT'],'civilite','alpha',2,60,'',2);
            $result .= $form->creer_text('fax','Fax','no','text',$res_select_partenaire[0]['FAX_CONTACT'],'civilite','alpha',2,60,'',2);
            $result .= $form->creer_text('email','Email','no','text',$res_select_partenaire[0]['EMAIL_CONTACT'],'civilite','alphanumeric',2,60,'',2);

            break;
    }

    $result .= $form->creer_bouton($param["action"]["cancel"],'bt_cancel','button','button');
    $result .= $form->creer_bouton($param["action"][$action]);

    $result .= $form->form_end();

echo $result;



?>





