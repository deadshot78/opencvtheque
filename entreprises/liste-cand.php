<h2 id="titre_page">Contacts</h2>
<input id="pagelink" type="hidden" value="entreprises candidat type_contact bouton"/> <!-- Répertoire table clef bouton-->

<script type="text/javascript">

    var largeurgrid = window.innerWidth * 0.95;
    /*var sub_navgrid_options = '';
    var sort_order = 'desc';
    var sort_subgrid = 'id';
    var show_subgrid = false;*/
    
// Nom des colonnes en haut du tableau
    var noms_colonnes = ['Civilité','Nom','Prénom','Fonction','Tél','Port','Email','Ville','Département','Région','Pays'];

// contenu des colonnes
    var modele_colonnes = [
                           {name:'civilite', index:'civilite', align:'left', editable:false, sortable:true, width:50},
                           {name:'nom', index:'nom', align:'left', editable:false, sortable:false, width:40},
                           {name:'prenom', index:'prenom', align:'left', editable:false, sortable:false, width:40},
                           {name:'fonction', index:'fonction', align:'left', editable:false, sortable:false, width:40},
                           {name:'tel', index:'tel', align:'left', editable:false, sortable:false, search:false,width:30},
                           {name:'gsm', index:'gsm', align:'left', editable:false, sortable:false, search:false,width:30},
                           {name:'email', index:'email', align:'left', editable:false, sortable:false, search:false,width:50},
                           {name:'ville', index:'ville', align:'left', editable:false, sortable:false, width:20},
                           {name:'departement', index:'departement', align:'left', editable:false, sortable:false, width:20},
                           {name:'region', index:'region', align:'left', editable:false, sortable:false, width:20},
                           {name:'pays', index:'pays', align:'left', editable:false, sortable:false, width:20}
                           ];
var navgrid_options = {view:true,edit:false,add:false,del:false};


<?php

include_once (ABSPATH.'includes/js/fonctionsjs.js');
?>

</script>

<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
include_once (ABSPATH.'commun/liste.php');
?>
<div id="formulairePerso">

</div>
