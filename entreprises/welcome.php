<?php
/*
* welcome.php Created 11 févr. 2011 at 11:57:03 by flenoble under Ocv-NG
* $Id$
*/

?>

<script src="includes/js/jquery.jqplot.min.js" type="text/javascript"></script>
<script src="includes/js/jqplot.dateAxisRenderer.min.js" type="text/javascript"></script>
<script src="includes/js/jqplot.canvasTextRenderer.min.js" type="text/javascript"></script>
<script src="includes/js/jqplot.canvasAxisTickRenderer.min.js" type="text/javascript"></script>
<script src="includes/js/jqplot.highlighter.min.js" type="text/javascript"></script>
<script src="includes/js/jqplot.cursor.min.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="css/jquery.jqplot.css" />


<script type="text/javascript">
    <?php
        include_once ('js/fonction_welcome.js');
    ?>
</script>

<div id="boxinfo">
    <div id="statut" class="roundbox"><h1>Statut</h1></div>
    <div id="statistiques" class="roundbox">
        <h1>Statistiques</h1>
        <div id="chartcontainer1" style="float: left; width: 48%; height: 200px"></div>
        <div id="chartcontainer2" style="float: left; width: 48%; height: 200px"></div>
    </div>
</div>
<br/>
<div id="candidatures" class="roundbox"><h1>Mes dernières candidatures</h1></div>
