<?php

 
require_once ('../includes/class/requetes.class.php');
require_once ('../includes/class/securite.class.php');

require_once('../conf/_connect.php');


$securite=new securite;

if (isset($_POST['bt']))	
		$action = $securite->verif_GetPost($_POST['bt']);

  $cnx= new actionsdata();
	$cnx->connect();


//dans les requêtes actuelles pointremise et complement ne sont pas en base de données
switch($action)
{

    case 'add':
 try
 {

            $sql  = "INSERT INTO cv_entreprise (";
            $sql .= "entreprise,adresse,adresse2,cp,ville,tel,fax,email,site_web, ";
            $sql .= "secteur_activite_entreprise,ideffectif,description,naf,";
            $sql .= "created_on";
            $sql .= ") VALUES (";
//---
            $sql .= "'".$securite->verif_GetPost($_POST['entreprise'])."',";
            $sql .= "'".$securite->verif_GetPost($_POST['adresse'])."',";
            $sql .= "'".$securite->verif_GetPost($_POST['adresse2'])."',";
            $sql .= "'".$securite->verif_GetPost($_POST['cp'])."',";
            $sql .= "'".$securite->verif_GetPost($_POST['ville'])."',";
            $sql .= "'".$securite->verif_GetPost($_POST['tel'])."',";
            $sql .= "'".$securite->verif_GetPost($_POST['fax'])."',";
            $sql .= "'".$securite->verif_GetPost($_POST['email'])."',";
            $sql .= "'".$securite->verif_GetPost($_POST['site_web'])."', ";
//--
            $sql .= "'".$securite->verif_GetPost($_POST['secteur_activite_entreprise'])."', ";
            $sql .= "'".$securite->verif_GetPost($_POST['ideffectif'])."', ";
            $sql .= "'".$securite->verif_GetPost($_POST['description'])."', ";
            $sql .= "'".$securite->verif_GetPost($_POST['naf'])."', ";
            
            
            //---
            $sql .= "'".time()."'";
            $sql .= ")";

           $cnx->requeteData($sql);
			$dernier=$cnx->last_id();
//-----------------
//-----------------
          
            $sql  = "INSERT INTO cv_entreprise_contact (";
            $sql .= "CIVILITE_CONTACT,NOM_CONTACT,PRENOM_CONTACT,FONCTION_CONTACT,ADRESSE1_CONTACT,ADRESSE2_CONTACT,CP_CONTACT,VILLE_CONTACT,";
            $sql .= "PAYS_CONTACT,TEL_CONTACT,GSM_CONTACT,FAX_CONTACT,EMAIL_CONTACT ";
            $sql .= "created_on";
            $sql .= ") VALUES (";
//---
            $sql .= "'".$securite->verif_GetPost($_POST['civilite_contact'])."',";
            $sql .= "'".$securite->verif_GetPost($_POST['nom_contact'])."',";
            $sql .= "'".$securite->verif_GetPost($_POST['prenom_contact'])."',";
            $sql .= "'".$securite->verif_GetPost($_POST['fonction_contact'])."',";
            $sql .= "'".$securite->verif_GetPost($_POST['adresse1_contact'])."',";
            $sql .= "'".$securite->verif_GetPost($_POST['adresse2_contact'])."',";
            $sql .= "'".$securite->verif_GetPost($_POST['cp_contact'])."',";
            $sql .= "'".$securite->verif_GetPost($_POST['ville_contact'])."',";
            $sql .= "'".$securite->verif_GetPost($_POST['pays_contact'])."', ";
            $sql .= "'".$securite->verif_GetPost($_POST['tel_contact'])."', ";
            $sql .= "'".$securite->verif_GetPost($_POST['fax_contact'])."', ";
            $sql .= "'".$securite->verif_GetPost($_POST['email_contact'])."', ";                    
            $sql .= "'".$dernier."', ";                    
            
            //---
            $sql .= "'".DATE("Y-m-d")."'";
            $sql .= ")";

           $result=$cnx->requeteData($sql);
           
}
 catch (Exception $e) {
    echo 'Erreur : ',  $e->getMessage(), "\n";
}

    	
    	break;

    case 'update':
 try
 {
 		$sql  = "UPDATE cv_entreprise SET ";
 		$sql .= "civilite='".		$securite->verif_GetPost($_POST['civilite'])				."',";
    	$sql .= "nom='".			$securite->verif_GetPost($_POST['nom'])						."',";
    	$sql .= "prenom='".			$securite->verif_GetPost($_POST['prenom'])					."',";
    	$sql .= "adresse='".		$securite->verif_GetPost($_POST['adresse'])					."',";
    	$sql .= "cp='".				$securite->verif_GetPost($_POST['cp'])						."',";
    	$sql .= "ville='".			$securite->verif_GetPost($_POST['ville'])					."',";
    	$sql .= "tel='".			$securite->verif_GetPost($_POST['tel'])						."',";
    	$sql .= "portable='".		$securite->verif_GetPost($_POST['portable'])				."',";
    	$sql .= "mail='".			$securite->verif_GetPost($_POST['mail'])					."',";

    	$sql .= "volume_hor='".				$securite->verif_GetPost($_POST['volume_hor'])				."',";
 		$sql .= "fiche_renseignement='".	$securite->verif_GetPost($_POST['fiche_renseignement'])		."',";
 		$sql .= "cv='".						$securite->verif_GetPost($_POST['cv'])						."',";
 		$sql .= "rib='".					$securite->verif_GetPost($_POST['rib'])						."',";
 		$sql .= "copie_identite='".			$securite->verif_GetPost($_POST['copie_identite'])			."',";
 		$sql .= "type_contrat='".			$securite->verif_GetPost($_POST['type_contrat'])			."',";
 		$sql .= "autorisation_enseign='".	$securite->verif_GetPost($_POST['autorisation_enseign'])	."',";
 		$sql .= "autorisation_cumul='".		$securite->verif_GetPost($_POST['autorisation_cumul'])		."',";
 		$sql .= "commentaires='".			$securite->verif_GetPost($_POST['commentaires'])			."',";
    	

 		$sql .= "nom_societe='".		$securite->verif_GetPost($_POST['nom_societe'])			."',";
 		$sql .= "adresse_societe='".	$securite->verif_GetPost($_POST['adresse_societe'])		."',";
 		$sql .= "cp_societe='".			$securite->verif_GetPost($_POST['cp_societe'])			."',";
 		$sql .= "ville_societe='".		$securite->verif_GetPost($_POST['ville_societe'])		."',";
 		$sql .= "tel_societe='".		$securite->verif_GetPost($_POST['tel_societe'])			."',";
 		$sql .= "gsm_societe='".		$securite->verif_GetPost($_POST['gsm_societe'])			."',";
 		$sql .= "mail_societe='".		$securite->verif_GetPost($_POST['mail_societe'])		."',";
 		$sql .= "rcs_siren_siret='".	$securite->verif_GetPost($_POST['rcs_siren_siret'])		."',";
 		$sql .= "justifi_urssaf='".		$securite->verif_GetPost($_POST['justifi_urssaf'])		."',";
 		
    	$sql .= "updated_on='".		DATE("Y-m-d H:m:s")											."' ";
    	$sql .= "WHERE id='".		$securite->verif_GetPost($_POST['id'])						."' ";
   	
    	$result=$cnx->requeteData($sql); 

//-----

    	
}
 catch (Exception $e) {
    echo 'Erreur : ',  $e->getMessage(), "\n";
}
    	break;

    case 'del':
    /*    $sql = "DELETE FROM profs WHERE id='".$prof->securite($_POST['id'])."'";
    	$result=$cnx->requeteData($sql); 
    */
    	  break;

}


 header("Location: ..") ;

?>
