/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function() {

    $.getJSON('redirecteur.php?dest=ajax-scripts_req-offre-chart&page=visite', function(data){
        $.jqplot('chartcontainer1',[data.rows],
        {
            show: true,
            title: 'nombre de visites',
            axes:{
                xaxis : {
                    renderer:$.jqplot.DateAxisRenderer,
                    rendererOptions:{tickRenderer:$.jqplot.CanvasAxisTickRenderer},
                    ticks: data.cols,
                    tickOptions:{
                        formatString:'%b %Y',
                        fontSize:'6pt',
                        fontFamily:'Tahoma',
                        angle:-30
                    }
                },
                yaxis:{min:data.min, max:data.max}
            },
            highlighter: {
                show: true,
                sizeAdjust: 7.5},
            cursor:{show:false}

        });
    });

    $.getJSON('redirecteur.php?dest=ajax-scripts_req-offre-chart&page=offre', function(data){
        $.jqplot('chartcontainer2',[data.rows],
        {
            show: true,
            title: 'nombre d\'offres',
            axes:{
                xaxis : {
                    renderer:$.jqplot.DateAxisRenderer,
                    rendererOptions:{tickRenderer:$.jqplot.CanvasAxisTickRenderer},
                    ticks: data.cols,
                    tickOptions:{
                        formatString:'%b %Y',
                        fontSize:'6pt',
                        fontFamily:'Tahoma',
                        angle:-30
                    }
                },
                yaxis:{min:data.min, max:data.max}
            },
            highlighter: {
                show: true,
                sizeAdjust: 7.5},
            cursor:{show:false}

        });
    });

});