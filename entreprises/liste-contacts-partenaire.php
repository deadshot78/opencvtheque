<?php
$securite = new securite();

//-- Partie Pagination
$page = $securite->verif_GetPost ($_GET['page']);	// numero de la page 
$limit = $securite->verif_GetPost ($_GET['rows']);  // nombre de lignes à afficher
//-- fin Partie Pagination


// partie tri
$sidx = $_GET['sidx']; // get index row - i.e. user click to sort
$sord = $_GET['sord']; // get the direction
if(!$sidx) $sidx =1;
//$oFirebug->fb($sidx);
//$oFirebug->fb($sord);

// partie recherche
$searchOn = stripslashes($_GET['_search']);
$searchfield = $_GET['searchField']; // get the field
$searchstring = $_GET['searchString']; // get the string
$searchoper = $_GET['searchOper']; // get the operator

/*$oFirebug->fb($searchOn);
$oFirebug->fb($searchfield);
$oFirebug->fb($searchstring);
$oFirebug->fb($searchoper);
$oFirebug->fb("----");
*/

if(isset($_GET['_search']) && !empty($_GET['_search']))
    $searchOn = stripslashes($_GET['_search']);
else $searchOn = "false";
//else die;

//echo "####".$searchfield."####";

if($searchOn=='true') {
    if($searchfield){
        $wh = conv_operator($searchfield,$searchoper,$searchstring);
    }
    else{
        $sarr = $_GET;
        foreach( $sarr as $k=>$v) {
            switch ($k) {
                case 'nom_contact':
                    $wh .= " AND ".$k." LIKE '%".$v."%'";
                    break;
                case 'prenom_contact':
                    $wh .= " AND ".$k." LIKE '%".$v."%'";
                    break;
                case 'naf':
                    $wh .= " AND ".$k." LIKE '%".$v."%'";
                    break;
            }
        }
    }
    
    }


//$wh = "";

    $cnx= new actionsdata();
    $cnx->connect();
   
    $req_liste_contacts = "SELECT ID_CONTACT_PARTENAIRE,CIVILITE_CONTACT,PRENOM_CONTACT,NOM_CONTACT,FAX_CONTACT,GSM_CONTACT,TEL_CONTACT,EMAIL_CONTACT ";
    $req_liste_contacts .= "FROM ".$param["table"]["contacts_partenaire"];
    $req_liste_contacts .= " WHERE id_contact=".$_SESSION['contact']." AND state != 2";
		     
// detemine la pagination
    $pagination=$cnx->pagination($cnx,$req_liste_contacts,$start,$page,$limit);

// Filtre du nombre de lignes par pages
    $req_liste_contacts .= " LIMIT ".$pagination['start']." , ".strval($pagination['limit']);

//echo $sql;

// prepare la requete à afficher avec la pagination
    $row=$cnx->requeteSelect ($req_liste_contacts);
   		
   		
// construit les données qui seront affichées
    $responce->page = $pagination['page'];
    $responce->total = $pagination['total_pages'];
    $responce->records = $pagination['count']; 
        

$i=0;

if($row != 0) {

    foreach($row as $data)
    {
                    $responce->rows[$i]['id_contact_partenaire'] = $data['ID_CONTACT_PARTENAIRE'];
                    $responce->rows[$i]['cell']=array(
                        conv_civilite($data['CIVILITE_CONTACT'], $param['civilite']),
                        $data['NOM_CONTACT'],
                        $data['PRENOM_CONTACT'],
                        $data['TEL_CONTACT'],
                        $data['FAX_CONTACT'],
                        $data['GSM_CONTACT'],
                        $data['EMAIL_CONTACT'],
                        );
                $i++;
    }
    $cnx->deconnect();
    echo json_encode($responce);
} else {
    $cnx->deconnect();
    echo 'aucun enregistrement';
}
       
?>
