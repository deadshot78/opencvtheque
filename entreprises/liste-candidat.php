<?php

$securite = new securite();

include_once ABSPATH.'includes/class/_init_liste_var.php';

if(isset($_GET['_search']) && !empty($_GET['_search']))
    $searchOn = stripslashes($_GET['_search']);
else $searchOn = "false";
//else die;

//echo "####".$searchfield."####";

if($searchOn=='true') {
    if($searchfield){
        $wh = conv_operator($searchfield,$searchoper,$searchstring);
    }
    else{
        $sarr = $_GET;
        foreach( $sarr as $k=>$v) {
            switch ($k) {
                case 'entreprise':
                    $wh .= " AND ".$k." LIKE '%".$v."%'";
                    break;
                case 'ville':
                    $wh .= " AND ".$k." LIKE '%".$v."%'";
                    break;
                case 'naf':
                    $wh .= " AND ".$k." LIKE '%".$v."%'";
                    break;
            }
        }
    }
    
}

    $cnx= new actionsdata();
    $cnx->connect();
   
    $req_liste_candidats = "SELECT ID_CONTACT,CIVILITE_CONTACT,PRENOM_CONTACT,";
    $req_liste_candidats .= "NOM_CONTACT,GSM_CONTACT,TEL_CONTACT,EMAIL_CONTACT,";
    $req_liste_candidats .= "FONCTION_CONTACT,name_city,name_state,name_country,";
    $req_liste_candidats .= "name_province,".$param["view"]["localisation"].".id_city ";
    $req_liste_candidats .= "FROM ".$param["table"]["contact"]." LEFT JOIN ";
    $req_liste_candidats .= $param["view"]["localisation"]." ON ".$param["table"]["contact"];
    $req_liste_candidats .= ".id_city=".$param["view"]["localisation"].".id_city ";
    $req_liste_candidats .= "WHERE type_contact=1 ";
		     
// detemine la pagination
    $pagination=$cnx->pagination($cnx,$req_liste_candidats,'',$page,$limit);

// Filtre du nombre de lignes par pages
    $req_liste_candidats .= "LIMIT ".$pagination['start']." , ".strval($pagination['limit']);

//echo $req_liste_candidats;

// prepare la requete à afficher avec la pagination
    $row=$cnx->requeteSelect ($req_liste_candidats);
   		
// construit les données qui seront affichées
    $responce->page = $pagination['page'];
    $responce->total = $pagination['total_pages'];
    $responce->records = $pagination['count']; 

$i=0;
if($row != 0) {
    foreach($row as $data)
    {
        $civilite = "";
        if(!empty($data['CIVILITE_CONTACT'])) {
            $civilite = $param['civilite'][$data['CIVILITE_CONTACT']];
        }

        $responce->rows[$i]['id'] = $data['ID_CONTACT'];
        $responce->rows[$i]['cell']=array(
                $civilite,
                $data['NOM_CONTACT'],
                $data['PRENOM_CONTACT'],
                $data['FONCTION_CONTACT'],
                $data['TEL_CONTACT'],
                $data['GSM_CONTACT'],
                $data['EMAIL_CONTACT'],
                $data['name_city'],
                $data['name_state'],
                $data['name_country'],
                $data['name_province']
                );
        $i++;
    }
    $cnx->deconnect();
    echo json_encode($responce);
} else {
    $cnx->deconnect();
    echo 'aucun enregistrement';
}
       
?>
