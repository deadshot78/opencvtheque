<?php require_once 'frm-tables.php'; //contient champs formulaires, les tables, les conditions ?>
<script>
    var dayNames=<?php echo $param["datepicker"]["dayNamesMin"]?>;
    var monthNames=<?php echo $param["datepicker"]["monthNames"]?>;
    var yearRange='<?php echo (date('Y')-60).':'.(date('Y')-15)?>';
    var permisRange='<?php echo (date('Y')-60).':'.date('Y')?>';
    <?php include_once ("js/fonction.js");
          include_once ("includes/js/form-fonctions.js"); ?>
    var largeurgrid = 0;
    var noms_colonnes = "";
    var modele_colonnes ="";
    var navgrid_options = "";
    var alert_ext = "";
    var alert_nbl = "";
    var add_title = "ajouter photo";
    var rem_title = "supprimer photo";
    var imgsrc = "entreprises_edit-logo";
    var data_show = "";
    var action_upload='entreprises_req-upload-logo';

    <?php include_once (ABSPATH.'includes/js/fonctionsjs.js'); ?>
</script>

<div id="profil">
<h2 id="titre_page"><?php echo $param["gestionprofil"]["title"]?></h2>
<input id="pagelink" type="hidden" value="entreprises cv id_entreprise cv"/> <!-- Répertoire table clef -->

<?php

    $cnx=new actionsdata();
    $securite=new securite;
    $infos_entreprise=$cnx->select_data($tab_champs,$tab_tables,$tab_conditions);
    $clean_post=$securite->clean_tab($infos_entreprise,"decode");

    $form=new formulaire();
    $form_entreprise = $form->creer_form(
            array(
                'nomform'=>'gestionprofilent',
                'actionform'=>'redirecteur.php?dest=entreprises_req-maj-profil',
                'methodform'=>'POST',
                'class'=>'formulaire-fiche',
                'submit'=>$param["action"]["update"]
            ),
        $tab_champs,
        $clean_post[0],
        $param
        );

    // Affichage formulaire
    echo $form_entreprise;
   // $entreprise->deconnect();
?>
</div>