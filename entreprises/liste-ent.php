<h2 id="titre_page">Entreprises</h2>
<input id="pagelink" type="hidden" value="entreprises entreprises-partenaire id bouton"/> <!-- Répertoire table clef bouton-->

<script type="text/javascript">

	<?php    if($_SESSION["type"]==2 || $_SESSION["type"]==3)   { ?>

	    var popin_largeur = 1024;
	    var popin_hauteur = 500;
	    var popin_add_title = '<?php echo $param["action"]["add"];?>';
            var popin_update_title = '<?php echo $param["action"]["update"];?>';
            //var popin_action='./entreprises/frm.php';
	    var popin_edition='entreprises_frm-partenaire';
	    var largeurgrid = window.innerWidth * 0.95;
            var navgrid_options = {view:false,edit:false,add:false,del:true};
            var parametre_supp = 'type=entreprise';
            var session = 23;
	<?php  } ?>
// Nom des colonnes en haut du tableau
    var noms_colonnes = ['Entreprise','Ville','Tel','Fax','Email','Secteur'];


// contenu des colonnes    
    var modele_colonnes = [
                           {name:'raison_sociale_entreprise', index:'raison_sociale_entreprise', align:'left', editable:false, sortable:true, width:50},
                           {name:'ville', index:'ville', align:'left', editable:false, sortable:false, width:40},
                           {name:'tel', index:'tel', align:'left', editable:true, sortable:false, search:false,width:30},
                           {name:'fax', index:'fax', align:'left', editable:true, sortable:false, search:false,width:30},
                           {name:'email', index:'email', align:'left', editable:true, sortable:false, search:false,width:50},
                           {name:'secteur', index:'secteur', align:'left', editable:false, search:false, sortable:false, width:20},
                           ];

    var show_subgrid = true;
    var subgrid_noms_colonnes = ['Civilite','Nom','Nom','Prenom','Prenom','Fonction','Tél','Fax','Portable','Email','id_entreprise'];
    var subgrid_modele_colonnes = [
                                   {name:'civilite', index:'Civilite', align:'left', editable:false, sortable:true, width:50},
                                   {name:'nom', index:'Nom', align:'left', editable:true, hidden:true,sortable:false, width:180,editrules:{edithidden:true, required:true}},
                                   {name:'nom_aff', index:'Nom_aff', align:'left', editable:false, sortable:false, width:180},
                                   {name:'prenom', index:'Prenom', align:'left', editable:true, hidden:true,sortable:false, width:180,editrules:{edithidden:true, required:true}},
                                   {name:'prenom_aff', index:'Prenom_aff', align:'left', editable:false, sortable:false, width:180},
                                   {name:'fonction', index:'Fonction', align:'left', editable:true, sortable:false, search:false,width:180},
                                   {name:'tel', index:'Tel', align:'left', editable:true, sortable:false, search:false,width:120},
                                   {name:'fax', index:'Fax', align:'left', editable:true, sortable:false, search:false,width:120},
                                   {name:'gsm', index:'GSM', align:'left', editable:true, sortable:false, width:120},
                                   {name:'email', index:'Email', align:'left', editable:true, sortable:false, width:180},
                                   {name:'id_entreprise', index:'id_entreprise', align:'left', hidden:true, editable:true, sortable:false, width:180} //
                                    ];

 
<?php
    include_once (ABSPATH.'includes/js/fonctionsjs.js');
    include_once('js/fonction_liste.js');
?>

</script>

<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
//print_r($_SESSION);
include_once (ABSPATH.'commun/liste.php');
?>

<div id="formulairePerso">

</div>