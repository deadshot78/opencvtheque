<?php

$securite = new securite();

//-- Partie Pagination
$page = $securite->verif_GetPost ($_GET['page']);	// numero de la page 
$limit = $securite->verif_GetPost ($_GET['rows']);  // nombre de lignes à afficher
//-- fin Partie Pagination


// partie tri
$sidx = $_GET['sidx']; // get index row - i.e. user click to sort
$sord = $_GET['sord']; // get the direction
if(!$sidx) $sidx =1;

// partie recherche
$searchOn = stripslashes($_GET['_search']);
$searchfield = $_GET['searchField']; // get the field
$searchstring = $_GET['searchString']; // get the string
$searchoper = $_GET['searchOper']; // get the operator

if(isset($_GET['_search']) && !empty($_GET['_search']))
    $searchOn = stripslashes($_GET['_search']);
else $searchOn = "false";

if($searchOn=='true') {
    if($searchfield){
        $wh = conv_operator($searchfield,$searchoper,$searchstring);
    }
    else{
        $sarr = $_GET;
        foreach( $sarr as $k=>$v) {
            switch ($k) {
                case 'raison_sociale_entreprise':
                    $wh .= " AND ".$k." LIKE '%".$v."%'";
                    break;
                case 'ville':
                    $wh .= " AND ".$k." LIKE '%".$v."%'";
                    break;
                /*case 'secteur_activite_entreprise':
                    $wh .= " AND ".$k." LIKE '%".$v."%'";
                    break;*/
            }
        }
    }
    
    }

    $cnx= new actionsdata();
    $cnx->connect();
   
        $req_liste_entreprises_partenaire = "SELECT id,raison_sociale_entreprise,ville,tel,fax,email,secteur_activite_entreprise ";
        $req_liste_entreprises_partenaire .= "FROM ".$param["table"]["entreprises_partenaire"];
        $req_liste_entreprises_partenaire .= " WHERE id_contact=".$_SESSION['contact']." AND state!=2";
        $req_liste_entreprises_partenaire .= $wh;


// detemine la pagination
    $pagination=$cnx->pagination($cnx,$req_liste_entreprises_partenaire,$start,$page,$limit);

// Filtre du nombre de lignes par pages
    $req_liste_entreprises_partenaire .= " LIMIT ".$pagination['start']." , ".strval($pagination['limit']);

//echo $sql;

// prepare la requete à afficher avec la pagination
	$res_liste_entreprises_partenaire=$cnx->requeteSelect ($req_liste_entreprises_partenaire);
   		
   		
// construit les données qui seront affichées
    $responce->page = $pagination['page'];
    $responce->total = $pagination['total_pages'];
    $responce->records = $pagination['count']; 

$i=0;

if($res_liste_entreprises_partenaire != 0) {
    foreach($res_liste_entreprises_partenaire as $data)
    {
        $secteur_activite=get_secteur($data['secteur_activite_entreprise'], $Langue);

                $responce->rows[$i]['id'] = $data['id'];
                $responce->rows[$i]['cell']=array(
                        $data['raison_sociale_entreprise'],
                        $data['ville'],
                        $data['tel'],
                        $data['fax'],
                        $data['email'],
                        $secteur_activite
                        );
                $i++;
    }
    $cnx->deconnect();
    echo json_encode($responce);
} else {
    $cnx->deconnect();
    echo 'aucun enregistrement';
}
       
?>