<?php
//#!/usr/bin/php
//http://localhost/opencvtheque-ng/admin/import_geonames.php
/*** Just submited server form, table ***/
//$list_servers=csv_read_array("../import/FR.txt","\t"); // Récupérer le nom du fichier
session_start();
include "../conf/include_req.php";
//$securite->check_provenance('upload_cv.php',$_SESSION['page_courante']);

require_once ('../includes/class/requetes.class.php');

//$import_liste=array_merge($title,$list_servers);

//csv_display_array($import_liste);

echo date('H:i:s:u')."<br/>";

$cnx= new actionsdata();
$cnx->connect();


$req_pays="select code,id from cv_country";
$res_pays=$cnx->requeteSelect($req_pays);

$pays=array();

foreach ($res_pays as $line){

    $pays[$line['id']]=$line['code'];

}

$req_insert_region="";
$req_insert_region_old="";
$req_insert_departement="";
$req_insert_departement_old="";
$req_insert_city="";
$req_insert_city_old="";
$city="";
$city_old="";



//if (($handle = fopen("FR.txt","r")) !== FALSE) {

$lines = file("FR.txt");
//for($i=count($lines);$i>0;$i--){
    //echo $lines[$i];
//}


//    while (($field = fgetcsv($handle, 2048, "\t")) !== FALSE) {
    foreach ($lines as $line){

        $field=explode("\t",$line);
        $country_code = addslashes($field[0]);
        $postal_code = addslashes($field[1]);
        $place_name = addslashes($field[2]);
        $state_name = addslashes($field[3]);
        $state_code = addslashes($field[4]);
        $province_name = addslashes($field[5]);
        $province_code = addslashes($field[6]);
        $community_name = addslashes($field[7]);
        $community_code = addslashes($field[8]);
        $latitude = addslashes($field[9]);
        $longitude = addslashes($field[10]);
    //    echo $country_code." ".$postal_code." ".$place_name." ".$state_name." ".$state_code." ".$province_name." ".$province_code." ".$latitude." ".$longitude."\n";
        $req_insert_region = "insert into cv_state (name_state,name_state_uppercase,state_slug,id_country,state_code) values ";
        $req_insert_region .="('".$state_name."','".strtoupper(suppr_accent($state_name))."','".strtolower(str_replace(' ','-',suppr_accent($state_name)))."','";
        $req_insert_region .=array_search(strtolower($country_code),$pays)."','".$state_code."')";

        if($req_insert_region!=$req_insert_region_old)
        {
            $res_insert_region = $cnx->requeteData($req_insert_region);
            $req_insert_region_old=$req_insert_region;
        }


        $req_insert_departement="insert into cv_province (code,name_province,name_province_uppercase,province_slug,id_region) values ";
        $req_insert_departement .="('".$province_code."','".ucwords($province_name)."','".strtoupper(suppr_accent($province_name))."','".strtolower(str_replace(' ','-',suppr_accent($province_name)))."','";
        $req_insert_departement .=$state_code."')";

        if($req_insert_departement!=$req_insert_departement_old){
            $res_insert_departement=$cnx->requeteData($req_insert_departement);
            $req_insert_departement_old=$req_insert_departement;
        }


        $req_insert_city = "insert into cv_city (id_province,name_city,name_city_uppercase,city_slug,latitude,longitude,cp) values ";
        $req_insert_city .= "('".$province_code."','".ucwords($place_name)."','".strtoupper(suppr_accent($place_name))."','".strtolower(str_replace(' ','-',suppr_accent($place_name)))."','";
        $req_insert_city .= $latitude."','".$longitude."'";



        if($city_old==""){
            $city_old=$place_name;
            $latitude_old=$latitude;
            $longitude_old=$longitude;
            $postal_code_old=$postal_code;
            $req_insert_city .= ",'".$postal_code."')";
            $req_insert_city_old = $req_insert_city;
        }
        else{
            if($place_name!=$city_old){

                $req_get_city="select id from cv_city where name_city='".$city_old."' AND longitude=".$longitude_old." AND latitude=".$latitude_old;
                $res_get_city=$cnx->requeteSelect($req_get_city);
                //$nb_ligne=mysql_num_rows($res_get_city);

                if(!$res_get_city){
                    $res_insert_city_old = $cnx->requeteData($req_insert_city_old);
                }
                else{

                   
                    $liste_cp_old=substr(strrchr($req_insert_city_old, ","),2,-2);

                    $req_update_city="update cv_city set cp=concat(cp,'-','".$liste_cp_old."') where id=".$res_get_city[0]['id'];
                    $res_update_city=$cnx->requeteData($req_update_city);
                }

                $city_old=$place_name;
                $latitude_old=$latitude;
                $longitude_old=$longitude;
                $postal_code_old=$postal_code;
                $req_insert_city_old=$req_insert_city.",'".$postal_code."')";

            }
            else{
                $city_old=$place_name;
                $req_insert_city_old=substr($req_insert_city_old,0,-2)."-".$postal_code."')";
            }
        }


        //}
//    fclose($handle);
}
$res_insert_city_old = $cnx->requeteData($req_insert_city_old);

    $cnx->deconnect();

echo date('H:i:s:u')."<br/>";
?>