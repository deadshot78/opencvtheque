<?php

include_once ABSPATH.'includes/class/_init_liste_var.php';
/*
* req_offre_chart.php Created 23 févr. 2011 at 11:17:24 by flenoble under Ocv-NG
* $Id$
*/

/* NB VISITE PAR CONTACT
 * 
SELECT cv_statistiques.id_offre, id_cv, cv_statistiques.id_contact,count(cv_statistiques.id_contact) as nb_visit_contact, cv_statistiques.created_on
FROM cv_statistiques
JOIN cv_offre ON cv_offre.id_offre = cv_statistiques.id_offre
WHERE cv_offre.id_contact =15
GROUP BY id_contact
 * 
 */


switch ($page){
    case 'visite':
            /*
             * Nombre de visites par mois
             */

            $req_nb_visites = "SELECT ".$param["table"]["statistiques"].".id_offre, ";
            $req_nb_visites .= "id_cv, ".$param["table"]["statistiques"].".id_contact, ";
            $req_nb_visites .= "count( ".$param["table"]["statistiques"].".id_contact ) AS nb_visite_contact, ";
            $req_nb_visites .= "FROM_UNIXTIME(".$param["table"]["statistiques"].".created_on) as created_on, ";
            $req_nb_visites .= "str_to_date(concat( year( FROM_UNIXTIME(".$param["table"]["statistiques"].".created_on) ) , '-', ";
            $req_nb_visites .= "month( FROM_UNIXTIME(".$param["table"]["statistiques"].".created_on) ),'-01'),'%Y-%m-%d  %H:%i:%s') AS periode ";
            $req_nb_visites .= "FROM ".$param["table"]["statistiques"]." JOIN ".$param["table"]["offre"]." ";
            $req_nb_visites .= "ON ".$param["table"]["offre"].".id_offre = ".$param["table"]["statistiques"].".id_offre ";
            $req_nb_visites .= "WHERE ".$param["table"]["offre"].".id_contact = '".$contact."' ";
            $req_nb_visites .= "AND FROM_UNIXTIME(".$param["table"]["statistiques"].".created_on) ";
            $req_nb_visites .= "> (DATE_SUB(CURDATE(), INTERVAL 12 MONTH)) GROUP BY ";
            $req_nb_visites .= "year( FROM_UNIXTIME(".$param["table"]["statistiques"].".created_on) ), ";
            $req_nb_visites .= "month( FROM_UNIXTIME(".$param["table"]["statistiques"].".created_on) )";

            $res_nb_visites = $cnx->requeteSelect($req_nb_visites);

            $i=1;
            $max = 0;

            $responce->min = 0;
            $responce->records = 30;

            foreach($res_nb_visites as $visite){

                $responce->rows[]=array($visite['created_on'],$visite['nb_visite_contact']);
                $responce->cols[]=array($visite['periode']);
                if($visite['nb_visite_contact']>$max){
                    $max = $visite['nb_visite_contact'];
                }

                $i++;
            }

            $responce->max = $max;

        break;

    case 'offre':

            $req_nb_offres = "SELECT id_offre, id_offre, id_contact, count(id_contact ) ";
            $req_nb_offres .= "AS nb_offre, FROM_UNIXTIME(created_on) as created_on, ";
            $req_nb_offres .= "str_to_date(concat( year( FROM_UNIXTIME(created_on) ) , '-', ";
            $req_nb_offres .= "month( FROM_UNIXTIME(created_on) ),'-01'),'%Y-%m-%d  %H:%i:%s') AS periode ";
            $req_nb_offres .= "FROM ".$param["table"]["offre"]." WHERE id_contact = '".$contact."' ";
            $req_nb_offres .= "AND FROM_UNIXTIME(created_on) ";
            $req_nb_offres .= "> (DATE_SUB(CURDATE(), INTERVAL 18 MONTH)) GROUP BY ";
            $req_nb_offres .= "year( FROM_UNIXTIME(created_on) ), ";
            $req_nb_offres .= "month( FROM_UNIXTIME(created_on) )";

            //echo $req_nb_offres;

            $res_nb_offres = $cnx->requeteSelect($req_nb_offres);

            $i=1;
            $max = 0;

            $responce->min = 0;
            $responce->records = 30;

            foreach($res_nb_offres as $offre){

                $responce->rows[]=array($offre['created_on'],$offre['nb_offre']);
                $responce->cols[]=array($offre['periode']);
                if($offre['nb_offre']>$max){
                    $max = $offre['nb_offre'];
                }

                $i++;
            }

            $responce->max = $max;

        break;

}

echo json_encode($responce);


?>
