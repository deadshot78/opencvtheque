var sub_navgrid_options = '';
var sort_order = 'desc';
var sort_subgrid = 'id';
var show_subgrid = false;
var popin_largeur = 1024;
var popin_hauteur = 500;
var popin_add_title = '<?php echo $param["action"]["add"];?>';
var popin_update_title = '<?php echo $param["action"]["update"];?>';
var popin_duplicate_title = '<?php echo $param["action"]["duplicate"];?>';
var popin_action='candidats_maj-experiences';
var popin_edition='candidats_frm-experiences';
var popin_rem_title = '<?php echo $param["action"]["delete"];?>';
var largeurgrid = window.innerWidth * 0.95;
var navgrid_options = {view:false,edit:true,add:false,del:true};

var popin_largeur = 1024;
var popin_hauteur = 500;
var popin_view_title = '<?php echo $param["action"]["view"];?>';
var popin_affiche='offres_view-tous';
var largeurgrid = window.innerWidth * 0.95;
var navgrid_options = {view:false,edit:false,add:false,del:false};

var noms_colonnes = ['Titre','Début','Fin','Fonction','Entreprise','Lieu','Description','Créée le'];
// contenu des colonnes
var modele_colonnes = [
                  {name:'titre_experience', index:'titre_experience', align:'left', width:30},
                  {name:'date_debut_experience', index:'date_debut_experience', align:'left', width:30},
                  {name:'date_fin_experience', index:'date_fin_experience', align:'left', width:40},
                  {name:'fonction_experience', index:'fonction_experience', align:'left', width:80},
                  {name:'entreprise_experience', index:'entreprise_experience', align:'left', width:50},
                  {name:'lieu_experience', index:'lieu_experience', align:'left', width:50},
                  {name:'description_experience', index:'description_experience', align:'left', width:50},
                  {name:'created_on', index:'created_on', editable: false, search:false, align:'left', width:40},
                   ];
