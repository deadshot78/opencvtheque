$(function(){

    //$('#gestionprofil').Fvalidate();
  
    $('#remove').click(function(){
            $.post("redirecteur.php?dest=candidats_req-upload-photo",{
                    'action':this.value},
                    function(data){
                             $(".photo").attr({
                                'src':'redirecteur.php?dest=candidats_edit-photo'
                             });
                    })
    });

    $('#date-naissance-candidat').datepicker({
        //minDate: new Date(1940, 1 - 1, 1),
        defaultDate: new Date(),
        yearRange: yearRange,
        dateFormat: 'dd/mm/yy',
        //minDate: '-60y',
        //maxDate: '-15y',
        dayNamesMin: dayNames,
        monthNamesShort: monthNames,
        changeMonth: true,
        changeYear: true
    });

    $('#date-permis-candidat').datepicker({
        //minDate: new Date(1940, 1 - 1, 1),
        defaultDate: new Date(),
        yearRange: permisRange,
        dateFormat: 'dd/mm/yy',
        //minDate: '-60y',
        //maxDate: '-15y',
        dayNamesMin: dayNames,
        monthNamesShort: monthNames,
        changeMonth: true,
        changeYear: true
    });

    /*('#ville-contact').autocomplete({
        minLength: 2,
        delay: 10,
        source: "./includes/class/autosuggest.inc.php",
        select: function(event, ui) {
                    $('#ville-contact').val(ui.item.both);
                    $('#cp-contact').val(ui.item.value);
                    return false;
                }
    });*/


    $("#radio_civilite-contact").buttonset();
    $("#radio_situation-candidat").buttonset();
});
