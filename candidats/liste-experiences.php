<?php
/*
* liste-formations.php Created 9 févr. 2011 at 13:48:01 by flenoble under Ocv-NG
* $Id$
*/

$securite = new securite();
$contact = $_SESSION['contact'];

include_once ABSPATH.'includes/class/_init_liste_var.php';

if(!$sidx) $sidx =1;
$wh = "";
$searchOn = $securite->verif_GetPost ($_GET['_search']);


//echo "####".$searchfield."####";

//print_r($_POST);

if($searchOn=='true') {
    //echo "####";

    if($searchfield){
        $wh = conv_operator($searchfield,$searchoper,$searchstring);
        echo $wh;
    }
    else{
        $sarr = $_GET;
        foreach( $sarr as $k=>$v) {
            switch ($k) {
                case 'id':
                case 'reference_offre':
                    $wh .= " AND ".$k." LIKE '".$v."%' ";
                    break;
/*                case 'intitule_offre':
                    $wh .= " AND ".$k." LIKE '%".$v."%' ";
                    break;
                case 'raison_sociale_entreprise':
                    $wh .= " AND ".$k." LIKE '%".$v."%' ";
                    break;
                case 'nom_type_offre':
                    $wh .= " AND ".$param["table"]["type_offre"].".nom LIKE '%".$v."%' ";
                    break;
                case 'nom_contact':
                    $wh .= " AND ".$k." LIKE '%".$v."%' ";
                    break;
                case 'date_depot_offre':
                    $wh .= " AND ".$k." LIKE '%".$v."%' ";
                    break;
                case 'date_debut_offre':
                    $wh .= " AND ".$k." LIKE '%".$v."%' ";
                    break;
                case 'lieu_offre':
                    $wh .= " AND (lieu_offre LIKE '".$v."%' OR name_city like '".$v."%') ";
                    break;
                case 'name_province':
                    $wh .= " AND ".$k." LIKE '".$v."%' ";
                    break;
                case 'name_state':
                    $wh .= " AND (".$k." LIKE '".$v."%' OR region_offre like '".$v."%') ";
                    break;
                case 'name_country':
                    $wh .= " AND ".$k." LIKE '".$v."%' ";
                    break;*/
            }
        }
    }
}
//---------------------------------------------------------------------
    $cnx= new actionsdata();
    $cnx->connect();

    $req_liste_experiences = "SELECT * FROM ".$param["table"]["experience"]." JOIN ";
    $req_liste_experiences .= $param["table"]["realiserexperience"]." ON ";
    $req_liste_experiences .= $param["table"]["experience"].".id_experience=";
    $req_liste_experiences .= $param["table"]["realiserexperience"].".id_experience ";
    $req_liste_experiences .= "WHERE ".$param["table"]["realiserexperience"].".id_contact = ";
    $req_liste_experiences .=  "'".$contact."' ";


    $req_liste_experiences .= "ORDER BY ".$param["table"]["experience"].".$sidx $sord ";

    //echo $req_liste_experiences;

// determine la pagination
    $pagination=$cnx->pagination($cnx,$req_liste_experiences,1,$page,$limit);

    //var_dump($pagination);

// Filtre du nombre de lignes par pages
    $req_liste_experiences .= "LIMIT ".$pagination['start']." , ".strval($pagination['limit']);

   //echo $req_liste_experiences;

// prepare la requete à afficher avec la pagination
    $res_liste_experiences=$cnx->requeteSelect ($req_liste_experiences);


// construit les données qui seront affichées

    $responce->page = $pagination['page'];
    $responce->total = $pagination['total_pages'];
    $responce->records = $pagination['count'];

    $i=0;

if($res_liste_experiences != 0) {
    foreach($res_liste_experiences as $data) {
// 	 	 	 	 	 	 	 	updated_on
        $responce->rows[$i]['id']=$data['ID_EXPERIENCE'];
        $responce->rows[$i]['cell']=array(
        $data['TITRE_EXPERIENCE'],
        $data['DATE_DEBUT_EXPERIENCE'],
        $data['DATE_FIN_EXPERIENCE'],
        $data['FONCTION_EXPERIENCE'],
        $data['ENTREPRISE_EXPERIENCE'],
        $data['LIEU_EXPERIENCE'],
        substr(str_replace(array(chr(10),chr(13)), array(""," "), $data['DESCRIPTION_EXPERIENCE']),0,20)." [...]",
        $data['created_on']);
        $i++;
    }

    $cnx->deconnect();
    echo json_encode($responce);
} else {
    $cnx->deconnect();
    echo 'aucun enregistrement';
}
?>
