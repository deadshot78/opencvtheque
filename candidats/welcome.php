<?php
/*
* welcome.php Created 11 févr. 2011 at 11:57:03 by flenoble under Ocv-NG
* $Id$
*/

?>
<link rel="stylesheet" type="text/css" href="css/jquery.jqplot.css" />
<script src="includes/js/jquery.jqplot.min.js" type="text/javascript"></script>

<script type="text/javascript">
    <?php
        include_once ('js/fonction_welcome.js');
    ?>
</script>

<div id="boxinfo">
    <div id="statut" class="roundbox"><h1>Statut</h1></div>
    <div id="statistiques" class="roundbox">
        <h1>Statistiques</h1>
        <div id="chart-container" style="width: 100%; height: 200px"></div>
    </div>
</div>
<br/>
<div id="candidatures" class="roundbox"><h1>Mes dernières candidatures</h1></div>
