<?php
/*
* req-maj-listecompetences.php Created 15 févr. 2011 at 11:33:33 by flenoble under Ocv-NG
* $Id$
*/
$securite=new securite();
$cnx= new actionsdata();
$cnx->connect();

$clean_post=$securite->clean_tab($_POST,"encode");

$req_update_liste_competences = "INSERT INTO ".$param["table"]["competence"];
$req_update_liste_competences .= "(id_categorie,nom_competence,created_on) VALUES ";
$req_update_liste_competences .= "(1,'".$clean_post['add_competence']."',";
$req_update_liste_competences .= "'".time()."')";

$res_update_liste_competences = $cnx->requeteData($req_update_liste_competences);

if (!$res_update_liste_competences) {
    $message  = 'Requête invalide : ' . mysql_error() ." ". mysql_errno(). "\n";
    $message .= "Requête complète : \n". $req_update_liste_competences;

    echo "false"." ".$message;
    die($message);
}
else{
    $return_succes = "competence added\n";
}

$req_insert_competence_candidat = "INSERT INTO ".$param["table"]["acquerircompetence"];
$req_insert_competence_candidat .= "(id_contact,id_competence,created_on) VALUES ";
$req_insert_competence_candidat .= "('".$_SESSION['contact']."','".$cnx->last_id()."',";
$req_insert_competence_candidat .= "'".time()."')";

$res_insert_competence_candidat = $cnx->requeteData($req_insert_competence_candidat);

if (!$res_insert_competence_candidat) {
    $message  = 'Requête invalide : ' . mysql_error() ." ". mysql_errno(). "\n";
    $message .= "Requête complète : \n". $req_insert_competence_candidat;

    echo "false"." ".$message;
    die($message);
}
else{
    $return_succes .= ", competence candidat updated\n";
}

echo $return_succes;

$cnx->deconnect();

?>
