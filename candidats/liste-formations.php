<?php
/*
* liste-formations.php Created 9 févr. 2011 at 13:48:01 by flenoble under Ocv-NG
* $Id$
*/

$securite = new securite();
$contact = $_SESSION['contact'];

include_once ABSPATH.'includes/class/_init_liste_var.php';

if(!$sidx) $sidx =1;
$wh = "";
$searchOn = $securite->verif_GetPost ($_GET['_search']);


//echo "####".$searchfield."####";

//print_r($_POST);

if($searchOn=='true') {
    //echo "####";

    if($searchfield){
        $wh = conv_operator($searchfield,$searchoper,$searchstring);
        echo $wh;
    }
    else{
        $sarr = $_GET;
        foreach( $sarr as $k=>$v) {
            switch ($k) {
                case 'id':
                case 'reference_offre':
                    $wh .= " AND ".$k." LIKE '".$v."%' ";
                    break;
/*                case 'intitule_offre':
                    $wh .= " AND ".$k." LIKE '%".$v."%' ";
                    break;
                case 'raison_sociale_entreprise':
                    $wh .= " AND ".$k." LIKE '%".$v."%' ";
                    break;
                case 'nom_type_offre':
                    $wh .= " AND ".$param["table"]["type_offre"].".nom LIKE '%".$v."%' ";
                    break;
                case 'nom_contact':
                    $wh .= " AND ".$k." LIKE '%".$v."%' ";
                    break;
                case 'date_depot_offre':
                    $wh .= " AND ".$k." LIKE '%".$v."%' ";
                    break;
                case 'date_debut_offre':
                    $wh .= " AND ".$k." LIKE '%".$v."%' ";
                    break;
                case 'lieu_offre':
                    $wh .= " AND (lieu_offre LIKE '".$v."%' OR name_city like '".$v."%') ";
                    break;
                case 'name_province':
                    $wh .= " AND ".$k." LIKE '".$v."%' ";
                    break;
                case 'name_state':
                    $wh .= " AND (".$k." LIKE '".$v."%' OR region_offre like '".$v."%') ";
                    break;
                case 'name_country':
                    $wh .= " AND ".$k." LIKE '".$v."%' ";
                    break;*/
            }
        }
    }
}
//---------------------------------------------------------------------
    $cnx= new actionsdata();
    $cnx->connect();

    $req_liste_formations = "SELECT * FROM ".$param["table"]["formation"]." JOIN ";
    $req_liste_formations .= $param["table"]["suivreformation"]." ON ";
    $req_liste_formations .= $param["table"]["formation"].".id_formation=";
    $req_liste_formations .= $param["table"]["suivreformation"].".id_formation ";
    $req_liste_formations .= "WHERE ".$param["table"]["suivreformation"].".id_contact = ";
    $req_liste_formations .=  "'".$contact."' ";


    $req_liste_formations .= "ORDER BY ".$param["table"]["formation"].".$sidx $sord ";

    // $sql .= $param["table"]["entreprise"].id_contact='$contact' AND ";

// determine la pagination
    $pagination=$cnx->pagination($cnx,$req_liste_formations,1,$page,$limit);

    //var_dump($pagination);

// Filtre du nombre de lignes par pages
    $req_liste_formations .= "LIMIT ".$pagination['start']." , ".strval($pagination['limit']);

   //echo $req_liste_formations;

// prepare la requete à afficher avec la pagination
    $res_liste_formations=$cnx->requeteSelect ($req_liste_formations);


// construit les données qui seront affichées

    $responce->page = $pagination['page'];
    $responce->total = $pagination['total_pages'];
    $responce->records = $pagination['count'];

    $i=0;

if($res_liste_formations != 0) {
    foreach($res_liste_formations as $data) {
// 	 	 	 	 	 	 	 	updated_on
        $responce->rows[$i]['id']=$data['ID_FORMATION'];
        $responce->rows[$i]['cell']=array(
        $data['INTITULE_FORMATION'],
        $data['DATE_DEBUT_FORMATION'],
        $data['DATE_FIN_FORMATION'],
        $data['DIPLOME_FORMATION'],
        $data['LIEU_FORMATION'],
        $data['ECOLE_FORMATION'],
        substr(str_replace(array(chr(10),chr(13)), array(""," "), $data['DESCRIPTION_FORMATION']),0,20)." [...]",
        $data['created_on']);
        $i++;
    }

    $cnx->deconnect();
    echo json_encode($responce);
} else {
    $cnx->deconnect();
    echo 'aucun enregistrement';
}
?>
