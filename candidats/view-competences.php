<?php
/*
* view-competences.php Created 11 févr. 2011 at 15:21:34 by flenoble under Ocv-NG
* $Id$
*/

$cnx= new actionsdata();
$cnx->connect();

$req_select_competences = "SELECT ".$param["table"]["competence"];
$req_select_competences .= ".id_competence,nom_competence FROM ";
$req_select_competences .= $param["table"]["competence"]." " ;
$req_select_competences .= "JOIN ".$param["table"]["acquerircompetence"]." ON ";
$req_select_competences .= $param["table"]["competence"].".id_competence = ";
$req_select_competences .= $param["table"]["acquerircompetence"].".id_competence ";
$req_select_competences .= "WHERE ".$param["table"]["acquerircompetence"].".id_contact = ";
$req_select_competences .=  "'".$contact."' GROUP BY id_competence";

//echo $req_select_competences;

$req_notselect_competences = "SELECT ".$param["table"]["competence"];
$req_notselect_competences .= ".id_competence,nom_competence FROM ";
$req_notselect_competences .= $param["table"]["competence"]." ";
$req_notselect_competences .= "WHERE ".$param["table"]["competence"].".id_competence ";
$req_notselect_competences .=  "NOT IN  (".str_replace(",nom_competence", "", $req_select_competences).")";

//echo "<br/>".$req_notselect_competences;

$res_notselect_competences = $cnx->requeteSelect($req_notselect_competences);
$res_select_competences = $cnx->requeteSelect($req_select_competences);

/*$req_sel_annee_matieres = "SELECT id_annee_matiere,matieres.id,libelle FROM lst_annee_matiere,matieres WHERE idniveauscolaire=".$niveau_scolaire." ";
$req_sel_annee_matieres .= "AND annee_scolaire='".$annee_scolaire."' AND matieres.id=idmatiere";
$res_sel_annee_matieres = $cnx->requeteSelect($req_sel_annee_matieres);

$req_notsel_matieres = "SELECT id,libelle FROM matieres WHERE id NOT IN (SELECT idmatiere FROM lst_annee_matiere ";
$req_notsel_matieres .= "WHERE idniveauscolaire=".$niveau_scolaire." AND annee_scolaire='".$annee_scolaire."')";
$res_notsel_matieres = $cnx->requeteSelect($req_notsel_matieres);*/



?>
<script type="text/javascript" src="includes/js/ui.multiselect.js"></script>
<script type="text/javascript" src="includes/js/ui-multiselect-fr.js"></script>

<script type="text/javascript">

<?php
    require_once ("js/fonction_competences.js");
    require_once ("includes/js/form-fonctions.js");
    require_once ("includes/js/fonctionsjs.js");
?>
</script>

<?php
$form = new formulaire ();
$result = $form->form_init('form-addcompetences','redirecteur.php?dest=candidats_req-maj-listecompetences','POST','formulaire-fiche');
$result .= $form->fieldset_new();
$result .= $form->creer_text('add_competence', 'Ajouter une competence', 'no', 'text', '','');
$result .= $form->creer_bouton($param["action"]["add"],'bt_add','submit','button');
$result .= $form->fieldset_end();
$result .= $form->form_end();

$result .= $form->form_init('form-updatecompetences','redirecteur.php?dest=candidats_req-maj-competences','POST','');
//$result .= $form->fieldset_new();
$result .= "<select id='liste_competences' class='multiselect' multiple='multiple' name='competences[]'>";

    if( $res_select_competences != 0){
        foreach ($res_select_competences as $selected) {
            $result .= "<option value='".$selected['id_competence']."' selected='selected'>".$selected['nom_competence']."</option>\n";
        }
    }

    foreach ($res_notselect_competences as $notselected) {
        $result .= "<option value='ns-".$notselected['id_competence']."' >".$notselected['nom_competence']."</option>\n";
    }
$result .= "</select>";
//$result .= $form->creer_bouton($param["action"]["view"],'bt_view','button');

$result .= "<div id='bodycontent'></div>";


$result .= $form->creer_bouton($param["action"]["update"],'','submit','button');

$result .= $form->form_end();

echo $result;

?>