<h2 id="titre_page">Offres</h2>
<input id="pagelink" type="hidden" value="archives offres reference_offre offres"/> <!-- Répertoire table clef bouton-->

<script type="text/javascript">

	<?php    if($_SESSION["type"]==2 || $_SESSION["type"]==3)   { ?>

	    var popin_largeur = 1024;
	    var popin_hauteur = 500;
	    var popin_add_title = '<?php echo $param["action"]["add"];?>';
            var popin_update_title = '<?php echo $param["action"]["update"];?>';
            var popin_view_title = '<?php echo $param["action"]["view"];?>';
            var popin_duplicate_title = '<?php echo $param["action"]["duplicate"];?>';
            var popin_action='offres_maj-offres.php';
	    var popin_edition='offres_frm-offre.php';
            var popin_affiche='offres_view-tous.php';
	    var popin_add_title_update = "Modification une offre";
	    var popin_rem_title_update = '<?php echo $param["action"]["delete"];?>';
	    var largeurgrid = window.innerWidth * 0.95;
            var navgrid_options = {view:false,edit:false,add:false,del:false};
  //          var navgrid_options = {view:false,edit:false,add:false,del:false};
            var session = 23;
	<?php  } ?>
 // Nom des colonnes en haut du tableau

    var noms_colonnes = ['ID','Référence','Publié le','Entreprise','Intitulé','Ville','Département','Région','Pays','Type','Début le'];


 // contenu des colonnes

    var modele_colonnes = [
                      {name:'id_offre', index:'id_offre', width:50, align:'left', search:false, hidden:true},
                      {name:'reference_offre', index:'reference_offre', align:'left', width:30},
                      {name:'created_on', index:'created_on', align:'left', width:30},
                      {name:'raison_sociale_entreprise', index:'raison_sociale_entreprise', align:'left', width:40},
                      {name:'intitule_offre', index:'intitule_offre', align:'left', width:80},
                      {name:'lieu_offre', index:'lieu_offre', align:'left', width:50},
                      {name:'name_province', index:'name_province', align:'left', width:50},
                      {name:'name_state', index:'name_state', align:'left', width:50},
                      {name:'name_country', index:'name_country', align:'left', width:50},
                      {name:'nom_type_offre', index:'nom_type_offre', align:'left', width:30},
                      {name:'date_debut_offre', index:'date_debut_offre', align:'left', width:40},
                       ];

    var show_subgrid=false;
<?php

include_once(ABSPATH.'includes/js/fonctionsjs.js');
//include_once('js/fonction.js');
?>

</script>

<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
include_once (ABSPATH.'commun/liste.php');
?>
<!--div pour un éditeur personnalisé par exemple formulaire parent -->
<div id="formulairePerso">

</div>
