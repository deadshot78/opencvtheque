<h2 id="titre_page">Archives contacts</h2>
<input id="pagelink" type="hidden" value="archives contacts-partenaire id_contact_partenaire bouton"/> <!-- Répertoire table clef bouton-->

<script type="text/javascript">

	<?php    if($_SESSION["type"]==2 || $_SESSION["type"]==3)   { ?>

	    var popin_largeur = 1024;
	    var popin_hauteur = 500;
	    var popin_add_title = '<?php echo $param["action"]["add"];?>';
            var popin_update_title = '<?php echo $param["action"]["update"];?>';
            var popin_view_title = '<?php echo $param["action"]["view"];?>';
            var popin_duplicate_title = '<?php echo $param["action"]["duplicate"];?>';
            var popin_action='./entreprises/frm.php';
	    var popin_edition='./entreprises/frm.php';
            var popin_affiche='./entreprises/frm.php';
	    var popin_add_title_update = "Modification une offre";
	    var popin_rem_title_update = '<?php echo $param["action"]["delete"];?>';
	    var largeurgrid = window.innerWidth * 0.95;
            var navgrid_options = {view:false,edit:false,add:false,del:true};
  //          var navgrid_options = {view:false,edit:false,add:false,del:false};
            var session = 23;
	<?php  } else { ?>

	var popin_largeur = 1024;
	var popin_hauteur = 500;
	var popin_view_title = '<?php echo $param["action"]["view"];?>';
	var popin_affiche='./offres/view-tous.php';
  	var largeurgrid = window.innerWidth * 0.95;
        var navgrid_options = {view:false,edit:false,add:false,del:false};
        var session=1;
  	<?php } ?>



// Nom des colonnes en haut du tableau
    var noms_colonnes = ['Entreprise','Ville','Tel','Fax','Email','NAF'];


// contenu des colonnes
    var modele_colonnes = [
                           {name:'entreprise', index:'entreprise', align:'left', editable:false, sortable:true, width:50},
                           {name:'ville', index:'ville', align:'left', editable:false, sortable:false, width:40},
                           {name:'tel', index:'tel', align:'left', editable:false, sortable:false, search:false,width:30},
                           {name:'fax', index:'fax', align:'left', editable:false, sortable:false, search:false,width:30},
                           {name:'email', index:'email', align:'left', editable:false, sortable:false, search:false,width:50},
                           {name:'naf', index:'naf', align:'left', editable:false, sortable:false, width:20},
                           ];

 var show_subgrid = false;
<?php

include_once (ABSPATH.'includes/js/fonctionsjs.js');
?>

</script>

<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
include_once (ABSPATH.'commun/liste.php');
?>
<div id="formulairePerso">

</div>
