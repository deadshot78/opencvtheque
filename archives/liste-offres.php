<?php

$securite = new securite();


$contact = $_SESSION['contact'];
$page = $_POST['page']; // get the requested page
$limit = $_POST['rows']; // get how many rows we want to have into the grid

//-- Partie Pagination
$page = $securite->verif_GetPost ($_GET['page']);	// numero de la page 
$limit = $securite->verif_GetPost ($_GET['rows']);  // nombre de lignes à afficher
//-- fin Partie Pagination


$sidx = $_GET['sidx']; // get index row - i.e. user click to sort
$sord = $_GET['sord']; // get the direction

$searchfield = $_GET['searchField']; // get the field
$searchstring = $_GET['searchString']; // get the string
$searchoper = $_GET['searchOper']; // get the operator

if(!$sidx) $sidx =1;
$wh = "";
$searchOn = $_GET['_search'];


//echo "####".$searchfield."####";

//print_r($_POST);

if($searchOn=='true') {
    //echo "####";

    if($searchfield){
        $wh = conv_operator($searchfield,$searchoper,$searchstring);
        echo $wh;
    }
    else{
        $sarr = $_GET;
        foreach( $sarr as $k=>$v) {
            switch ($k) {
                case 'id':
                case 'reference_offre':
                    $wh .= " AND ".$k." LIKE '".$v."%' ";
                    break;
                case 'intitule_offre':
                    $wh .= " AND ".$k." LIKE '%".$v."%' ";
                    break;
                case 'raison_sociale_entreprise':
                    $wh .= " AND ".$k." LIKE '%".$v."%' ";
                    break;
                case 'nom_type_offre':
                    $wh .= " AND ".$param["table"]["type_offre"].".nom LIKE '%".$v."%' ";
                    break;
                case 'nom_contact':
                    $wh .= " AND ".$k." LIKE '%".$v."%' ";
                    break;
                case 'created_on':
                    $wh .= " AND ".$k." LIKE '%".$v."%' ";
                    break;
                case 'date_debut_offre':
                    $wh .= " AND ".$k." LIKE '%".$v."%' ";
                    break;
                case 'lieu_offre':
                    $wh .= " AND (lieu_offre LIKE '".$v."%' OR name_city like '".$v."%') ";
                    break;
                case 'name_province':
                    $wh .= " AND ".$k." LIKE '".$v."%' ";
                    break;
                case 'name_state':
                    $wh .= " AND (".$k." LIKE '".$v."%' OR region_offre like '".$v."%') ";
                    break;
                case 'name_country':
                    $wh .= " AND ".$k." LIKE '".$v."%' ";
                    break;
            }
        }
    }
}
//---------------------------------------------------------------------
    $cnx= new actionsdata();
    $cnx->connect();

    $req_liste_offres = "(SELECT id_offre,reference_offre,created_on,intitule_offre,lieu_offre, ";
    $req_liste_offres .= "name_city,name_state,name_country,name_province,".$param["view"]["localisation"].".id_city, ";
    $req_liste_offres .= "nom as nom_type_offre,raison_sociale_entreprise,date_debut_offre,".$param["table"]["offre"].".id_city as city_offre ";
    $req_liste_offres .= "FROM ".$param["table"]["offre"]." LEFT JOIN ".$param["view"]["localisation"]." ON ".$param["table"]["offre"].".id_city=".$param["view"]["localisation"].".id_city";
    $req_liste_offres .= ", ".$param["table"]["type_offre"];

    $req_creator_offres = ",".$param["table"]["entreprise"];
    $req_creator_offres .= " WHERE ".$param["table"]["offre"].".id_contact = ";
    $req_creator_offres .= $param["table"]["entreprise"].".id_contact ";
    $req_creator_offres .= "AND ".$param["table"]["offre"].".offre_partenaire=0 ";


    switch($_SESSION['type']){
        case 3: //Admin

        case 2: // Entreprises
                $req_partenaire_offres = ",".$param["table"]["entreprises_partenaire"];
                $req_partenaire_offres .= " WHERE ".$param["table"]["offre"].".id_entreprise_partenaire = ";
                $req_partenaire_offres .= $param["table"]["entreprises_partenaire"].".id ";
                $req_reader_offres = "AND ".$param["table"]["offre"].".statut_offre=2 ";
                $req_reader_offres .= "AND ".$param["table"]["type_offre"].".id=".$param["table"]["offre"].".type_offre ";
                $req_reader_offres .= "AND ".$param["table"]["offre"].".id_contact=".$_SESSION['contact']." ";
            break;
    }


    $req_union_offres .= $req_liste_offres.$req_partenaire_offres.$req_reader_offres.$wh;
    $req_union_offres .= ") UNION ";
    $req_union_offres .= $req_liste_offres.$req_creator_offres.$req_reader_offres.$wh;
    $req_union_offres .= ") ";
    //$sql .= "GROUP BY id_offre,reference_offre,date_depot_offre,intitule_offre,lieu_offre, ";
    //$sql .= "type_offre,date_debut_offre,entreprise ";
	
    $req_union_offres .= "ORDER BY $sidx $sord ";

    // $sql .= $param["table"]["entreprise"].id_contact='$contact' AND ";

// determine la pagination
    $pagination=$cnx->pagination($cnx,$req_union_offres,$start,$page,$limit);

// Filtre du nombre de lignes par pages
    $req_union_offres .= "LIMIT ".$pagination['start']." , ".strval($pagination['limit']);

//   echo $req_union_offres;

// prepare la requete à afficher avec la pagination
    $row=$cnx->requeteSelect ($req_union_offres);
   		
   		
// construit les données qui seront affichées
    $responce->page = $pagination['page'];
    $responce->total = $pagination['total_pages'];
    $responce->records = $pagination['count']; 
	
    $i=0;
    if($row != 0) {
        foreach($row as $data) {

            if($data['city_offre']!=0){

                //$data_localisation=get_localisation($data['city_offre'],$param);

                $city=$data['name_city'];
                $province=$data['name_province'];
                $state=$data['name_state'];
                $country=$data['name_country'];

            }
            else{
                $city=$data['lieu_offre'];
                $province='';
                $state=$data['region_offre'];
                $country='';
            }

            $responce->rows[$i]['id']=$data['id_offre'];
            $responce->rows[$i]['cell']=array(
            $data['id_offre'],
            $data['reference_offre'],
            date($data['date_depot_offre'],"d-m-Y"),
            $data['raison_sociale_entreprise'],
            ucwords(strtolower($data['intitule_offre'])),
            ucwords(strtolower($city)),// Ville
            ucwords(strtolower($province)),	// Departement
            ucwords(strtolower($state)),	// Region
            ucwords(strtolower($country)),	// Pays
            $data['nom_type_offre'],
            conv_date($data['date_debut_offre'])
            );
            $i++;
        }
        
        $cnx->deconnect();
	echo json_encode($responce);


} else {
    $cnx->deconnect();
    echo 'aucun enregistrement';
}
?>
