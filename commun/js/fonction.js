        $(function(){


        //Gestion du formulaire
        $("#creation-form").dialog({
			autoOpen: false,
			height: 685,
			width: 630,
			modal: true,
            open: function(){
                $('#fieldsociete').hide();
            },
            close: function() {
                $(":input[type=text]").val('').removeClass('ui-state-error');
            }

        });

        //apppel du formulaire de creation de compte
		$('#create-user')
			.button()
			.click(function() {
				$('#creation-form').dialog('open');
                return false;
			});


            $("#radio_civilite,#radio_type").buttonset();

            $("#formcompte").Fvalidate();
            $("#formlogin").Fvalidate();

            $("#frm-submit").attr("disabled", "disabled");

            //Spécificité du formulaire
            $("input[name=confmdp-contact]").sameAs($("input[name=mdp-contact]"));
            $("input[name=prenom-contact]").rule(/^[a-zA-Z-\s-\'éèàçôöïîêôëüû]*$/);
            $("input[name=tel-contact]").rule(/^[0-9\s\(\)\+]*$/);
            $("input[name=fax-contact]").rule(/^[0-9\s\(\)\+]*$/);

            $("input[name=type]").click(function(){
                if(this.value==1){
                    $('#fieldsociete').hide();
                    if($('#creation-form').height()>700){
                        $('#creation-form').height($('#creation-form').height()-150);
                    }
                    $("#raison-sociale").removeAttr("require");
                    $("#secteur-activite").removeAttr("require");
                    $("#fonction-contact").removeAttr("require");
//                    $("#formcompte").Fvalidate();
                }
                else{
                    $('#creation-form').height($('#creation-form').height()+150);
                    $("#raison-sociale").attr("require", "yes");
                    $("#secteur-activite").attr("require", "yes");
                    $("#fonction-contact").attr("require","yes");
//                    $("#formcompte").Fvalidate();
                    $('#fieldsociete').show();

                }
            });

            //Vérification du nom en majuscules
            $('#nom-contact').blur(function(){
                this.value=(this.value.toUpperCase());
                $("input[name=nom-contact]").rule(/^[A-Z-\s-\']*$/);
                $(this).removeClass('inp-error');
            });

            $('#email-contact').blur(function(){
                    $.post('redirecteur.php?dest=commun_verif-mail',{
                            'email':this.value},
                            function(data){
                                if(data==0){
                                    $('#email-contact').removeClass('inp-error');
                                    $('#email-contact').addClass('inp-focus');
                                    $('#email-contact').next('.err-msg').hide();
                                }
                                else{
                                    $('#email-contact').removeClass('inp-focus');
                                    $('#email-contact').addClass('inp-error');
                                    $('#email-contact').next('.err-msg').show();
                                }
                            })
            });

            //Vérification du Captcha
            $("#defaultReal").keyup(function(){
                if(this.value.length>5){
                    $.post('redirecteur.php?dest=commun_verif-captcha',{
                            'realPerson':this.value,
                            'realPersonHash':$("input[name=defaultRealHash]").val()},
                            function(data){
                                if(data==0){
                                    $("#frm-submit").removeAttr("disabled");
                                }
                                else{
                                    $("#frm-submit").attr("disabled", "disabled");
                                }
                            })
                }
                else{
                    $("#frm-submit").attr("disabled", "disabled");
                }
            });

        $("#defaultReal").realperson({regenerate:'<?=$param["creation"]["capchange-contact"]?>'});
        //$("#creation-form").dialog({title:<?=$param["creation"]["titre"]?>});
        $("#creation-form").dialog( "option", "title", '<?=$param["creation"]["titre"]?>' );

        function autocomplete_location(id_city,city,cp,province,state,country)
        {
        //alert('test');
                        if(!state){
                            var state='#state';
                        }
                        if(!country){
                            var country='#country';
                        }
                        if(!province){
                            var province='#province';
                        }

                        $(city).autocomplete({


                                source: "redirecteur.php?dest=ajax-scripts_req-location&searchField=name_city", //element correspond au champ de selection dans la table

                                focus: function(event, ui){
                                        $(city).val(ui.item.value);
                                        return false;
                                },
                                minLength: 2,
                                select: function(event, ui) {
                                        $(id_city).val(ui.item.id);
                                        $(city).val(ui.item.value);
                                        $(cp).val(ui.item.both.cp);
                                        if($(state)){
                                            $(state).val(ui.item.both.state);
                                        }
                                        if($(country)){
                                            $(country).val(ui.item.both.country);
                                        }
                                        if($(province)){
                                            $(province).val(ui.item.both.province);
                                        }

                                        return false;


                                    }
                        });

                        $(cp).autocomplete({

                                source: "redirecteur.php?dest=ajax-scripts_req-location&searchField=cp", //element correspond au champ de selection dans la table

                                focus: function(event, ui){
                                        $(city).val(ui.item.value);
                                        return false;
                                },
                                minLength: 2,
                                select: function(event, ui) {
                                        $(id_city).val(ui.item.id);
                                        $(city).val(ui.item.value);
                                        $(cp).val(ui.item.both.cp);
                                        if($(state)){
                                            $(state).val(ui.item.both.state);
                                        }
                                        if($(country)){
                                            $(country).val(ui.item.both.country);
                                        }
                                        if($(province)){
                                            $(province).val(ui.item.both.province);
                                        }

                                        return false;


                                    }
                        });



        }


        autocomplete_location('#id_city','#ville-contact','#cp-contact','','','#pays-contact');

        });