<?php

$login = "";
$last_action = "";
$page = "";

if(isset($_SESSION['login'])) $login = $_SESSION['login'];
if(isset($_SESSION['last_action'])) $last_action = $_SESSION['last_action'];
if(isset($_SESSION['page'])) $page = $_SESSION['page'];

/*echo $login."<br/>";
echo $last_action."<br/>";
echo $page."<br/>";*/

if(sha1('infosessions'.session_id().$login.$last_action) != $page) {
    session_destroy();
    echo "<meta http-equiv='refresh' content='0; URL=.'>";
} else {
    unset($_SESSION['page']);
    $_SESSION['page'] = 'nopage';
    //echo $_SESSION['page']."<br/>";

    $path = session_save_path();
    echo "Cache type : ".session_cache_limiter()." | Délais expiration : ".session_cache_expire();
    $strhtml = "<table border='1'><thead><th>Heure connexion</th><th>Dernière action</th><th>IP</th><th>Login</th><th>Utilisateur</th><th>Service</th><th>Fichier</th><th>Actions</th></thead><tbody>";
    $folder = opendir($path."/");

    if(isset($_GET['delete'])){
        $retour = unlink($path."/".$_GET['delete']);
    }

    $tab_session = array();
    $i=0;
    while ($file = readdir($folder)) {
      if ($file != "." && $file != ".." && $file != "index.php") {
        $tab_session[$i]['heureconnexion']=date("d-m-Y H:i:s ",filemtime($path."/".$file));
        $tab_session[$i]['fichier']=$file;

        $lines = fopen($path."/".$file,"r");
        while ($line=fgets($lines)) {
            print_r(session_decode($line));
            $field=explode(";",$line);
            foreach($field as $session_info){
                $tab_session[$i][substr($session_info,0,strpos($session_info, "|"))]=str_replace('"', '', substr(strrchr($session_info, ":"),1));

            }
        }
        fclose($lines);
        $i++;
      }
    }
    closedir($folder);

    foreach($tab_session as $session){
        if(isset($session['login'])){
            $strhtml .= "<tr><td>".$session['heureconnexion']."</td>";
            $strhtml .= "<td>".date("d-m-Y H:i:s ",$session['last_action'])."</td>";
            $strhtml .= "<td>".$session['remote']."</td>";
            $strhtml .= "<td>".$session['login']."</td><td>".$session['contact']."</td>";
            $strhtml .= "<td>".$session['type']."</td><td>".$session['fichier']."</td>";
            $strhtml .= "<td><a class='ui-icon ui-icon-trash' href='commun/infosessions.php?delete=".$session['fichier']."' title='supprimer la session de ".$session['contact']." '></a></td></tr>";
        }
    }

    $strhtml .= "</tbody></table>";
    echo $strhtml;
    $_SESSION['page'] = 'nopage';
}
?>