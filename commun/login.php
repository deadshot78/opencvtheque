<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
//session_start() ;
/*require_once ('includes/class/requetes.class.php');
require_once ('includes/class/requetes.class.php');
require_once ('includes/class/listes.class.php');
require_once ('includes/class/securite.class.php');
require_once ('includes/class/_inc_function.php');
require_once ('includes/class/formulaire.class.php');*/
$app_path =  constant("ABSPATH");
require_once $app_path.'/conf/_connect.php';
?>

<script type="text/javascript">
    <?php require_once ("js/fonction.js"); ?>
</script>

<div id="actu">

</div>
<div id="login">
<h1><a href="#" >Cvtheque</a></h1><br/>
<?php include_once $app_path.'/conf/version.txt';?>
<!--
<p class="message">
Vous êtes désormais déconnecté(e).
<br/>
</p>
-->
<?php
    if(isset ($_SESSION["erreur_login"])){
        echo $_SESSION["erreur_login"];
    }
?>
<br/>
<form id="formlogin" name="formlogin" action="redirecteur.php?dest=commun_req-login" method="post">
	<p>
		<label><?php echo $param["login"]["identifiant"]?> *<br />
		<input type="text" name="log" id="user_login" value="<?php
                if(isset ($_SESSION["login"])){
                    echo $_SESSION["login"];
                }
                ?>" require="yes" rule="email" minlength="5"  maxlength="50" tabindex="2" /></label>
	</p>
	<p>
		<label><?php echo $param["login"]["mdp"]?> *<br />
		<input type="password" name="pwd" id="user_pass" value="" require="yes" value="" minlength="5"  maxlength="40" tabindex="4" /></label>
	</p>
    <p class="forgetmenot"><label><input name="rememberme" type="checkbox" id="rememberme" value="forever" tabindex="6" /><?php echo $param["login"]["rememberme"]?></label></p>
    <p class="create">
		<input type="submit" name="wp-submit" id="wp-submit" class="button-primary" value="<?php echo $param["login"]["connect"]?>" tabindex="8" />
		<button name="create-user" id="create-user" tabindex="9"  ><?php echo $param["login"]["creecompte"]?></button>
        <input type="hidden" name="redirect_to" value="./index.php" />
		<input type="hidden" name="testcookie" value="1" />

	</p>
</form>
</div>

<!-- //// -->
<div id="creation-form">
<?php
$form = new formulaire ();


$result = $form->form_init('formcompte','redirecteur.php?dest=commun_req-creation','POST','');
$result .= $form->creer_radio('type',$param["creation"]["whois"],'yes',$param['typecontact'],'','type','radio','',11);

//$result .= $form->creer_une_checkbox('offre_partenaire','Partenaire','Offre partenaire','no',1,$data_offre['offre_partenaire'],'',2);

//$result .= $form->creer_text('action','action','no','hidden',$action,'','',2,60,'',0);

// Entreprise
$result .= $form->fieldset_new($param["creation"]["societe"],"fieldsociete");
$result .= $form->creer_text('raison-sociale',$param["creation"]["raison_sociale"],'yes','text','','','',0,50,'','',10);
$result .= $form->creer_select('secteur-activite',$param["creation"]["secteurs"],
            'yes','includes/listes/sel-secteurs-'.$Langue.'.xml',TRUE,'','','',15,TRUE,FALSE,'contact');
$result .= $form->creer_text('fonction-contact',$param["creation"]["fonction-contact"],'yes','text','','','alpha',0,40,'','',10);
$result .= $form->creer_text('site-web',$param["creation"]["web"],'no','text','','','',0,60,'','',10);
$result .= $form->creer_text('fax-contact',$param["creation"]["fax-contact"],'no','text','','','',0,60,'','',10);
$result .= $form->fieldset_end();

// Contact détail
$result .= $form->fieldset_new($param["creation"]["contact"],"fieldcontact");
//$result .= $form->creer_radio('civilite_contact_partenaire','Civilite','no',$param['civilite'],$data_offre['civilite_contact'],'civilite_contact_partenaire','radio','',21,$disable);
$result .= $form->creer_radio('civilite','Civilité','yes',$param['civilite'],'','civilite','radio','',21,'');
$result .= $form->creer_text('nom-contact',$param["creation"]["nom-contact"],'yes','text','','','alpha',2,60,'','',10);
$result .= $form->creer_text('prenom-contact',$param["creation"]["prenom-contact"],'yes','text','','','alpha',2,60,'','',10);
$result .= $form->creer_text('adresse1-contact',$param["creation"]["adresse1-contact"],'yes','text','','','alnumhyph',2,60,'','',10);
$result .= $form->creer_text('adresse2-contact',$param["creation"]["adresse2-contact"],'no','text','','','alnumhyph',0,60,'','',10);
$result .= $form->creer_text('id_city','','yes','hidden','','','numeric',0,14);
$result .= $form->creer_text('cp-contact',$param["creation"]["cp-contact"],'yes','text','','','alnumhyph',2,10,'','',10);
$result .= $form->creer_text('ville-contact',$param["creation"]["ville-contact"],'yes','text','','','',2,70,'','',10);
$result .= $form->creer_text('pays-contact',$param["creation"]["pays-contact"],'yes','text','','','alpha',2,70,'','',10);
$result .= $form->creer_text('tel-contact',$param["creation"]["tel-contact"],'yes','text','','','alnumhyph',5,17,'','',10);
$result .= $form->fieldset_end();

$result .= $form->fieldset_new($param["creation"]["emailmdp"],"fieldemail");
$result .= $form->creer_text('email-contact',$param["creation"]["email-contact"],'yes','text','','','email',5,60,'','',10,$param["erreur"]["email"]);
$result .= $form->creer_text('mdp-contact',$param["creation"]["mdp-contact"],'yes','password','','','',5,20,'','',10);
$result .= $form->creer_text('confmdp-contact',$param["creation"]["confmdp-contact"],'yes','password','','','',5,20,'','',10,$param["creation"]["err-mdp"]);
$result .= $form->fieldset_end();

$result .= $form->creer_captcha('captcha-contact', $param["creation"]["captcha-contact"], 'yes');
$result .= $form->creer_bouton($param["action"]["create"],'frm-submit','submit','button');

$result .= $form->form_end();

echo $result;


?>
<!-- /////  -->
</div>












