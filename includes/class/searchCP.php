<?php
require_once("../class/_inc_function.php");
require_once('../../conf/_connect.php');
require_once('../class/requetes.class.php');
require_once('../class/listes.class.php');

if(isset($_GET["term"]) && !empty($_GET["term"]) && !is_null($_GET["term"]))
{
  $listesVilles = new actionsdata();
  $listesVilles->connect();
  $ville = $listesVilles->securite($_GET["term"]);
  $ville = preg_replace("/\s/","-",$ville);
  //echo $ville;
  $sql = "SELECT id,cp,name_city_uppercase
          FROM ville
          WHERE name_city_uppercase LIKE '".trim(strtoupper($ville))."%'
          ORDER BY name_city_uppercase  LIMIT 30 ";
  //echo $sql ;
  //die();
  $resultVille = $listesVilles->requeteSelect($sql);
  $listesVilles->deconnect();
  if(count($resultVille)==0)
     $resultVille[0] = array("id"=>"0","libelle"=>"pas défini");

    foreach($resultVille as $value)
    {
     $json = array();
     $json['id'] = $value['id'];
     $json['value'] = substr($value['cp'],0,5)." ".$value['name_city_uppercase'];
     $json['ville'] = $value['name_city_uppercase'];
     $json['cp'] = substr($value['cp'],0,5);
     $data[] = $json;
    }
    header("Content-type: application/json");
    echo json_encode($data);
}

