<?php
class Errors
{
	private $values = array();
	private $form = '';
	private $list_errors = array();
        private $list_errors_id = array();
	private $exist = false;
	private $nb_errors = 0;

	/**
	 * Constructeur de la class, spécifie les valeurs du formulaire
	 *
	 * @param array $values
	 * @param string $form
	 */
	public function __construct($values, $form)
	{
		$this->values = $values;
		$this->form = $form;
	}

	/**
	 * Ajout l'erreur a $this->list_error
	 *
	 * @param string $error
	 * @param mixed $values
	 * @param string $type
	 */
	public function add($error, $values, $type)
	{
		/*** modififé le  06/09/2010  *****/
                //sauvegardé afin de connaitre le nom du champs ou (l id)
                $old_values = $values;
                
                if (is_array($values)) {
			$value_bis = array();
                        $old_values = $values[0];

			foreach ($values as $value) {
				$value_bis[] = $this->values[$value];
			}

			$values = $value_bis;
		}
		else
		{
			if (isset($this->values[$values])) {
				$values = $this->values[$values];
			}
		}

		if (!Checking::$type($values)) {
			$this->list_errors[] = $error;
                        $this->list_errors_id[] =  $old_values;
			$this->exist = true;
			$this->nb_errors++;
		}
	}

	/**
	 * Génère l'affichage des erreurs, assigne les valeurs au formulaire et vérifie qu'il est bien valide
	 *
	 * @return bool
	 */
	public function valid()
	{
		if ($this->exist) {
			$_SESSION[$this->form]['form_errors'] = $this->list_errors;
                        $_SESSION[$this->form]['form_errors_id'] = $this->list_errors_id;

			$_SESSION[$this->form]['form_values'] = $this->values;

			return false;
		}

		return true;
	}

	public function __get($name)
	{
		if (property_exists($this, $name)) {
			return $this->$name;
		}
	}
}
?>
<?php
/*$_SESSION['test']['form_values'] = array();
$_SESSION['test']['form_errors'] = array();

if (isset($_POST['input_submit'])) {
	$errors = new Errors($_POST, 'test');

	$errors->add('Le champ ne doit pas être vide.', 'input1', 'notEmpty');
	$errors->add('Le champ ne doit pas être égale à zéro.', 'input2', 'notZero');
	$errors->add('Les valeurs doivent être identique.', array('input3', 'input4'), 'isEqual');

	// Si le forumaire est valide
	if ($errors->valid()) {
		echo 'Le formulaire est valide<br />';
	}
}

if (!empty($_SESSION['test']['form_errors'])) {
	$errors_values = $_SESSION['test']['form_errors'];

	foreach ($errors_values as $value) {
		echo $value . '<br />';
	}
}*/
?>

<?/*<!-- <form action="">
	<p>
		Input 1 : <input type="text" name="input1" value="<?php echo $_SESSION['test']['form_values']['input1']; ?>" /><br />
		Input 2 : <input type="text" name="input2" value="<?php echo $_SESSION['test']['form_values']['input2']; ?>" /><br />
		Input 3 : <input type="text" name="input3" value="<?php echo $_SESSION['test']['form_values']['input3']; ?>" /><br />
		Input 4 : <input type="text" name="input4" value="<?php echo $_SESSION['test']['form_values']['input4']; ?>" /><br />
		<input type="submit" name="input_submit" value="Envoyer" />
	</p>
</form> -->*/?>