<?php
/* 
 * Important
 * Constructeur qui mémorisent le nombre de colonnes pour un tableau
 * Les lignes commencent par --> sont des sous functions
-----------------------------------------------------
 * Liste des fonctions disponibles : 
creer_form($form,$nom_champs,$valeurs,$labels)
form_init($nomform,$actionform,$methodform,$class='')
form_end()
tableau_new($border=0,$class='')
tableau_end ()
tr_init($class='')
tr_end()
 --> td_init($val='')
 --> td_end()
colonne_new($val)
colonne_end()	
fieldset_new($legend='')
fieldset_end()
 --> creer_legend($legend)
creer_label($int_champ,$nom,$require)
creer_text($int_champ,$nametext,$require='NO',$type,$variable,$class='',$rules='',$minlength='',$maxlength='',$tabindex='')
creer_date($int_champ,$nametext,$require='NO',$variable,$class='',$tabindex='')
creer_radio($int_champ,$nametext,$require,$valeur_case,$ichecked,$name,$type,$tabindex='')
creer_checkbox($int_champ,$nametext,$require,$valeur_case,$ichecked,$name,$type,$tabindex='')
creer_une_checkbox($name,$nametext,$require,$value='on',$ischecked=false,$tabindex='')
creer_textarea($int_champ,$nametext,$require,$name,$value_text,$require='FALSE',$rules='',$minlength='',$maxlength='',$tabindex='')
creer_select($int_champ,$nametext,$require,$xmvalue,$isfile=TRUE,$selected='',$tabindex='')
creer_photo($int_champ,$nametext,$require,$pathphoto,$updatable='no',$class='photo')
Creer_bouton($value='insert',$name='bt',$type='submit',$class = "")
creer_adresse($valeur_adresse,$name,$id)
affiche_text($nametext,$value,$class='civilite')
affiche_radio($nametext,$value,$selected,$class='civilite')
affiche_checkbox($nametext,$value,$selected,$class='civilite')
affiche_select($nametext,$value='',$fichier='',$selected,$class='civilite')


*/


// Nombre de colonne pour le tableau
define ("COLONNE_TABLEAU",2);


class formulaire {

	public function __construct($colonne_tableau=COLONNE_TABLEAU)
	{
	 	$this->colonneMaxi=$colonne_tableau;
	}
	

public function creer_form($form,$nom_champs,$valeurs,$labels)
{
   /* fonction qui permet de générer les formulaires en fonction d'un tableau
     * creer_form($form,$nom_champs,$valeurs,$labels)
     * $form = Tableau contenant les différentes valeurs du formulaire (OBLIGATOIRE)
     *
     *      - nomform => nom du formulaire (OBLIGATOIRE),
     *      - actionform => Page exécuté lors de l'envoi du formulaire,
     *      - methodform => GET ou POST
     * $nom_champs = tableau qui contient la liste des champs et leurs options (OBLIGATOIRE)
     *      - fieldlegend => Légende du fieldset des champs
     *      - nom => nom du champs (IDENTIQUE BASE SQL ou vide dans le cas du fieldset ou de la photo)
     *      - type => text,photo,radio,select
     *      - value => valeur de l'option si différent de text (lien photo, label radio, liste select)
     *      - updatable => yes/no (OBLIGATOIRE si type=photo)
     *      - require => yes/no définit si le champs est obligatoire ou non
     *      - rules => type de champs attendu alpha,numeric,floating,alnum,alnumhyph,email
     *      - minlength => longueur minimum attendue
     *      - maxlength => longueur maximum attendue
     *      - class => si la zone à une class particulière
     *      - formatdate => si la zone est une date yes/no
     * $valeurs = valeurs résultantes d'un MYSQL_FETCH_ASSOC (OBLIGATOIRE)
     * $labels = tableau avec la liste des traductions des labels (OBLIGATOIRE)
     * $id = est le nom de la valeur id de l'input
     *
     */


        $htcode = "";
        $tabindex=1;
        $nom_champ='nom';
        $class_champ='class';
        $format_date_champ = 'formatdate';
        $type_champ='type';
        $value_champ='value';
        $champ_require='require';
        $size = "";
        

        $champ=array();

		$htcode .= $this->form_init($form['nomform'],$form['actionform'],$form['methodform'],$form['class']);

        foreach($nom_champs as $fieldset){
            if(array_key_exists('fieldlegend', $fieldset)){
                $htcode .= $this->fieldset_new($fieldset['fieldlegend']);
            }
            
            foreach($fieldset as $champ ){
                $int_champ="";
                if(is_array($champ)){
                $int_champ=str_replace('_','-',$champ[$nom_champ]);
                if($champ[$type_champ]!='photo' && $champ[$class_champ]!='')
                {
                    $htcode .= "<span class='".$champ[$class_champ]."'>\n";
                }

                //Remet la date dans le bon sens
                if(key($champ)=='$format_date_champ'){
                    if($champ[$format_date_champ]=='yes'){
                        $valeurs[$champ[$nom_champ]]=strftime("%d/%m/%Y",strtotime($valeurs[$champ[$nom_champ]]));
                    }
                }

			switch ($champ[$type_champ]){
case $champ[$type_champ]=='hidden':
	$variable=$valeurs[$champ[$nom_champ]];
	$htcode .= $this->creer_text($int_champ,'',$champ['require'],
	$champ[$type_champ],$variable,'',$champ['rules'],$champ['minlength'],$champ['maxlength'],$size,$champ['readonly'],$tabindex);
break;
case $champ[$type_champ]=='text':
	$variable=$valeurs[$champ[$nom_champ]];
	$htcode .= $this->creer_text($int_champ,$labels[$form['nomform']][$int_champ],$champ['require'],
	$champ[$type_champ],$variable,$champ['class'],$champ['rules'],$champ['minlength'],$champ['maxlength'],$size,$champ['readonly'],$tabindex);
break;
case $champ[$type_champ]=='radio' :
                          //creer_radio($int_champ,$nametext,$require,$valeur_case,$ichecked,$name,$type='radio',$class='',$tabindex='')
	$htcode .= $this->creer_radio($int_champ,$labels[$form['nomform']][$int_champ],$champ['require'],
                         	$champ[$value_champ],$valeurs[$champ[$nom_champ]],$int_champ,$champ[$type_champ],$champ['class'],$tabindex);
break;
case  $champ[$type_champ]=='checkbox':
	$htcode .= $this->creer_checkbox($int_champ,$labels[$form['nomform']][$int_champ],$champ['require'],
                         	$champ[$value_champ],$valeurs[$champ[$nom_champ]],$int_champ,$champ[$type_champ],$champ['class'],$tabindex);
                    break;
case 'select': 
	$htcode .= $this->creer_select($int_champ,$labels[$form['nomform']][$int_champ],$champ['require'],$champ[$value_champ],TRUE,$valeurs[$champ[$nom_champ]],$size,'',$tabindex);
                    break;
case 'photo':
	$htcode .= $this->creer_photo($int_champ,'',$champ['require'],
               			$champ[$value_champ],$champ['updatable'],$champ['class']);
break;
case 'textarea':
	$htcode .= $this->creer_textarea($int_champ,$labels[$form['nomform']][$int_champ],
                           	$int_champ,$valeurs[$champ[$nom_champ]],$champ['require'],$champ['rules'],$champ['minlength'],
                           	$champ['maxlength'],$tabindex);

                           
                    break;

                }

                if($champ[$type_champ]!='photo')
                {
                     $htcode .= "</span>\n";
                }
                $tabindex++;
            }
        }
			 $htcode .= $this->fieldset_end();
        }

		$htcode .= $this->creer_bouton($form['submit']);

		$htcode .= $this->form_end();

        return $htcode;
    }


	

    function form_init($nomform,$actionform,$methodform,$class='')
    {
/*
$nomform : Nom du formulaire
$actionform: chemin d'execution
$methodform : GET ou POST
$class : style CSS
*/
    	$string = "<form  ";
    	$string .= "id='".$nomform."' ";
    	$string .= "name='".$nomform."' ";
    	$string .= "action='".$actionform."' ";
    	$string .= "method='".$methodform."' ";
    	if ($class!='') $string .= "class='".$class."' ";
    	$string .= ">\n";
    	
    	return $string;
    	
    }
    
    function form_end()
    {
/*
Fin du formulaire
*/
    	$string = "</form>\n";
   	
    	return $string;
    	
    }    

function tableau_new($border=0,$width='100%',$class='')
{
/*
$border : 1 = affiche une bordure
$class : Style en CSS
*/
	$string = "<table ";
	$string .= "border = '".$border."' ";
	if ($class!="") 	$string .= "class='".$class."' ";
	$string .= " width = '".$width."' ";
	$string .= ">";
	return $string;
}
function tableau_end ()
{
/*
fin du tableau
*/
	$string = "</table>";
	return $string;	
}

function tr_init($align='',$valign='TOP',$class='')
{
/*
Créer une nouvelle ligne
$class : style CSS
*/
	$string ="<tr ";
	if ($class) 
		$string .= "class='".$class."' ";
	$string .= " align = '".$align."' ";
	$string .= " valign = '".$valign."' ";
	$string .= ">";
	return $string;
}
function tr_end()
{
/*
fin de la ligne
*/
	$string = "</tr>";
	return $string;	
	
}
function td_init($val='',$align='',$valign='',$class='')
{
/*
Créer une nouvelle colonne
val : numero de la colonne
class : style CSS
*/
	$string ="<td ";
	if ($class) 	$string .= "class='".$class."' ";
	if ($val=='0') 	$string .= "colspan = '".$this->colonneMaxi."' ";

	$string .= " align = '".$align."' ";
	$string .= " valign = 'TOP' ";
	
	$string .= ">";
	
	return $string;	
}
function td_end()
{
/*
fin de la colonne
*/
	$string = "</td>";
	return $string;	

}


function colonne_new($val)
{
/*
Nouvelle colonne
$val : numero de la colonne (0 ou 1 ou autre chiffre pour les autres colonnes)
*/
	$string="";

	switch ($val)
	{
		case 0:					// colonne sur toute la largeur du tableau
			$string .= $this->tr_init();
			$string .= $this->td_init(0);
		break;
	
		case 1:					// 1ere colonne de la ligne
			$string .= $this->tr_init();
			$string .= $this->td_init();
		break;

		default:				// toutes les colonnes
			$string .= $this->td_end();
			$string .= $this->td_init();
		break;
	} 

	return $string;

}

function colonne_end()				
{
/*
fin de la colonne
*/
	$this->td_end();
	$this->tr_end();
}

function fieldset_new($legend='',$id_fieldset='')
{
/*
Dessine un cadre
$legend : texte sur le cadre
*/
	 $string = "<fieldset id=".$id_fieldset.">\n";
   	if ($legend!="")
		$string .= $this->creer_legend($legend);

	return $string;
}

function fieldset_end()
{
/* 
fin du cadre
*/
	return "</fieldset>\n";
}

function creer_legend($legend)
{            
/*
Ecrit un texte sur le cadre (fonction appelée à partir de "fieldset_new")
$legend : texte à afficher
*/
	$string = "<legend>".$legend."</legend>\n";
	return $string;
	
}
            
function creer_label($int_champ,$nom,$require)
{
/*
Texte avant le formulaire
$int_champ : champ texte formaté
$nom : texte affiché
$require : yes : affiche une étoile pour champ obligatoire / no pour pas d'étoile
*/
	$string = "<label class='formlabel' ";
	$string .= "for='".$int_champ."'>".$nom." ";
	if($require=='yes') 
	{
		$string .= " * ";
	}
	$string .= "</label>\n";

	
	return $string;
}

function creer_captcha($int_champ,$nametext,$require='NO'){
    $string="<div>\n";
    $string .= $this->creer_label($int_champ,$nametext,$require);
    $string .= "<div id='captcha'>\n";
    $string .= "<input id='defaultReal' type='text' name='defaultReal'/>\n";
    $string .= "</div>\n";
    $string .= "</div>\n";

    return $string;

}

function creer_text($int_champ,$nametext,$require='NO',$type='text',$variable='',$class='',$rules='',$minlength='',$maxlength='',$size='',$readOnly=false,$tabindex='',$error_text='')
{
    /* Partie labels
            $int_champ :
            $nametext : texte qui sera affiché
            $require : YES ou NO => champ obligatoire ou pas
       Partie case à saisir
            $type : TEXT,PASSWORD,HIDDEN
            $variable : valeur du champ input
            $rules : type de champs attendu alpha,numeric,floating,alnum,alnumhyph,email,formatdate
            $minlenght : longueur minimum
            $maxlenght : longueur maximum
            $tabindex : numero d'indexation
            $class : style CSS du champ
    */

    $id_int_champ = str_replace("[]", "", $int_champ);
    $string="";
    if ($type !="hidden") $string .= "<p>";
    if ($type !="hidden")
            $string .= $this->creer_label($int_champ,$nametext,$require);

    $string .= "<input alt='$nametext' type='".$type."' ";
    $string .= "name='".$int_champ."' ";
    $string .= "id='".$id_int_champ."' ";
    $string .= "value=\"".$variable."\" ";
    $string .= "require='".$require."' ";
    if ($readOnly == true)	$string .= "readonly ";
    if ($rules != '')	$string .= "rule='".$rules."' ";
    if ($minlength != '')	$string .= "minlength='".$minlength."' ";
    if ($maxlength != '')	$string .= "maxlength='".$maxlength."' ";
    if ($size != '')	$string .= "size='".$size."' ";
    if ($tabindex != '')	$string .= "tabindex='".$tabindex."' ";
    if ($class != '')	$string .= "class='".$class."' ";

    $string .= ">";
    $string .= "</input>";
    if ($type !="hidden") $string .= "<br>\n";

    if($error_text!=''){
        $string .= "<span class='err-msg'><br/>".$error_text."</span>";
    }

    if ($type !="hidden") $string .="</p>";
    return $string;
}

public function creer_date($int_champ,$nametext,$require='NO',$variable,$class='',$tabindex='')
{
/* champ date */
/*
 Partie labels
	$int_champ : 
	$nametext : texte qui sera affiché
	$require : YES ou NO => champ obligatoire ou pas
 Partie case à saisir
	$variable : valeur du champ input
	$tabindex : numero d'indexation 
	$class : style CSS du champ 
*/

$string="";
$string .="<p>";
$string .= $this->creer_label($int_champ,$nametext,$require);
	
$string .= "<input type='text' ";
$string .= "name='".$int_champ."' ";
$string .= "id='".$int_champ."' ";

    if ($variable==""){
        $variable=DATE("Y-m-d");
    }

    if (is_numeric($variable)){
        strftime("%d/%m/%Y",strtotime($variable));
    }
    else{
        $variable=conv_date($variable);
    }

$string .= "value=\"".$variable."\" ";
$string .= "required='".$require."' ";
$string .= "rule='formatdate' ";

$string .= "style='width:100px;'  ";

if ($tabindex != '')	$string .= "tabindex='".$tabindex."' ";
if ($class != '')	$string .= "class='".$class."' ";

$string .= ">";
$string .= "</input>";
$string .= "<br>\n";
$string .="</p>";

return $string;

}


    /*
     * creer_radio($valeur_radio,$ichecked,$name)
     * $valeur_radio = Tableau contenant les différentes valeurs des civilité
     * $ichecked =  définit si nécessaire la valeur sélectionnée
     * $name =  est le nom de la valeur name de l'input
     */

    public function creer_radio($int_champ,$nametext,$require,$valeur_case,$ichecked,$name,$type='radio',$class='',$tabindex='',$readOnly=false){

    	$htcode = "";
        $htcode .= "<p>";

        $htcode .= $this->creer_label($int_champ,$nametext,$require);
        
        if(is_array($valeur_case))
	{
            //count($valeur_civilite);
            $htcode .= "<div id='".$type."_".$name."'";
            if ($tabindex != '')	$htcode .= "tabindex='".$tabindex."' ";
            
            $htcode .=">\n";
            for ($i = 1; $i <= count($valeur_case); $i++) {

	    $valeur_case=array_values($valeur_case);
               $htcode .= "<input ";
		$htcode .= "type='".$type."' ";
		$htcode .= "id='".str_replace('-','',$name).$i."' ";
		$htcode .= "name='".$name."' "; 
		$htcode .= "value='".$i."' ";

	

              if (is_numeric($ichecked) && $ichecked==$i){
                   $htcode .= " checked='checked' ";
               } else {

		if ($valeur_case == $ichecked )
                    $htcode .= " checked='checked' ";
		}
                if ($readOnly == true){
                    $htcode .= "disabled='true' ";
                }

               $htcode .= "/>";
		$htcode .= "<label for='".str_replace('-','',$name).$i."' ";
				if ($class != '')	$htcode .= "class='".$class."' ";
		$htcode .= ">";
               $htcode .= $valeur_case[$i-1];
               $htcode .= "</label>"."\n";

            }
            $htcode .= "</div>\n";

        }
        else
        {
            $htcode .= "valeur_radio doit être un tableau";
        }
        $htcode .= "<br />";
        $htcode .= "</p>";
        return $htcode;
    }


    /*
     * creer_checkbox($valeur_radio,$ichecked,$name)
     * $valeur_radio = Tableau contenant les différentes valeurs des civilité
     * $ichecked =  définit si nécessaire la valeur sélectionnée
     * $name =  est le nom de la valeur name de l'input
     */
    public function creer_checkbox($int_champ,$nametext,$require,$valeur_case,$ichecked,$name='',$type='checkbox',$class='',$tabindex=''){

    	$htcode = "";
        
        $htcode .= "<p>";
        $htcode .= $this->creer_label($int_champ,$nametext,$require);
        
        if(is_array($valeur_case))
	{
           
            //count($valeur_civilite);
            $htcode .= "<div id='".$type."_".$name."'";
            if ($tabindex != '')	$htcode .= "tabindex='".$tabindex."' ";
            
            $htcode .=">\n";

            $i=1;

		foreach ($valeur_case as $key => &$row) {

                    $htcode .= "<input ";
                    $htcode .= "type='".$type."' ";
                    $htcode .= "id='".str_replace('-','',$name).$i."' ";
                    $htcode .= "name='".$name."[]' ";
                    $htcode .= "value='".$key."' ";

                    if (count($ichecked)!=0 && in_array($key,$ichecked))
                    {
                             $htcode .= " checked='checked' ";
                    }
                   $htcode .= "/>";
                    $htcode .= "<label for='".str_replace('-','',$name).$i."' ";
                            if ($class != '')	$htcode .= "class='".$class."' ";
                    $htcode .= ">";
                   $htcode .= $row;
            //	$htcode .=$row[$key];
                    $htcode .= "</label>"."\n";
                    $i++;
                }
            $htcode .= "</div>\n";
        }
        else
        {
            $htcode .= "valeur_radio doit être un tableau";
        }       
        $htcode .= "</p>";
       
        return $htcode;
    }



    /*
     * creer_checkbox($valeur_radio,$ichecked,$name)
     * $value = valeur par defaut du checkbox
     * $ischecked =  définit si la checkbox est coché ou pas
     * $name =  est l'id et le name de la checkbox
     */
    public function creer_une_checkbox($name,$nametext,$alt_nametext,$require,$value='1',$ischecked=false,$class='',$tabindex=''){

    	$htcode = "";
        $htcode .= "<p>";

        $htcode .= $this->creer_label('',$alt_nametext,$require);
        $htcode .= "<input type='checkbox' id ='$name' name='$name' value='$value' ";
        if($ischecked) $htcode .= "checked ";
		if ($class != '')	$htcode .= "class='".$class."' ";
        $htcode .= "/ >";
        $htcode .= "<label for='".str_replace('-','',$name)."' ";
        if ($class != ''){
            $htcode .= "class='".$class."' ";
        }
        $htcode .= ">";
        $htcode .= $nametext;
        if($require=='yes')
	{
		$string .= " * ";
	}
            //	$htcode .=$row[$key];
        $htcode .= "</label>"."\n";
        $htcode .= "<br /> ";
        
        $htcode .= "</p>";
        return $htcode;
    }


public function creer_textarea($int_champ,$nametext,$name,$value_text,$require='FALSE',$rules='',$minlength='',$maxlength='',$cols="",$rows="",$tabindex='',$class='',$readOnly=false)
{
/* creer un champ textarea */
/* Partie labels
	$int_champ :
	$nametext : texte qui sera affiché
	$require : YES ou NO => champ obligatoire ou pas
   Partie case à saisir
	$name : nom du champ
	$value_text : valeur du champ input
	$rules : type de champs attendu alpha,numeric,floating,alnum,alnumhyph,email,formatdate
	$minlenght : longueur minimum
	$maxlenght : longueur maximum
	$tabindex : numero d'indexation
*/

        $id_int_champ = str_replace("[]", "", $name);
        $htcode = "";
        $htcode .= "<p>";
        $htcode .= $this->creer_label($int_champ,$nametext,$require);
        $htcode .= "<textarea alt='$nametext' name='".$name."' id='".$id_int_champ."' ";
					if ($tabindex != '')	$htcode .= "tabindex='".$tabindex."' ";

					$htcode .=" required='".$require."' ";
                    if($rules!='')	$htcode .=" rule='".$rules."' ";
                    if($class!='')	$htcode .=" class='".$class."' ";
                    if($minlength!='')	$htcode .=" minlength='".$minlength."' ";
                    if($maxlength!='')	$htcode .=" maxlength='".$maxlength."' ";
                    if($cols!='')	$htcode .=" cols='".$cols."' ";
                    if($rows!='')	$htcode .=" rows='".$rows."' ";



//                    $htcode .=" tabindex='".$tabindex."' ";
	$htcode .= ">";
	$htcode .= $value_text;
	$htcode .= "</textarea>\n";

        $htcode .= "<br />\n";
        $htcode .= "</p>";
        return $htcode;

}

    /*
     *
     * Fonction qui permet de générer un select avec des optgroup
     * basé sur un fichier xml
     * indiquer uniquement le chemin du fichier xml
     * val_required =  si la liste est obligatoire ou non mettre yes sinon no par défaut
     */

    public function creer_select($int_champ,$nametext,$require,$xmvalue,$isfile=TRUE,$selected='',$size='auto',$defaultValue='',$tabindex='',$force_int_champ=FALSE,$readOnly=false,$class=''){

        $htcode = "";
        $htcode .= "<p>";
        
        $htcode .= $this->creer_label($int_champ,$nametext,$require);

        //si $xmvalue est un chaine de caractère correspondant au chemin d'un fichier xml
	if(is_string($xmvalue))
        {
            if (file_exists($xmvalue) )
            {
                    if ($isfile){
                                    $selectsecteur = simplexml_load_file($xmvalue);
                    }
                    else {
                        $xmvalue=utf8_encode($xmvalue);
                                    $selectsecteur = simplexml_load_string($xmvalue);
                    }
                    $listevaleurs = $selectsecteur->getName();


            if($force_int_champ==FALSE){
                    $htcode .= "<".$listevaleurs." id=".$selectsecteur['id']." name=".$selectsecteur['name']." require=".$require." ";
            }
            else{


                    $htcode .= "<".$listevaleurs." id=".$int_champ." name=".$int_champ." require=".$require." ";
            }
            

            if ($size != '')	$htcode .= "size='".$size."' ";
            if ($tabindex != '')	$htcode .= "tabindex='".$tabindex."' ";
            if($class!='')	$htcode .=" class='".$class."' ";
            if ($readOnly == true){
                    $htcode .= "disabled='true' ";
            }

            $htcode .= ">\n";
            foreach($selectsecteur->children() as $element){
                if ($element->getName()=='optgroup'){
                    $htcode .=  "<".$element->getName()." ".$element[0]->attributes()->getName()."='".$element->attributes()."'>\n";
                    foreach($element->children() as $selement){
                        $htcode .=  "<".$selement->getName()." ".$selement[0]->attributes()->getName()."='".$selement->attributes()."' ";
                        if($selected==$selement->attributes() && $selement[0]->attributes()->getName()=="value"){
                            $htcode .=  "selected";
                        }
                        $htcode .=  ">".$selement."</".$selement->getName().">\n";
                    }
                    $htcode .=  "</".$element->getName().">\n";
                }
                else{
                    $htcode .=  "<".$element->getName()." ".$element[0]->attributes()->getName()."='".$element->attributes()."' ";
                    if(($selected==$element->attributes()) && ($element[0]->attributes()->getName()=='value')){
                        $htcode .=  "selected";
                    }
                    $htcode .=  ">".$element."</".$element->getName().">\n";
                }

            }
            $htcode .=  "</".$listevaleurs.">\n";

           }
        }
	else
	{
            //voir pour modifier id lorsque int_champ contient []

            $id_int_champ = str_replace("[]", "", $int_champ);

		$htcode .= "<select alt='$nametext' ";
		$htcode .= "required='$require' name='$int_champ' id='$id_int_champ' ";
	        if ($size != '')$htcode .= "style='width:".$size."px' ";
	        if ($tabindex != '')$htcode .= "tabindex='".$tabindex."' ";
                if($class!='')	$htcode .=" class='".$class."' ";
                if ($readOnly == true){
                    $htcode .= "disabled='true' ";
                }
		$htcode .= ">";
	
	
		if($defaultValue!='' && is_array($defaultValue) && count($defaultValue)>0)
                    $htcode .= "<option value=\"".$defaultValue[0]."\">".$defaultValue[1]."</option>";
                else
                    $htcode .= "<option value='0'>Faites un choix ...</option>";

		foreach ($xmvalue as $key => $rows)
		{
			$htcode .= "<option value=".$key." ";
			if ($key== $selected) $htcode .= " SELECTED ";
			$htcode .= ">".$rows."</option>";			
		}
		$htcode .= "</select>";

	}
        $htcode .= "<br />\n";
        $htcode .= "</p>";
        return $htcode;
    }

   public function creer_select_heure($int_champ,$nametext,$require,$xmvalue,$selected='',$tabindex=''){

        $htcode = "";
        $htcode .= "<p>";
        
        $htcode .= $this->creer_label($int_champ,$nametext,$require);
        
        $htcode .= "<select alt='$nametext' ";
		$htcode .= "required='$require' name='$int_champ' id='$int_champ' ";
        if ($tabindex != '')$htcode .= "tabindex='".$tabindex."' ";
		$htcode .= ">";

		foreach ($xmvalue as $key => $rows)
		{
			$htcode .= "<option value=".$key." ";
			if ($key== $selected) $htcode .= " SELECTED ";
			$htcode .= ">".$rows."</option>";			
		}
		$htcode .= "</select>";
	
	return $htcode;
        
   }

   public function creer_select_minute($xmvalue,$selected='',$tabindex=''){

   	 $htcode .= "<select alt='$nametext' ";
		$htcode .= "required='$require' name='$int_champ' id='$int_champ' ";
        if ($tabindex != '')$htcode .= "tabindex='".$tabindex."' ";
		$htcode .= ">";


		foreach ($xmvalue as $key => $rows)
		{
			$htcode .= "<option value=".$key." ";
			if ($key== $selected) $htcode .= " SELECTED ";
			$htcode .= ">".$rows."</option>";			
		}
		$htcode .= "</select>";
   	
		
        $htcode .= "<br />\n";
        $htcode .= "</p>";
        return $htcode;

   }
    

    public function creer_photo($int_champ,$nametext,$require,$pathphoto,$updatable='no',$class='photo'){
        $htcode ="";
        $htcode .= "<p>";

        $htcode .= $this->creer_label($int_champ,$nametext,$require);
        $htcode .= "<div id='upload_photo'>\n";
        if($class==''){
            $class='photo';
        }
        $htcode .= "<img class='".$class."' src='".$pathphoto."' />\n";
        if($updatable!='no'){
            $htcode .= "<p>\n";
            $htcode .= "<input type='button' id='upload' class='btn-profil ui-icon ui-icon-plusthick'/>\n";
            $htcode .= "<input type='button' id='remove' value='remove' class='btn-profil ui-icon ui-icon-minusthick'/>\n";
            $htcode .= "</p>\n";
        }
        $htcode .= "</div>\n";

        $htcode .= "<br />\n";
        $htcode .= "</p>";
        
        return $htcode;
    }

	public function creer_bouton($value='insert',$name='bt',$type='submit',$class='')
	{
            /* creer un bouton pour le formulaire */
            /*
                $value : valeur du bouton (insert,delete,update)
                $name : nom du bouton
                $type : button / submit
                $class : Class CSS
            */
        if($name == 'bt' || $name=='')
            {
            $name=$name.time();
            }

        $string = "";

        $string .= "<p>";      

        $string .= "<input ";
        $string .= "type='$type' ";
        $string .= "id='$name' ";
        $string .= "name=\"$name\" ";
        $string .= "value='$value' ";
        
        if(!empty($class)) $string .= "class='".$class."' ";
        
        $string .= "/>";
        $string .= "</p>";

        
        return $string;
     }

    /*
     * creer_adresse($valeur_civilite,$ichecked,$name,$id)
     * $valeur_adresse = Tableau contenant les différentes valeurs des civilité
     * $name =  est le nom de la valeur name de l'input
     * $id = est le nom de la valeur id de l'input
     *
     */

     public function creer_adresse($valeur_adresse,$name,$id){

         /*
          * Point de remise
          * N° APPARTEMENT - BOITE A LETTRE - ETAGE - COULOIR - ESCALIER 
          * 
          */
         echo '<div class="ligne_box">'."\n";
         echo '<label class="form_label lbllong" for='.$name.'_point_remise>Point de remise :</label>'."\n";
         echo '<input class="form_input long" id="'.$id.'_point_remise" name="'.$name.'_point_remise" rule="alnum" maxlength="38"></input>'."\n";
         echo '</div>'."\n";
         echo '<br />'."\n";
         /*
          * Complement point de remise
          * ENTREE - TOUR - IMMEUBLE - BATIMENT - RESIDENCE - ZI
          * 
          */
         echo '<div class="ligne_box"\n>'."\n";
         echo '<label class="form_label lbllong" for='.$name.'_complement_remise>Complément PR :</label>'."\n";
         echo '<input class="form_input long" id="'.$id.'_complement_remise" name="'.$name.'_complement_remise" rule="alnum" maxlength="38"></input>'."\n";
         echo '</div>'."\n";
         echo '<br />'."\n";

         /*
          * Numéro de voie
          * Numéro - et complément bis ter ...
          * 
          */
          echo '<div class="ligne_box">'."\n";
          echo '<label class="form_label lblcourt" for='.$name.'_num_voie>Numéro :</label>'."\n";
          echo '<input class="form_input court" id="'.$id.'_num_voie" name="'.$name.'_num_voie" required="yes" rule="alnum" maxlength="5"></input>'."\n";
          //echo '</p\n>'."\n";
          
          /*
           * Complément voie
           * BP - TSA - POSTE RESTANTE - LIEU DIT
           * 
           */
          //echo '<p\n>'."\n";
          echo '<label class="form_label lblcourt" for='.$name.'_complement_voie> Voie :</label>'."\n";
          echo '<input class="form_input moyen" id="'.$id.'_complement_voie" name="'.$name.'_complement_voie" rule="alnum" maxlength="32"></input>'."\n";
          echo '</div>'."\n";
          echo '<br />'."\n";
           /*
           * Code postal
           * Code postal
           * 
           */
          echo '<div class="ligne_box">'."\n";
          echo '<label class="form_label lblcourt" for='.$name.'_cp>CP :</label>'."\n";
          echo '<input class="form_input court" id="'.$id.'_cp" name="'.$name.'_cp" required="yes" rule="numeric" maxlength="5"></input>'."\n";
          //echo '</tr\n>'."\n";

           /*
           * Ville
           * Localite - CEDEX
           *
           */
          //echo '<p\n>'."\n";
          echo '<label class="form_label lblcourt" for='.$name.'_ville> Localité :</label>'."\n";
          echo '<input class="form_input moyen" id="'.$id.'_cp" name="'.$name.'_ville" required="yes" rule="alpha" maxlength="32"></input>'."\n";
          echo '</div>'."\n";

                     /*
           * Pays
           * Pays
           *
           */
          echo '<div class="ligne_box">'."\n";
          echo '<label class="form_label lblcourt" for='.$name.'_pays> Pays :</label>'."\n";
          echo '<input class="form_input moyen" id="'.$id.'_pays" name="'.$name.'_pays" rule="alpha" maxlength="32"></input>'."\n";
          echo '</div>'."\n";
     }
     
     
function affiche_label($int_champ,$nom,$require,$class="formlabel")
{
/*
Texte avant le formulaire
$int_champ : champ texte formaté
$nom : texte affiché
$require : yes : affiche une étoile pour champ obligatoire / no pour pas d'étoile
*/
        $string = "";
	$string .= "<label class='".$class."' ";
	$string .= "for='".$int_champ."'>".$nom." ";
	if($require=='yes') 
	{
		$string .= " * ";
	}
	$string .= "</label>";

	
	return $string;
}

function affiche_text($nametext,$value,$class='',$style='',$id='',$force=false,$classlabel="formlabel")
{


//echo "!!!!".$value."<br/>";
/*
fonction à utiliser pour 1 champ : Text, textarea, date, hidden

$nametext : titre de la ligne
$value : contenu de la ligne (stockée en BDD)
$class : style CSS
$style : date => convertir le mois en clair
		 mot => 1ere Lettre en majuscule et le reste en minuscule
		 ville => 1ere Lettre en majuscule et le reste en minuscule à tous les mots
		 maj => tout en majuscule
		 min => tout en minuscule
*/	

	if ($value=="" && $force==false) return;

	$string ="<div class='divblock'>";
	$string .= "<p>";
	$string .= $this->affiche_label('',$nametext,'',$classlabel);
	$string .= "<div class='$class'";
        if($id!='') $string .= "id='$id'";
        $string .=">";
	$string .= add_cr($this->MiseEnForme($style,$value));
	$string .= "</div></p></div>";
	//$string .= "<br>";
	$string .= "";
	return $string;

}

function affiche_radio($nametext,$value,$selected,$class='',$classlabel="formlabel")
{
/*
fonction pour les radio boutons

$nametext : titre de la ligne
$value : liste des valeurs
$selected : ligne selectionné et provenance de la BDD
$class : style CSS
*/	

	if ($selected=="") return;

	$string ="<div class='divblock'>";
	$string .= "<p>";
	$string .= $this->affiche_label('',$nametext,'',$classlabel);
	$string .= "<div class='$class'>";
	for ($i = 1; $i <= count($value); $i++) 
	{
		if ($i == $selected ) $string .= $value[$i];
	}
	
	$string .= "</div>\n";

	$string .="</p></div>";
 
	return $string;

}


function affiche_checkbox($nametext,$value,$selected,$class='',$classlabel="formlabel")
{
/*
fonction pour les checkbox

$nametext : titre de la ligne
$value : liste des valeurs
$selected : ligne selectionné et provenance de la BDD
$class : style CSS
*/	

	if ($selected=="") return;

	$string ="<div class='divblock'>";
	$string .= "<p>";
	$string .= $this->affiche_label('',$nametext,'',$classlabel);
	$string .= "<div class='$class'>";
	foreach ($value as $key => &$row) 
	{
	
			if (count($selected)!=0 && in_array($key,$selected))
			{
				$string .= $row." ";
			}
	
	}
	$string .= "</div>\n";
	$string .="</p></div>";

	return $string;
}


function affiche_select($nametext,$value='',$fichier='',$selected='',$class='',$classlabel="formlabel")
{
/*
fonction pour les listes deroulantes

$nametext : titre de la ligne
$value : liste des valeurs
$selected : ligne selectionné et provenance de la BDD
$class : style CSS
*/	

	if ($selected=="") return;

	$string ="<div class='divblock'>";
	$string .= "<p>";	
	$string .= $this->affiche_label('',$nametext,'',$classlabel);
        $string .= "<div class='$class'>";

            if ($fichier )
            {
            	
                   if ($fichier){
                                    $selectsecteur = simplexml_load_file($fichier);

                    }
                    else {
                        $fichier=utf8_encode($fichier);

                                    $selectsecteur = simplexml_load_string($fichier);
                    }

                    foreach($selectsecteur->children() as $element)
                    {
		                if ($element->getName()=='optgroup')
		                {
		             //       $htcode .=  "<".$element->getName()." ".$element[0]->attributes()->getName()."='".$element->attributes()."'>\n";
		                    foreach($element->children() as $selement)
		                    {
		                        //if($selected==$selement->attributes() && $selement[0]->attributes()->getName()=="value")
		                        if($selected==$selement->attributes() )
		                        {
				                      $string .= $element->getName();
		                        }
		                    }
		                }
		                else{
		                   $string .= $element->getName();
		                }

            		}
           }

	
     if ($value)
     {   
		$i=0;
		foreach ($value as $key => &$row)
	 	{
			if ($key == $selected)
			{
				$string .= $row;
			}
		}

	}
	
	
	$string .= "</div>\n";
	$string .="</p></div>";

	return $string;

}
     
     
     
//function qui génère un tableau a partie d'un tableau associatif
//function affiche_tableau($tab,$tabKey = array())
function affiche_tableau($sql,$titreColonne = array())
{
if (!$sql) return "Aucune informations";

  $str = "<table  class='tab_info'>\n\t";
  //entete du tableau
  $str.="<tr class='entete_tab_info'>\n\t";
//  foreach($titreColonne as $Key)
//  {
 //    $str.= "<th>".$Key."</th>";
$str.= "<th>Matières</th>";
$str.= "<th>Année</th>";
$str.= "<th>Semestre</th>";
$str.= "<th>Nbre heure</th>";
// }
  $str.="\n</tr>\n";

//  if(is_array($tab) && count($tab)>0)
//  {
	require_once ("../includes/class/requetes.class.php");
  $cnx=new actionsdata();	
  $cnx->connect();	
  $qid=$cnx->requeteSelect ($sql,'assoc');	

// print_r($qid);
  foreach ($qid as $row)
  {


$str.="<tr class='ligne_tab_info$i'>\n\t";
$str.= "<td>".$row['libelle']."</td>";
$str.= "<td>".$row['libelle_annee']."</td>";
$str.= "<td>".$row['libelle_semestre']."</td>";
$str.= "<td>".$row['heure']."</td>";
$str.="\n</tr>\n";


  /*   $str.="<tr class='ligne_tab_info$i'>\n\t";
          foreach($titreColonne as $Key)
          {
              $str.= "<td>".$val[$Key]."</td>";
          }
          $str.="\n</tr>\n";

          //pour le switch de class          
          if($i==0): $i=1; else: $i=0; endif;
  	*/
  }
  /*      $tabKey = array_keys($tab[0]);
      
        $i=0;
        foreach($qid as $val)
        {
          $str.="<tr class='ligne_tab_info$i'>\n\t";
          foreach($tabKey as $Key)
          {
              $str.= "<td>".$val[$Key]."</td>";
          }
          $str.="\n</tr>\n";

          //pour le switch de class          
          if($i==0): $i=1; else: $i=0; endif;
        }*/
//  }  
  $str .= "\n</table>"; 
  return $str;
}

function MiseEnForme($style,$value)
{
	$string="";
	switch ($style)
	{
		case 'maj':
			$string .= $this->MiseEnForme_majuscule($value);
		break;
		case 'min':
			$string .= $this->MiseEnForme_minuscule($value);
		break;
		case 'mot':
			$string .= $this->MiseEnForme_un_mot($value);
		break;
		case 'plusmots':
			$string .= $this->MiseEnForme_mots($value);
		break;
		case 'date':
			$string .= $this->MiseEnForme_date($value);
		break;
		case 'html':
			$string .= $this->MiseEnForme_html($value);
		break;
		default:
			$string .= $value;
		break;
	}
	return $string;
}	
// Mise en forme de plusieurs mots
// 1ere lettre en majuscule et le reste en minuscule
function MiseEnForme_mots($string)
{
	
	if (strpos($string, "-")==true)
		$tmp=explode ("-",$string);
	elseif (strpos($string, " ")==true)
		$tmp=explode (" ",$string);
	elseif (strpos($string, "/")==true)
		$tmp=explode ("/",$string);

	if (!$tmp) return $string;

	$result="";
	foreach ($tmp as $row)
	{
		 $result .= ucfirst(strtolower($row));
		 $result .= " ";
	}

	return $result;
}
// 1ere lettre en majuscule et le reste en minuscule
function MiseEnForme_html($string)
{
	return nl2br($string);
}
// 1ere lettre en majuscule et le reste en minuscule
function MiseEnForme_majuscule($string)
{
	return strtoupper($string);
}

// 1ere lettre en majuscule et le reste en minuscule
function MiseEnForme_minuscule($string)
{
	return strtolower($string);
}


// 1ere lettre en majuscule et le reste en minuscule
function MiseEnForme_un_mot($string)
{
	return ucfirst(strtolower($string));
}

// Mise en forme de la date 
// le mois en clair
function MiseEnForme_date($string)
{

	$tmp=explode ("-",$string);
	
	$new=$tmp[0]." ".conv_mois($tmp[1])." ".$tmp[2];
	
	return $new;
}
    //put your code here
}
?>