<?php
//require_once("../includes/class/_inc_function.php");

class listes
{

        function liste_civilite()
        {
        //    return "1:Mademoiselle;2:Madame;3:Monsieur:4:Madame et Monsieur";

                /*$tab=array(
                                        "1" => "Mademoiselle",
                                        "2" => "Madame",
                                        "3" => "Monsieur",
                                        "4" => "Madame et Monsieur"
                                );
                        */
                $tab="1:Mademoiselle;2:Madame;3:Monsieur:4:Madame et Monsieur";

                        return $tab;
        }

        // select provisoire pour les civilites
        function select_civilite($name,$num=1,$xml=false)
        {
            $str = ($xml) ? "<?xml version=\"1.0\"?>" : "";

            $liste = array("Mademoiselle","Madame","Monsieur","Madame et Monsieur");
            $str.="<select name='$name' id='$name'>";
            foreach($liste as $cle => $valeur)
            {
              $str .= "<option value='".($cle+1)."'";
              if(($cle+1) == $num) $str.= " selected ";
              $str .= ">$valeur</option>";
            }
            $str.="</select>";

            return $str;
        }


        //function qui génère un tableau a partie d'un tableau associatif
        function listeElementTr($tab,$tabKey = array())
        {
          $str = "<table class='tab_info'>\n\t";
          //entete du tableau
          $str.="<tr class='entete_tab_info'>\n\t";
          foreach($tabKey as $Key)
          {
             $str.= "<th>".$Key."</th>";
          }
          $str.="\n</tr>\n";

          if(is_array($tab) && count($tab)>0)
          {

                $tabKey = array_keys($tab[0]);

                $i=0;
                foreach($tab as $val)
                {
                  $str.="<tr class='ligne_tab_info$i'>\n\t";
                  foreach($tabKey as $Key)
                  {
                      $str.= "<td>".$val[$Key]."</td>";
                  }
                  $str.="\n</tr>\n";
                }
          }
          $str .= "\n</table>";
          return $str;
    }

    function select_liste($table,$id,$text,$where="",$orderby='true')
{
/*
  $table = nom de la table
  $id : champ ID de la table
  $text : le champ texte qui sera affiché
*/

       $liste= new actionsdata();
       $liste->connect();


        $sql = "SELECT $id,$text FROM ".$table." ";
        if($where != "")  {
            $sql .= "WHERE $where ";
        }
        if($orderby == 'true') {
            $sql .= "ORDER BY $text";
        }

        $data = $liste->requeteSelect($sql);
        //$rows[0]="--- Choisir ---";
        foreach($data as $row)
        {
                $rows[$row[$id]] = $row[$text];
        }

       return $rows;
}

}


class listes_cvtheque
{
	/*
 * allimente une liste déroulante groupe
 *
 */

    function liste_niveau_langue()
    {
    //global $serveur,$user,$password,$bdd;

       //$liste= new actionsdata($serveur,$user,$password,$bdd);
       $liste= new actionsdata();
       $liste->connect();

            $sql="SELECT * FROM cv_niveau_langue ";
            $row= $liste->requeteSelect($sql);

            $rows="";
            foreach($row as $data)
            {
                    $rows .= $data['ID_NIVEAU_LANGUE']."=>".$data['NOM_NIVEAU'].";";
            }
            return substr ($rows,0,strlen($rows)-1);
    }


    function liste_effectif()
    {
    //global $serveur,$user,$password,$bdd;

       //$liste= new actionsdata($serveur,$user,$password,$bdd);
       $liste= new actionsdata();
       $liste->connect();

            $sql="SELECT * FROM cfg_effectif ";
            $row= $liste->requeteSelect($sql);

            $rows="";
            foreach($row as $data)
            {
                    $rows .= $data['id']."=>".$data['nom'].";";
            }
            return substr ($rows,0,strlen($rows)-1);

    }

    function liste_langues($table)
    {
    //global $serveur,$user,$password,$bdd;

       //$liste= new actionsdata($serveur,$user,$password,$bdd);
       $liste= new actionsdata();
       $liste->connect();

            $sql="SELECT id_langue,nom_langue FROM ".$table;
            $row = $liste->requeteSelect($sql);
            
            return $row;

    }


}

?>