<?php
/*
$nom_expediteur = "Christophe V";
$expediteur = "christophe.villeneuve@alterway.fr";
$destinataire= "hellosct1@gmail.com";
$destinataire_copy = "hello@hello-design.fr";
$destinataire_cache = "christophe.villeneuve@alterway.fr";
$fichier ="example.zip";
$sujet="Rmail example 3 - Test email";
$html  ="<b>Sample HTML</b><br />";
$html .=" <img src='logo_edc_2.jpg'><br />";
$html .="<i>oki</i>";
$priority="high";
*/


require_once ABSPATH.'/includes/class/requetes.class.php';
require_once ABSPATH.'/includes/Rmail/Rmail.php';
//require_once('../conf/_connect.php');

class mail
{

function offre_envoie_mail($type_email,$numero_offre,$expediteur,$destinataire,$destinataire_copy='',$destinataire_cache='')
{
/*
  $type_email : id de la table cfg_email (choisir le type d'email)
  $expediteur : coordonnée de la personne qui envoie (récupération venant de l'identification)
  $destinataire : personne qui reçoit l'email
  $destinataire_copy : destinataire en copie ci necessaire
  $destinataire_cache : destinataire en copie caché ci necessaire
*/
global $periode;

try
{

//   require_once('Rmail.php');
  $cnx= new actionsdata();
$cnx->connect();

$coordonnees_expediteur = $this->coord_exp($cnx,$expediteur);
$offre = $this->offre($cnx,$numero_offre);
$contenu_email = $this->contenu_email($cnx,$type_email);

$html=$contenu_email[0]['body'];
$html=str_replace ('[reference_offre]',$offre[0]['reference_offre'],$html);
$html=str_replace ('[duree]',$offre[0][duree],$html);
$html=str_replace ('[prenom_contact]',$coordonnees_expediteur[0][prenom_contact],$html);
$html=str_replace ('[nom_contact]',$coordonnees_expediteur[0][nom_contact],$html);
$html=str_replace ('[signature]',$coordonnees_expediteur[0][signature],$html);
$html=str_replace ('[periode_scolaire]',$periode,$html);

    /**
    * Now create the email it will be attached to
    */
    $mail = new Rmail();
//    $mail->addAttachment(new StringAttachment($body, 'Attached message', 'message/rfc822', new SevenBitEncoding()));
//    $mail->addAttachment(new FileAttachment('example.zip'));
//   $mail->addEmbeddedImage(new fileEmbeddedImage('background.gif'));

    /**
    * Add an attachment to the email.
    */
    /*
    $nom_expediteur=$coordonnees_expediteur[0]['prenom_contact']." ".$coordonnees_expediteur[0]['nom_contact'];
$email_expediteur=$coordonnees_expediteur[0]['email_contact'];

    $mail->setFrom("".$nom_expediteur." <".$email_expediteur.">");
    $mail->setSubject($contenu_email[0]['sujet']);

    $mail->setPriority($contenu_email[0]['priority']);

    $mail->setHTML($html);
    //    $mail->setText('Sample text');
    $fichier= "../documents/".$contenu_email[0]['piece_jointe'];

    $mail->addAttachment(new fileAttachment($fichier));

    if ($contenu_email[0]['receipt']==1) $mail->setReceipt($expediteur);
    if ($destinataire_copy) $mail->setCc($destinataire_copy);
    if ($destinataire_cache) $mail->setBcc($destinataire_cache);

    $result  = $mail->send(array($destinataire));
  */


$nom_expediteur = "Christophe V";
$expediteur = "christophe.villeneuve@alterway.fr";
$destinataire= "hellosct1@gmail.com";
$destinataire_copy = "hello@hello-design.fr";
$destinataire_cache = "christophe.villeneuve@alterway.fr";
$fichier ="../documents/Calendrier des stages 2010-2011.doc";
$sujet="Rmail example 3 - Test email";
$html  ="<b>Sample HTML</b><br />";
//$html .=" <img src='logo_edc_2.jpg'><br />";
$html .="<i>oki</i>";
$priority="high";

    $mail->setFrom("".$nom_expediteur." <".$expediteur.">");
    $mail->setSubject($sujet);
    /**
    * Set high priority for the email. This can also be:
    * high/normal/low/1/3/5
    */
    $mail->setPriority($priority);

    $mail->setHTML($html);
    //    $mail->setText('Sample text');

    $mail->addAttachment(new fileAttachment($fichier));

    $mail->setReceipt($expediteur);

    $mail->setCc($destinataire_copy);
    $mail->setBcc($destinataire_cache);

    $result= $mail->send(array($destinataire));
               exit;
return $result;
}
 catch (Exception $e) {
    echo 'Erreur : ',  $e->getMessage(), "\n";
}


}


function coord_exp($cnx,$expediteur)
{
// coordonnées expediteur

try
{

$sql="SELECT prenom_contact,nom_contact,email_contact,signature ";
$sql .="FROM cv_contact ";
$sql .="WHERE login_contact = '".$expediteur."' ";

$result=$cnx->requeteSelect($sql);

//$cnx->deconnect();

return $result;

}
catch (Exception $e) {
    echo 'Erreur : ',  $e->getMessage(), "\n";
}

}


function contenu_email ($cnx,$type_email)
{
//$cnx= new actionsdata();
//$cnx->connect();

try
{
$sql="SELECT sujet,body,piece_jointe,priority,receipt ";
$sql .="FROM cfg_email ";
$sql .="WHERE id = '".$type_email."' ";

$result=$cnx->requeteSelect($sql);

//$cnx->deconnect();

return $result;

}
catch (Exception $e) {
    echo 'Erreur : ',  $e->getMessage(), "\n";
}


}

function offre($cnx,$numero_offre)
{
// coordonnées expediteur

try
{

$sql="SELECT  reference_offre, nom as duree ";
$sql .="FROM cv_offre, cfg_duree ";
$sql .="WHERE id_offre = '".$numero_offre."' ";
$sql .="AND cv_offre.idduree=cfg_duree.id ";

$result=$cnx->requeteSelect($sql);

//$cnx->deconnect();

return $result;

}
catch (Exception $e) {
    echo 'Erreur : ',  $e->getMessage(), "\n";
}

}


}
?>