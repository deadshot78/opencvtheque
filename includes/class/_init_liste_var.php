<?php
/*
* _init_liste_var.php Created 8 févr. 2011 at 13:39:43 by flenoble under Ocv-NG
* $Id$
*/
    $searchfield = "";
    $searchstring = "";
    $searchoper = "";
    $limit = 0;
    $page = 1;
    $sidx = "";
    $sord = "";

    $securite = new securite();


    if(isset($_GET['sidx'])) $sidx = $securite->verif_GetPost ($_GET['sidx']); // get index row - i.e. user click to sort
    if(isset($_GET['sord'])) $sord = $securite->verif_GetPost ($_GET['sord']); // get the direction

    if(isset($_GET['page'])) $page = $securite->verif_GetPost ($_GET['page']); // numero de la page
    if(isset($_GET['rows'])) $limit = $securite->verif_GetPost ($_GET['rows']); // nombre de lignes à afficher
    if(isset($_GET['searchField'])) $searchfield = $securite->verif_GetPost ($_GET['searchField']); // get the field
    if(isset($_GET['term'])) $searchstring = $securite->verif_GetPost ($_GET['term']); // get the string
    if(isset($_GET['searchString'])) $searchstring = $securite->verif_GetPost ($_GET['searchString']); // get the string
    if(isset($_GET['searchOper'])) $searchoper = $securite->verif_GetPost ($_GET['searchOper']); // get the operator

    $responce = new stdClass();

    $cnx= new actionsdata();
    $cnx->connect();

?>
