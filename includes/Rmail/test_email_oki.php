<?php
    /**
    * o------------------------------------------------------------------------------o
    * | This package is licensed under the Phpguru license. A quick summary is       |
    * | that for commercial use, there is a small one-time licensing fee to pay. For |
    * | registered charities and educational institutes there is a reduced license   |
    * | fee available. You can read more  at:                                        |
    * |                                                                              |
    * |                  http://www.phpguru.org/static/license.html                  |
    * o------------------------------------------------------------------------------o
    *
    * � Copyright 2008,2009 Richard Heyes
    */

    /**
    * This example includes setting a Cc: and Bcc: address
    */

    require_once('Rmail.php');

    /**
    * Now create the email it will be attached to
    */
    $mail = new Rmail();
//    $mail->addAttachment(new StringAttachment($body, 'Attached message', 'message/rfc822', new SevenBitEncoding()));
//    $mail->addAttachment(new FileAttachment('example.zip'));
//   $mail->addEmbeddedImage(new fileEmbeddedImage('background.gif'));

    /**
    * Add an attachment to the email.
    */
$nom_expediteur = "Christophe V";
$expediteur = "christophe.villeneuve@alterway.fr";
$destinataire= "hellosct1@gmail.com";
$destinataire_copy = "hello@hello-design.fr";
$destinataire_cache = "christophe.villeneuve@alterway.fr";
$fichier ="example.zip";
$sujet="Rmail example 3 - Test email";
$html  ="<b>Sample HTML</b><br />";
$html .=" <img src='logo_edc_2.jpg'><br />";
$html .="<i>oki</i>";
$priority="high";

    $mail->setFrom("".$nom_expediteur." <".$expediteur.">");
    $mail->setSubject($sujet);
    /**
    * Set high priority for the email. This can also be:
    * high/normal/low/1/3/5
    */
    $mail->setPriority($priority);

    $mail->setHTML($html);
    //    $mail->setText('Sample text');

    $mail->addAttachment(new fileAttachment($fichier));

    $mail->setReceipt($expediteur);

    $mail->setCc($destinataire_copy);
    $mail->setBcc($destinataire_cache);

    $result  = $mail->send(array($destinataire));

?>

Message has been sent <?php print_r($result); ?>
