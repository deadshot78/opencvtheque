/* Debut Fichier fonctionjs.js */
var messobligatoire = "Les champs précédés par (*) sont obligatoires";

if(typeof show_subgrid == 'undefined'){
   var show_subgrid=false;
}
if(typeof sort_subgrid == 'undefined'){
   var sort_subgrid='id';
}
if(typeof sort_order == 'undefined'){
   var sort_order='desc';
}
if(typeof sub_navgrid_options == 'undefined'){
   sub_navgrid_options = {edit:true,add:false,del:false};
}

(function($){
    $.widget("ui.combobox",{
        options: {
            value : "testvalue",
            search : "testsearch",
            date_ex : "testdate"
        },

        _create: function(){
            var self = this;
            var select = this.element.hide();
            var old_value = '';
            if($('#ndb-'+select.attr("id")+'_val').val()){
                old_value = $('#ndb-'+select.attr("id")+'_val').val();
                $('#ndb-'+select.attr("id")+'_val').val('');
            }
            var input = $("<input>")


            .insertAfter(select)
            .autocomplete({
                delay: 0,
                minLength: 0,
                source: function( request, response ) {
                    $.ajax({
                        url: self.options.value,
                        dataType: "json",
                        data: {
                                term: self.options.search,
                                date_ex: self.options.date_ex
                        },
                        success: function( data ) {
                            response( data, function() {
                                return {
                                    label: data.label,
                                    value: data.id
                                }
                            });
                        }
                    });
                },
                select: function( event, ui ) {
                                $(select).val(ui.item.id )
                }

            })
            .attr("id","cbx"+select.attr("id"))
            .addClass("ui-widget ui-widget-content")
            .val(old_value);
                input.click(function() {
                if (input.autocomplete("widget").is(":visible")){
                    input.autocomplete("close");
                    return;
                }
                input.autocomplete("search","");
                input.focus();
            });
        }
    });
})(jQuery);


$(function(){

   var lien = $('#pagelink').val();
   var lastsel = '';
//         Crée un tableau afin d'extraire la destinaton de la page demandée
   var destination = lien.split(' ');
   var rep_page = destination[0].toLowerCase();
   var element_page = destination[1].toLowerCase();
   var element_part = destination[2].toLowerCase();
   var element_bouton = destination[3].toLowerCase();
   $("#list").jqGrid({
       //redirecteur.php?dest=commun_testsess&page='+element_page
        url:'redirecteur.php?dest='+rep_page+'_liste-'+element_page,
        datatype: 'json',
        mtype: 'GET',
        loadtext: 'Chargement...',
        colNames: noms_colonnes,
        colModel: modele_colonnes,
        //autowidth: 'true',
        width: largeurgrid,
        height: 'auto',
        pager: '#pager',
        rowList:[10,20,30],
        rowNum: 10,
        sortname: element_part,
        sortorder: sort_order,
        viewrecords: true,

        ///// SUBGRID /////
        subGrid: show_subgrid,
        subGridRowExpanded: function(subgrid_id, row_id) {
        // we pass two parameters
        // subgrid_id is a id of the div tag created whitin a table data
        // the id of this elemenet is a combination of the "sg_" + id of the row
        // the row_id is the id of the row
        // If we wan to pass additinal parameters to the url we can use
        // a method getRowData(row_id) - which returns associative array in type name-value
        // here we can easy construct the flowing
            var subgrid_table_id, pager_id;
            subgrid_table_id = subgrid_id+"_t";
            pager_id = "p_"+subgrid_table_id;
            $("#"+subgrid_id).html("<table id='"+subgrid_table_id+"' class='scroll'></table><div id='"+pager_id+"' class='scroll'></div>");
            $("#"+subgrid_table_id).jqGrid({
                url:'redirecteur.php?dest='+rep_page+'_subgrid-'+element_page+'&q=2&id='+row_id,
                datatype: "json",
                colNames: subgrid_noms_colonnes,
                colModel: subgrid_modele_colonnes,
                rowNum: 10,
                pager: pager_id,
                sortname: sort_subgrid,
                sortorder: "asc",
                width: largeurgrid-50,
                height: 'auto',
                editurl: 'redirecteur.php?dest='+rep_page+'_maj-subgrid-'+element_page,

                ondblClickRow: function(id){
                if(id && id!==lastsel){
                    jQuery("#"+subgrid_table_id).jqGrid('restoreRow',lastsel);
                    jQuery("#"+subgrid_table_id).jqGrid('editRow',id,true);
                    lastsel=id;}
                }

            });
            $("#"+subgrid_table_id).jqGrid('navGrid',"#"+pager_id,sub_navgrid_options);
        },

        ///// SUBGRID /////

        /*cellEdit: true,
        cellurl: './'+rep_page+'/maj_'+element_page+'.php', */

        //var editligne pour savoir si on peut editer ou non en doublecliquant sur la ligne
        ondblClickRow: function(id){
            if(id && id!==lastsel){
                jQuery('#list').jqGrid('restoreRow',lastsel);
                jQuery('#list').jqGrid('editRow',id,true);
                lastsel=id;}
            },

        gridComplete:function(){

            //affiche de boutons options
            $("td").children('.applications').each(function () {
            //alert($(this).html());
            var documentclass=$(this).html().substring($(this).html().lastIndexOf("/")+1);
            $(this).addClass(documentclass);
            $('tr.jqgrow').height(34);
            $(this).html($(this).html().replace("application/"+documentclass, ""));
            $(this).html($(this).html().replace("image/"+documentclass, ""));
            $(this).attr({'title': data_show});
            //jQuery('#list').jqGrid('editRow',id,true);
            });

            $(".jqgrow").each(function(){
                var style = $(this).children('td:hidden').text();
                if(style=='good' || style=='bad'){
                    $(this).children('td:last-child').addClass(style);
                }
            });
        },

        editurl: 'redirecteur.php?dest='+rep_page+'_maj-'+element_page,
        caption: $('#titre_page').text()
    });

    $("#list").jqGrid(
        'navGrid',
        '#pager',
        navgrid_options,
        // options //
        {width:800,
            viewPagerButtons:false,
            reloadAfterSubmit:true,
            jqModal:false,
            closeOnEscape:true,
            bottominfo:messobligatoire,
            closeAfterEdit:true
        });// edit options


    $("#list").jqGrid('filterToolbar',{});

/* file upload */

    if(element_bouton=='cv'){

            $("#list").navButtonAdd('#pager',{
                    caption:"",
                    title:add_title,
                    buttonicon:"ui-icon-plus",
                    id:"upload",
                    position:"first"
            });

            var button = $('#upload'), interval;

            new AjaxUpload(button,{
            action: 'redirecteur.php?dest='+action_upload,
            name: 'userfile',
            autoSubmit: true,

            onSubmit : function(file, ext){

                var tabdata = jQuery("#list").jqGrid('getDataIDs');
                if (tabdata.length>=10){
                    // extension is not allowed
                    alert(alert_nbl);
                    // cancel upload
                    this.disable();
                    return false;
                }

                if (! (ext && /^(pdf|owf|doc|jpg|png|gif|bmp)$/i.test(ext))){
                        // extension is not allowed
                        alert(alert_ext);
                        // cancel upload
                        return false;
                }
                this.disable();

            },
            onComplete: function(file, response){
                //button.text('Upload');
                //window.clearInterval(interval);
                // enable upload button
                //alert (response);
                this.enable();
                var heure=new Date();

                jQuery("#list").trigger("reloadGrid");
                jQuery(".photo, .logo").attr({
                    'src':'redirecteur.php?dest='+imgsrc+'&alea='+heure.getTime()
                });
            }
        });
    }//---- fin upload
});
        
function creer_bouton_navgrid(navgrid,pager,text_caption,text_title,icon,bouton_id,action,popin_fichier,popin_largeur,popin_hauteur){

//popinfonction pourra être viré quand tous les enregistrements via js seront supprimés pour les appels au popin

                $(navgrid).navButtonAdd(pager,{
                	   caption:text_caption,
                	   title: text_title,
                	   buttonicon:icon,
                	   onClickButton: function(){
                               
                                        //alert("<?php echo __DIR__; ?>");
                                       var parametre = '';
                                       $.get('redirecteur.php?dest=commun_testsess',function(data){
                                            if(data=='0'){
                                                alert('Votre session est expirée, merci de vous reconnecter !');
                                                $(location).attr('href',".");
                                                return false;
                                            }
                                            else{
                                                //recupere l'id de l'enregistrement précisé lor de l'envoie du JSON

                                                   var id = $(navgrid).jqGrid('getGridParam','selrow');
                                                   if(id != null && (action == 'update' || action == 'duplicate'))
                                                   {
                                                       parametre = "id="+id ;
                                                       if(typeof parametre_supp != 'undefined') {
                                                            parametre = parametre+'&'+parametre_supp;
                                                       }
                                                       afficheFormulairePopin('#bodycontent',text_title,action,parametre,popin_fichier,popin_largeur,popin_hauteur);
                                                   }
                                                   else
                                                   {
                                                       if(action != 'add' && action != 'export' ){
                                                            alert ("Vous devez sélectionner une ligne");
                                                       }
                                                       else{

                                                           if(action == 'add'){

                                                               if(typeof parametre_supp != 'undefined') {
                                                                    parametre = parametre_supp;
                                                               }
                                                               afficheFormulairePopin('#bodycontent',text_title,action,parametre,popin_fichier,popin_largeur,popin_hauteur);
                                                           } else {
                                                                if($('#secretIFrame').length == 0){
                                                                    $('<iframe id="secretIFrame" src="" style="display:none; visibility:hidden;"></iframe>').appendTo($('#formulairePerso'));
                                                                }
                                                                $("#secretIFrame").attr("src",popin_fichier);
                                                           }
                                                           
                                                       }
                                                   }
                                            }
                                        });

                	   },
                	   id:bouton_id,
                	   position:"first"
                	});

}

function alphacheck(value, colname) {

    var objRegExp  =  /^[A-Z-\s-\']*$/;

    //check for numeric characters
    if (objRegExp.test(value) == false){
            return [false,"Merci de renseigner une chaine de caractères valide"];
        }
        else{
            return [true,""];
        }
}

  //function qui rapatrier les données et affiche le formulaire
  function afficheFormulairePopin (nomdiv,titre,action,parametre,fichier,largeur,hauteur,option)
  {

     var divform="<div id='formulairePerso'></div>";
     var param = "action="+action;

     $('#formulairePerso').remove();
     $(divform).appendTo(nomdiv);

     if(parametre!="")
     {
         param = param+"&"+parametre;
     }

     if(action=="update"){       
        var tab_parametre=parametre.split('=');
     }
     else if(action=="add"){
        var tab_parametre= new Array();
     }

     $.ajax({
     type: "POST",
     url: 'redirecteur.php?dest='+fichier,
     data: param,
     success: function(msg){
        $('#formulairePerso').html(msg);
        $('#formulairePerso').attr("title",titre);

         //dialogPopin(nomdiv,largeur,hauteur);},
        $('#formulairePerso').dialog({
        autoOpen: true,
        height: hauteur,
        width: largeur,
        modal : true
    });
    },

     failure: function(msg){alert("Erreur fenetre")}
     });

  }

  //affiche le popup via jquery ui dialog
  function dialogPopin(div,largeur,hauteur,valretour)
  {

  alert('Cette fonction n\'est plus utilisée');

  }

function initOnglets(div)
{
    $(div).tabs();
}

function initBoutonsPhoto (img,idPhoto,btUpload,btRemove)
{

alert ('toto');

 var param = "";

 var action_upload = "includes/ajax/add_upload_photo.php";

 var upload = new AjaxUpload(btUpload, {
                action: action_upload,                
                onChange : function(file , ext){                      
                        //return false;
                        if (! (ext && /^(jpg|png|jpeg|gif|JPEG|GIF|JPG|PNG)$/.test(ext))){
                                // extension is not allowed
                                alert('Erreur: entension de fichier incorrect');
                                // cancel upload
                                return false;
                        }
                        upload.setData({'old': $(idPhoto).val()});

                },                
                onComplete : function(file,response){                  
                  var decoupeResponse =  response.toString().split(";");
                  $(idPhoto).val(decoupeResponse[0]);
                  $(img).attr("src","tmp/"+decoupeResponse[1]);
                }
                });
       

 $(btRemove).click(function(){    
         
         var numetudiant = $('#numetudiant').val();
         var photoTemp = $(idPhoto).val();
         var param = "numEtudiant="+numetudiant+"&photoTemp="+photoTemp;
         $.ajax({
         type: "POST",
         url: "includes/ajax/remove_upload_photo.php",
         data: param,
         success: function(msg){
            if(!msg.toString().match("error"))
            {
                $(img).attr("src","");
            }
            else
            {
                alert("Erreur d'enregistrement");
                return false;
            }
          },
         failure: function(msg){alert("Erreur d'enregistrement")}
         });         
       });      
}

function affichePopup(url,name,option)
{
    var popup = window.open(url,name,option);
    return popup;
}

//function qui rapatrier le donnée et affiche le formulaire
function affichePopin (nomdiv,titre,action,parametre,fichier,largeur,hauteur)
{
 var param = "action="+action;
 if(action=="update"){
 //  param = param +"&id="+id+"&num="+numParent;
    param = param+"&"+parametre;
    var tab_parametre=parametre.split('=');
}

 $.ajax({
 type: "POST",
 url: fichier,
 data: param,
 success: function(msg){
     $(nomdiv).html(msg);
     $(nomdiv).attr("title",titre);
     viewPopin(nomdiv,largeur,hauteur,tab_parametre[1]);},
 failure: function(msg){alert("Erreur fenetre")}
 });

}

/*
* Autocompletion ville,cp,region,pays
* il faut passer les id des champs à remplir
*/

function autocomplete_location(id_city,city,cp,province,state,country)
{
//alert('test');
                if(!state){
                    var state='#state';
                }
                if(!country){
                    var country='#country';
                }
                if(!province){
                    var province='#province';
                }

		$(city).autocomplete({


                        source: "ajax_scripts/req_location.php?element=name_city", //element correspond au champ de selection dans la table

                        focus: function(event, ui){
                                $(city).val(ui.item.value);
                                return false;
                        },
			minLength: 2,
			select: function(event, ui) {
                                $(id_city).val(ui.item.id);
                                $(city).val(ui.item.value);
                                $(cp).val(ui.item.both.cp);
                                if($(state)){
                                    $(state).val(ui.item.both.state);
                                }
                                if($(country)){
                                    $(country).val(ui.item.both.country);
                                }
                                if($(province)){
                                    $(province).val(ui.item.both.province);
                                }

				return false;


                            }
		});

                $(cp).autocomplete({

                        source: "ajax_scripts/req_location.php?element=cp", //element correspond au champ de selection dans la table

                        focus: function(event, ui){
                                $(city).val(ui.item.value);
                                return false;
                        },
			minLength: 2,
			select: function(event, ui) {
                                $(id_city).val(ui.item.id);
                                $(city).val(ui.item.value);
                                $(cp).val(ui.item.both.cp);
                                if($(state)){
                                    $(state).val(ui.item.both.state);
                                }
                                if($(country)){
                                    $(country).val(ui.item.both.country);
                                }
                                if($(province)){
                                    $(province).val(ui.item.both.province);
                                }

				return false;


                            }
		});



}

function autocomplete_partenaire(id_contact_partenaire,id_entreprise_partenaire,raison_sociale_entreprise_partenaire,
    nom_contact_partenaire,returninfo,civilite_contact_partenaire,description_partenaire,id_effectif_partenaire,
    prenom_contact_partenaire,fonction_contact_partenaire,tel_partenaire,gsm_partenaire,fax_partenaire,
    email_partenaire,adresse1_partenaire,adresse2_partenaire,ville_partenaire,cp_partenaire,id_secteur_activite_partenaire,
    id_city_contact_partenaire,id_city_entreprise_partenaire,$id_city_partenaire){

    //alert('test');
            if(!prenom_contact_partenaire){
                var prenom_contact_partenaire='#prenom_contact_partenaire';
            }
            if(!description_partenaire){
                var description_partenaire='#description_partenaire';
            }
            if(!id_effectif_partenaire){
                var id_effectif_partenaire='#id_effectif_partenaire';
            }
            if(!id_secteur_activite_partenaire){
                var id_secteur_activite_partenaire='#id_secteur_activite_partenaire';
            }
            if(!fonction_contact_partenaire){
                var fonction_contact_partenaire='#fonction_contact_partenaire';
            }
            if(!tel_partenaire){
                var tel_partenaire='#tel_partenaire';
            }
            if(!gsm_partenaire){
                var gsm_partenaire='#gsm_partenaire';
            }
            if(!fax_partenaire){
                var fax_partenaire='#fax_partenaire';
            }
            if(!email_partenaire){
                var email_partenaire='#email_partenaire';
            }
            if(!adresse1_partenaire){
                var adresse1_partenaire='#adresse1_partenaire';
            }
            if(!adresse2_partenaire){
                var adresse2_partenaire='#adresse2_partenaire';
            }
            if(!ville_partenaire){
                var ville_partenaire='#ville_partenaire';
            }
            if(!cp_partenaire){
                var cp_partenaire='#cp_partenaire';
            }
            if(!id_city_contact_partenaire){
                var id_city_contact_partenaire='#id_city_contact_partenaire';
            }
            if(!civilite_contact_partenaire){
                var civilite_contact_partenaire='#civilite_contact_partenaire';
            }
            if(!id_city_entreprise_partenaire){
                var id_city_entreprise_partenaire='#id_city_entreprise_partenaire';
            }
            if(!id_city_partenaire){
                var id_city_partenaire='#id_city_partenaire';
            }


            $(raison_sociale_entreprise_partenaire).focus(function(){
                if(($(id_entreprise_partenaire).val()!='')||($(id_contact_partenaire).val()!='')){
                    $('.contact').val('');
                    $(nom_contact_partenaire).val('');
                    $(raison_sociale_entreprise_partenaire).val('');
                }
                    $(':input .contact').removeAttr("readonly");
                    $(':disabled').removeAttr("disabled");
                    $('.ui-buttonset').buttonset({disabled: false});
                    $('.ui-buttonset').buttonset("refresh");

            });
            $(nom_contact_partenaire).focus(function(){
                if(($(id_entreprise_partenaire).val()!='')||($(id_contact_partenaire).val()!='')){
                    $('.contact').val('');
                    $(nom_contact_partenaire).val('');
                    $(raison_sociale_entreprise_partenaire).val('');
                }
                    $(':input .contact').removeAttr("readonly");
                    $(':disabled').removeAttr("disabled");
                    $('.ui-buttonset').buttonset({disabled: false});
                    $('.ui-buttonset').buttonset("refresh");
                

            });


		$(raison_sociale_entreprise_partenaire).autocomplete({


                        source: "ajax_scripts/req_partenaire.php?element=raison_sociale_entreprise&returninfo="+returninfo, //element correspond au champ de selection dans la table

                        focus: function(event, ui){
                                $(raison_sociale_entreprise_partenaire).val(ui.item.value);
                                return false;
                        },
			minLength: 2,
                        delay: 150,
			select: function(event, ui) {
                                $(id_contact_partenaire).val(ui.item.both.id_contact_partenaire);
                                $(id_entreprise_partenaire).val(ui.item.both.id_entreprise_partenaire);
                                $(raison_sociale_entreprise_partenaire).val(ui.item.value);
                                $(nom_contact_partenaire).val(ui.item.both.nom_contact_partenaire);

                                if($(prenom_contact_partenaire)){
                                    $(prenom_contact_partenaire).val(ui.item.both.prenom_contact_partenaire);
                                }
                                if($(description_partenaire)){
                                    $(description_partenaire).val(ui.item.both.description_partenaire);
                                }
                                if($(id_effectif_partenaire)){
                                    $(id_effectif_partenaire).val(ui.item.both.id_effectif_partenaire);
                                }
                                if($(id_secteur_activite_partenaire)){
                                    $(id_secteur_activite_partenaire).val(ui.item.both.id_secteur_activite_partenaire);
                                }
                                if($(fonction_contact_partenaire)){
                                    $(fonction_contact_partenaire).val(ui.item.both.fonction_contact_partenaire);
                                }
                                if($(tel_partenaire)){
                                    $(tel_partenaire).val(ui.item.both.tel_partenaire);
                                }
                                if($(gsm_partenaire)){
                                    $(gsm_partenaire).val(ui.item.both.gsm_partenaire);
                                }
                                if($(fax_partenaire)){
                                    $(fax_partenaire).val(ui.item.both.fax_partenaire);
                                }
                                if($(email_partenaire)){
                                    $(email_partenaire).val(ui.item.both.email_partenaire);
                                }
                                if($(adresse1_partenaire)){
                                    $(adresse1_partenaire).val(ui.item.both.adresse1_partenaire);
                                }
                                if($(adresse2_partenaire)){
                                    $(adresse2_partenaire).val(ui.item.both.adresse2_partenaire);
                                }
                                if($(ville_partenaire)){
                                    $(ville_partenaire).val(ui.item.both.ville_partenaire);
                                }
                                if($(cp_partenaire)){
                                    $(cp_partenaire).val(ui.item.both.cp_partenaire);
                                }
                                if($(id_city_contact_partenaire)){
                                    $(id_city_contact_partenaire).val(ui.item.both.id_city_contact_partenaire);
                                }
                                if($(civilite_contact_partenaire)){
                                    /*$(civilite_contact_partenaire+ui.item.both.civilite_contact_partenaire).next('label').attr('aria-pressed','true');
                                    $(civilite_contact_partenaire+ui.item.both.civilite_contact_partenaire).next('label').addClass('ui-state-active');*/
                                    var radio=$(civilite_contact_partenaire+ui.item.both.civilite_contact_partenaire);
                                    $(civilite_contact_partenaire+ui.item.both.civilite_contact_partenaire)[0].checked=true;
                                    $(civilite_contact_partenaire+ui.item.both.civilite_contact_partenaire).parent('.ui-buttonset').buttonset({disabled: true});
                                    $(civilite_contact_partenaire+ui.item.both.civilite_contact_partenaire).button("refresh");


                                }
                                if($(id_city_entreprise_partenaire)){
                                    $(id_city_entreprise_partenaire).val(ui.item.both.id_city_entreprise_partenaire);
                                }
                                if($(id_city_partenaire)){
                                    $(id_city_partenaire).val(ui.item.both.id_city_partenaire);
                                }

                                $('.contact').attr('readonly','readonly');
                                $(id_effectif_partenaire).attr("disabled","disabled");
                                $(id_secteur_activite_partenaire).attr("disabled","disabled");
                                $(civilite_contact_partenaire).parent().buttonset({disabled: true});
                                $('.ui-buttonset').buttonset("refresh");
				return false;

                            }


		});

                $(nom_contact_partenaire).autocomplete({

                        source: "ajax_scripts/req_partenaire.php?element=nom_contact&returninfo="+returninfo, //element correspond au champ de selection dans la table

                        focus: function(event, ui){
                                $(nom_contact_partenaire).val(ui.item.value);

                                return false;
                        },
			minLength: 2,
                        delay: 150,
			select: function(event, ui) {
                            //*alert(ui.item.both.id_contact_partenaire);

                                $(id_contact_partenaire).val(ui.item.both.id_contact_partenaire);
                                $(id_entreprise_partenaire).val(ui.item.both.id_entreprise_partenaire);
                                $(nom_contact_partenaire).val(ui.item.value);
                                $(raison_sociale_entreprise_partenaire).val(ui.item.both.raison_sociale_entreprise_partenaire);

                                if($(prenom_contact_partenaire)){
                                    $(prenom_contact_partenaire).val(ui.item.both.prenom_contact_partenaire);
                                }
                                if($(description_partenaire)){
                                    $(description_partenaire).val(ui.item.both.description_partenaire);
                                }
                                if($(id_effectif_partenaire)){
                                    $(id_effectif_partenaire).val(ui.item.both.id_effectif_partenaire);
                                }
                                if($(id_secteur_activite_partenaire)){
                                    $(id_secteur_activite_partenaire).val(ui.item.both.id_secteur_activite_partenaire);
                                }
                                if($(fonction_contact_partenaire)){
                                    $(fonction_contact_partenaire).val(ui.item.both.fonction_contact_partenaire);
                                }
                                if($(tel_partenaire)){
                                    $(tel_partenaire).val(ui.item.both.tel_partenaire);
                                }
                                if($(gsm_partenaire)){
                                    $(gsm_partenaire).val(ui.item.both.gsm_partenaire);
                                }
                                if($(fax_partenaire)){
                                    $(fax_partenaire).val(ui.item.both.fax_partenaire);
                                }
                                if($(email_partenaire)){
                                    $(email_partenaire).val(ui.item.both.email_partenaire);
                                }
                                if($(adresse1_partenaire)){
                                    $(adresse1_partenaire).val(ui.item.both.adresse1_partenaire);
                                }
                                if($(adresse2_partenaire)){
                                    $(adresse2_partenaire).val(ui.item.both.adresse2_partenaire);
                                }
                                if($(ville_partenaire)){
                                    $(ville_partenaire).val(ui.item.both.ville_partenaire);
                                }
                                if($(cp_partenaire)){
                                    $(cp_partenaire).val(ui.item.both.cp_partenaire);
                                }
                                if($(id_city_contact_partenaire)){
                                    $(id_city_contact_partenaire).val(ui.item.both.id_city_contact_partenaire);
                                }
                                if($(civilite_contact_partenaire)){
                                    /*$(civilite_contact_partenaire+ui.item.both.civilite_contact_partenaire).next('label').attr('aria-pressed','true');
                                    $(civilite_contact_partenaire+ui.item.both.civilite_contact_partenaire).next('label').addClass('ui-state-active');*/
                                    var radio=$(civilite_contact_partenaire+ui.item.both.civilite_contact_partenaire);
                                    $(civilite_contact_partenaire+ui.item.both.civilite_contact_partenaire)[0].checked=true;
                                    $(civilite_contact_partenaire+ui.item.both.civilite_contact_partenaire).parent('.ui-buttonset').buttonset({disabled: true});
                                    $(civilite_contact_partenaire+ui.item.both.civilite_contact_partenaire).button("refresh");


                                }
                                if($(id_city_entreprise_partenaire)){
                                    $(id_city_entreprise_partenaire).val(ui.item.both.id_city_entreprise_partenaire);
                                }
                                if($(id_city_partenaire)){
                                    $(id_city_partenaire).val(ui.item.both.id_city_partenaire);
                                }

                                $('.contact').attr('readonly','readonly');
                                $(id_effectif_partenaire).attr("disabled","disabled");
                                $(id_secteur_activite_partenaire).attr("disabled","disabled");
                                $(civilite_contact_partenaire).parent().buttonset({disabled: true});
                                $('.ui-buttonset').buttonset("refresh");
				return false;
                            }
		});
}

//initialisation du bouton impression
function initBtImp(div,url,titre,option)
{
   if(option=="") var loption
   var btImp =  $(div);
   btImp.click(function(){
    affichePopup(url,titre,loption);
   });
}

/* Fin Fichier fonctionjs.js */

function close_dialog(){
   $(this).dialog("destroy");
}

function initDatePicker(div,format,jours,mois)
{
   if(format=="") format = "dd/mm/yy";
   $(div).datepicker({dateFormat: format, dayNamesMin:jours , monthNamesShort:mois , changeMonth: true , changeYear: true});
}
