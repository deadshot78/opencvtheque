/* Debut Fichier fonction.js Offres */
/* modification des offres entreprises */

$(function(){

    // Affiche l'offre sélectionnée
    creer_bouton_navgrid('#list','#pager','',popin_view_title,'ui-icon-document','Editer une offre','update',popin_affiche,popin_largeur,popin_hauteur);

    if(session==23){
        // Duplique l'offre sélectionnée
        creer_bouton_navgrid('#list','#pager','',popin_duplicate_title,'ui-icon-newwin','Dupliquer','duplicate',popin_edition,popin_largeur,popin_hauteur);

        // Edite l'offre sélectionnée
        creer_bouton_navgrid('#list','#pager','',popin_update_title,'ui-icon-pencil','Editer une offre','update',popin_edition,popin_largeur,popin_hauteur);


        // Ajoute une offre
        creer_bouton_navgrid('#list','#pager','',popin_add_title,'ui-icon-plus','Ajouter une offre','add',popin_edition,popin_largeur,popin_hauteur);
    }

})

        