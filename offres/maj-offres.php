<?php

$securite=new securite;
$id=$securite->verif_GetPost($_POST['id']);
$action=$securite->verif_GetPost($_POST['action']).$securite->verif_GetPost($_POST['oper']);
$id_contact=$_SESSION['contact'];
$id_new_contact_partenaire="";
$id_new_entreprise_partenaire="";

function option_offres($cnx,$idOffre,$idniveauetude,$idlangueexigee,$idlanguesouhaitee,$table)
{
    try
    {
        $req_del_detail="DELETE FROM ".$table." WHERE idoffre='".$idOffre."'";
        $res_del_detail=$cnx->requeteData($req_del_detail);

        if (count($idniveauetude))
        {

            foreach ($idniveauetude as $row)
            {
                    $req_niveau_etudes .= "INSERT INTO ".$table." (";
                    $req_niveau_etudes .= "idoffre,idniveauetude,created_on";
                    $req_niveau_etudes .= ") VALUES (";
                    $req_niveau_etudes .= "'".$idOffre."',";
                    $req_niveau_etudes .= "'".$row['idniveauetude']."', ";
                    $req_niveau_etudes .= time();
                    $req_niveau_etudes .= "); ";
                    $res_niveau_etudes=$cnx->requeteData($req_niveau_etudes);

            }
        }

        if (count($idlangueexigee))
        {
            foreach ($idlangueexigee as $row)
            {
                    $req_langues_exigees .= "INSERT INTO ".$table." (";
                    $req_langues_exigees .= "idoffre,idlangueexigee,created_on";
                    $req_langues_exigees .= ") VALUES (";
                    $req_langues_exigees .= "'".$idOffre."',";
                    $req_langues_exigees .= "'".$row['idlangueexigee']."', ";
                    $req_langues_exigees .= time();
                    $req_langues_exigees .= "); ";
                    $res_langues_exigees=$cnx->requeteData($req_langues_exigees);

            }
        }

        if (count($idlanguesouhaitee))
        {
            foreach ($idlanguesouhaitee as $row)
            {
                    $req_langues_souhaitees  .= "INSERT INTO ".$table." (";
                    $req_langues_souhaitees .= "idoffre,idlanguesouhaitee,created_on";
                    $req_langues_souhaitees .= ") VALUES (";
                    $req_langues_souhaitees .= "'".$idOffre."',";
                    $req_langues_souhaitees .= "'".$row['idlanguesouhaitee']."', ";
                    $req_langues_souhaitees .= time();
                    $req_langues_souhaitees .= ")";
                    $res_langues_souhaitees = $cnx->requeteData($req_langues_souhaitees);
            }
        }

        $res_detail_candidat=$cnx->requeteMulti($req_niveau_etudes.$req_langues_exigees.$req_langues_souhaitees);

    }

    catch (Exception $e) {
        echo 'Erreur : ',  $e->getMessage(), "\n";
    }

}

if (isset($_POST['action'])>0 || isset($_POST['oper'])>0) // a voir le bouton
{
        $cnx= new actionsdata();
        $cnx->connect();
	
        $securite = new securite();
        $clean_post=$securite->clean_tab($_POST,"encode");

    switch($action)
    {

        case ($action=='add' || $action=='duplicate'):
             try
             {


                //prepare le prochain numero reference offre
                $result=$cnx->requeteSelect("select MAX(reference_offre) as nbre FROM ".$param["table"]["offre"]);
                $tmp=explode ("-",$result[0]['nbre']);
                $reference_offre = $tmp[1]+1;

                if ($tmp[0]!=DATE(y))
                {
                    $reference_offre_out="00001"; /******************/
                }
                else
                {
                    $reference_offre_out="00000";
                    $reference_offre_out=substr_replace($reference_offre_out,$reference_offre,-(strlen($reference_offre)+1),strlen($reference_offre)+1);
                }

                $newreference=DATE(y)."-".$reference_offre_out;

                // ajout nouvel offre
                $req_insert_offre  = "INSERT INTO ".$param["table"]["offre"]." (";
                $req_insert_offre .= "reference_offre,intitule_offre,description_offre,";
                $req_insert_offre .= "created_on,type_offre,date_debut_offre,profil_candidat_offre,";
                $req_insert_offre .= "salaire_max_offre,offre_partenaire,";
                $req_insert_offre .= "entreprise_anonym,";
                $req_insert_offre .= "idduree,iddeplacement,avantage,despossible,";
                $req_insert_offre .= "id_city,id_entreprise_partenaire,id_contact_partenaire,id_contact,";
                $req_insert_offre .= "statut_offre,created_on";
                $req_insert_offre .= ") VALUES (";
                $req_insert_offre .= "'".$newreference."',";
    //---
                $req_insert_offre .= "'".$clean_post['intitule_offre']."',";
                $req_insert_offre .= "'".$clean_post['description_offre']."',";
                $req_insert_offre .= "'".DATE("Y-m-d")."', ";
                $req_insert_offre .= "'".$clean_post['type_offre']."',";
                $req_insert_offre .= "'".conv_date_en($_POST['date_debut_offre'])."',";
                $req_insert_offre .= "'".$clean_post['profil_candidat_offre']."', ";
                $req_insert_offre .= "'".$clean_post['salaire_max_offre']."',";
                $req_insert_offre .= "'".$clean_post['offre_partenaire']."',";
                $req_insert_offre .= "'".$clean_post['entreprise_anonym']."',";
                $req_insert_offre .= "'".$clean_post['idduree']."',";
                $req_insert_offre .= "'".$clean_post['iddeplacement']."',";
                $req_insert_offre .= "'".$clean_post['avantage']."',";
                $req_insert_offre .= "'".$clean_post['despossible']."',";
                $req_insert_offre .= "'".$clean_post['id_city']."',";
                //---
                //Ajout entreprise
                if($clean_post['id_entreprise_partenaire']==''){
                    $id_new_entreprise_partenaire=creer_entreprise_partenaire($id_contact,$clean_post,$param["table"]["entreprises_partenaire"]);
                    $id_entreprise_partenaire=$id_new_entreprise_partenaire;
                    $req_insert_offre .= "'".$id_new_entreprise_partenaire."', ";
                }
                else {
                    $req_insert_offre .= "'".$clean_post['id_entreprise_partenaire']."', ";
                    $id_entreprise_partenaire=$clean_post['id_entreprise_partenaire'];
                }

                //Ajout contact
                if($clean_post['id_contact_partenaire']==''){
                    $id_new_contact_partenaire=creer_contact_partenaire($id_contact,$clean_post,$id_entreprise_partenaire,$param["table"]["contacts_partenaire"]);
                    $req_insert_offre .= "'".$id_new_contact_partenaire."', ";
                }
                else {
                    $req_insert_offre .= "'".$clean_post['id_contact_partenaire']."', ";
                }
                //---
                $req_insert_offre .= "'".$id_contact."',";
                $req_insert_offre .= "'0',";
                $req_insert_offre .= time().")";
                $res_insert_offre=$cnx->requeteData($req_insert_offre);
                $id_new_offre=$cnx->last_id();                                //dernier enregistrement


                // ajout options pour les offres
                 option_offres($cnx,$id_new_offre,$clean_post['idniveauetude'],$clean_post['idlangueexigee'],$clean_post['idlanguesouhaitee'],$param["table"]["offre_detail"]);

                 //     $mail = new mail();
                 //     $r=$mail->offre_envoie_mail(1, $dernierOffre,$_SESSION["login"],$_POST['email']);

            }
             catch (Exception $e) {
                echo 'Erreur : ',  $e->getMessage(), "\n";
            }

            break;

       case 'update': // Mise à jour de l'offre

    //  entreprise
           $req_update_entreprise_partenaire  = "UPDATE ".$param["table"]["entreprises_partenaire"]." SET ";
           $req_update_entreprise_partenaire .= "SECTEUR_ACTIVITE_ENTREPRISE = '".$clean_post['id_secteur_activite_partenaire']."',";
           $req_update_entreprise_partenaire .= "ideffectif = '".$clean_post['id_effectif_partenaire']."',";
           $req_update_entreprise_partenaire .= "description = '".$clean_post['description_partenaire']."',";
           $req_update_entreprise_partenaire .= "WHERE id_entreprise = '".$clean_post['id_entreprise_partenaire']."' ";
           $res_update_entreprise_partenaire=$cnx->requeteData($req_update_entreprise_partenaire);

    // offre

            $req_update_offre  = "UPDATE ".$param["table"]["offre"]." SET ";
            $req_update_offre .= "intitule_offre = '".$clean_post['intitule_offre']."',";
            $req_update_offre .= "description_offre = '".$clean_post['description_offre']."',";
            $req_update_offre .= "type_offre = '".$clean_post['type_offre']."',";
            $req_update_offre .= "offre_partenaire = '".$clean_post['offre_partenaire']."',";
            $req_update_offre .= "id_city = '".$clean_post['id_city']."',";
            $req_update_offre .= "iddeplacement = '".$clean_post['iddeplacement']."',";
            $req_update_offre .= "date_debut_offre = '".conv_date_en($clean_post['date_debut_offre'])."',";
            $req_update_offre .= "salaire_max_offre = '".$clean_post['salaire_max_offre']."',";
            $req_update_offre .= "avantage = '".$clean_post['avantage']."',";
            $req_update_offre .= "profil_candidat_offre = '".$clean_post['profil_candidat_offre']."', ";
            $req_update_offre .= "despossible = '".$clean_post['despossible']."', ";
            $req_update_offre .= "entreprise_anonym = '".$clean_post['entreprise_anonym']."', ";
            $req_update_offre .= "idduree = '".$clean_post['idduree']."', ";
            $req_update_offre .= "updated_on=".time()." ";

            if($clean_post['offre_partenaire']==1){
                if($clean_post['id_entreprise_partenaire']==''){
                    $id_new_entreprise_partenaire=creer_entreprise_partenaire($cnx,$clean_post,$param["table"]["entreprises_partenaire"]);
                    $id_entreprise_partenaire=$id_new_entreprise_partenaire;
                    $req_update_offre .= ", id_entreprise_partenaire = '".$id_new_entreprise_partenaire."', ";
                }
                else {
                    $req_update_offre .= ", id_entreprise_partenaire = '".$clean_post['id_entreprise_partenaire']."', ";
                    $id_entreprise_partenaire=$clean_post['id_entreprise_partenaire'];
                }
                if($clean_post['id_contact_partenaire']==''){
                    $id_new_contact_partenaire=creer_contact_partenaire($cnx,$clean_post,$id_entreprise_partenaire,$param["table"]["contacts_partenaire"]);
                    $req_update_offre .= "id_contact_partenaire = '".$id_new_contact_partenaire."' ";
                }
                else {
                    $req_update_offre .= "id_contact_partenaire = '".$clean_post['id_contact_partenaire']."' ";
                }
            }


            $req_update_offre .= "WHERE ID_OFFRE = '".$clean_post['id_offre']."' ";
//echo $req_update_offre;
            $res_update_offre=$cnx->requeteData($req_update_offre);
            //print_r($res_update_offre);
            // options pour les offres
            option_offres($cnx,$clean_post['id_offre'],$clean_post['idniveauetude'],$clean_post['idlangueexigee'],$clean_post['idlanguesouhaitee'],$param["table"]["offre_detail"]);

            break;

        case 'del':
            $req_del_offre = "UPDATE ".$param["table"]["offre"]." SET ";
            $req_del_offre .= "updated_on=".time().", ";
            $req_del_offre .= "statut_offre=2 WHERE id_offre=".$clean_post['id']." AND id_contact=".$id_contact;
            $res_del_offre=$cnx->requeteData($req_del_offre);

            $cnx->deconnect();
            break;

    }

}

?>
