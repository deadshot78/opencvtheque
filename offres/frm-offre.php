<?php


$securite=new securite;
if(isset($_POST['id'])){
    $id = $securite->verif_GetPost($_POST['id']);    
} else {
    $id = "";
}

$niveauetude =  array();
$langueexigee =  array();
$languesouhaitee =  array();

$action=$securite->verif_GetPost($_POST['action']);
$contact=$_SESSION['contact'];

$data_offre = array('id_offre'=>'','offre_partenaire'=>'','intitule_offre'=> '',
                    'description_offre'=> '','city_offre'=> '', 'cp_offre'=> '',
                    'province_offre'=> '','state_offre'=> '', 'country_offre'=> '',
                    'type_offre'=> '', 'idduree'=> '','iddeplacement'=> '',
                    'salaire_max_offre'=> '', 'avantage'=> '', 'profil_candidat_offre'=> '',
                    'despossible'=> '', 'date_depot'=> '','date_debut_offre'=> '',
                    'secteur_activite_entreprise'=> '','entreprise'=> '',
                    'description'=> '','adresse'=> '','adresse2'=> '',
                    'cp_contact'=> '','ville_contact'=> '','tel_contact'=> '',
                    'gsm_contact'=> '','fax_contact'=> '','email_contact'=> '',
                    'civilite_contact'=> '','nom_contact'=> '','prenom_contact'=> '',
                    'fonction_contact'=> '','entreprise_anonym'=> '',
                    'id_entreprise_partenaire' => '','id_contact_partenaire' => '',
                    'id_contact'=> '','id_entreprise'=> '','id_city'=> '',
                    'id_city_partenaire'=> '','ideffectif'=> '','id'=> '');

$result = "";

?>
<script type="text/javascript">
var jours = <?php echo $param["datepicker"]["dayNamesMin"] ;?>;
var mois  =<?php echo $param["datepicker"]["monthNamesShort"] ;?>;

<?php include_once ("includes/js/form-fonctions.js"); ?>

$(function() { 

    $('#offre_partenaire').click(function(){
        //alert('toto');
        $('#fldpartenaire').slideToggle("fast");
        $('#id_contact_partenaire').val(' ');
        $('.contact').val('');
    });

    if($('#id_contact_partenaire').val()==''){
        $('#fldpartenaire').hide();
    }

    autocomplete_location('#id_city','#ville_offre','#cp_offre','#departement_offre','#region_offre','#pays_offre');
    autocomplete_location('#id_city_partenaire','#ville_partenaire','#cp_partenaire');
    autocomplete_partenaire('#id_contact_partenaire','#id_entreprise_partenaire','#raison_sociale_entreprise_partenaire','#nom_contact_partenaire','mix');

    $("#despossible,#entreprise_anonym,#offre_partenaire").button();
    $("#radio_civilite_contact_partenaire,#radio_type_offre,#radio_idduree,#radio_iddeplacement,#checkbox_idniveauetude,#checkbox_idlangueexigee,#checkbox_idlanguesouhaitee").buttonset();
  
  initDatePicker("#date_debut_offre","dd/mm/yy",jours,mois);

});


</script>
<?php

	$cnx=new actionsdata();
	$cnx->connect();


  	if ($id)  
	{
 
            $req_select_offre = "SELECT id_offre,id_city,description_offre,region_offre,lieu_offre,";
            $req_select_offre .="date_debut_offre,intitule_offre,type_offre,idduree,iddeplacement,profil_candidat_offre,";
            $req_select_offre .= "avantage,despossible,entreprise_anonym,id_entreprise_partenaire,";
            $req_select_offre .= "salaire_max_offre,id_contact_partenaire,offre_partenaire,created_on";

            $req_select_offre .=" FROM ".$param["table"]["offre"]." a ";//,".$param["view"]["partenaires"]." b ";
            $req_select_offre .= "WHERE a.id_offre=".$id." ";
            //$req_select_offre .= "AND (a.id_entreprise_partenaire = b.id OR a.id_contact_partenaire = b.id_contact_partenaire)";
            //$req_select_offre .= "AND c.id_entreprise = b.id ";

            //    echo $req_select_offre;

// RÃ©cupÃ©ration des informations concernant la localisation de l'offre
// si l'info n'existe pas c'est une ancienne offre on rÃ©cupÃ¨re alors les infos depuis l'offre

	    $res_select_offre=$cnx->requeteSelect($req_select_offre);
            if($res_select_offre[0]['id_city']!=0){

                $data_localisation=get_localisation($res_select_offre[0]['id_city'],$param);

                $cp=$data_localisation[0]['cp'];
                $city=$data_localisation[0]['name_city'];
                $province=$data_localisation[0]['name_province'];
                $state=$data_localisation[0]['name_state'];
                $country=$data_localisation[0]['name_country'];
                //print_r($data_localisation);

            }
            else{
                $cp='';
                $city=$res_select_offre[0]['lieu_offre'];
                $province='';
                $state=$res_select_offre[0]['region_offre'];
                $country='';
            }
            
// Récupération des informations du partenaire = Contact_partenaire + Entreprise partenaire
            if($res_select_offre[0]['offre_partenaire']==1){
                if($res_select_offre[0]['id_contact_partenaire']!=''){
                    $info_partenaire=get_partenaire('id_contact_partenaire',$res_select_offre[0]['id_contact_partenaire'], $contact, $param);
                    $data_localisation_partenaire=get_localisation($info_partenaire[0]['both']['id_city_partenaire'], $param);

                }
                else{
                    $info_partenaire=get_partenaire('id',$res_select_offre[0]['id_entreprise_partenaire'], $contact, $param);
                    $data_localisation_partenaire=get_localisation($info_partenaire[0]['both']['id_city_partenaire'], $param);
                }


                if($info_partenaire[0]['both']['id_city_partenaire']!=0){
                    $data_localisation_partenaire=get_localisation($info_partenaire[0]['both']['id_city_partenaire'], $param);
                    $cp_partenaire=$data_localisation_partenaire[0]['cp'];
                    $ville_partenaire=$data_localisation_partenaire[0]['name_city'];
                }
                else{
                    $cp_partenaire=$info_partenaire[0]['both']['cp_partenaire'];
                    $ville_partenaire=$info_partenaire[0]['both']['ville_partenaire'];
                }

            }
            //echo "<pre>".print_r($info_partenaire)."</pre>";

	 $data_offre = array(
	 			'id_offre'=> $res_select_offre[0]['id_offre'],
                                'offre_partenaire'=>$res_select_offre[0]['offre_partenaire'],
	 			'intitule_offre'=> $res_select_offre[0]['intitule_offre'],
	 			'description_offre'=> $res_select_offre[0]['description_offre'],
    	 			'city_offre'=> $city,
                                'cp_offre'=> $cp,
	 			'province_offre'=> $province,
                                'state_offre'=>$state,
                                'country_offre'=>$country,
	 			'type_offre'=> $res_select_offre[0]['type_offre'],
	 			'idduree'=> $res_select_offre[0]['idduree'],
	 			'iddeplacement'=> $res_select_offre[0]['iddeplacement'],
	 			'salaire_max_offre'=> $res_select_offre[0]['salaire_max_offre'],
	 			'avantage'=> $res_select_offre[0]['avantage'],
	 			'profil_candidat_offre'=> $res_select_offre[0]['profil_candidat_offre'],
	 	 		'despossible'=> $res_select_offre[0]['despossible'],
	 			'date_depot'=> $res_select_offre[0]['created_on'],
	 			'date_debut_offre'=> $res_select_offre[0]['date_debut_offre'],
				'secteur_activite_entreprise'=> $info_partenaire[0]['both']['id_secteur_activite_partenaire'],
	 			'entreprise'=> $info_partenaire[0]['both']['raison_sociale_entreprise_partenaire'],
				'description'=> $info_partenaire[0]['both']['description_partenaire'],
				'adresse'=> $info_partenaire[0]['both']['adresse1_partenaire'],
                                'adresse2'=> $info_partenaire[0]['both']['adresse2_partenaire'],
                                'cp_contact'=> $cp_partenaire,
                                'ville_contact'=> $ville_partenaire,
				'tel_contact'=> $info_partenaire[0]['both']['tel_partenaire'],
                                'gsm_contact'=> $info_partenaire[0]['both']['gsm_partenaire'],
				'fax_contact'=> $info_partenaire[0]['both']['fax_partenaire'],
				'email_contact'=> $info_partenaire[0]['both']['email_partenaire'],
				'civilite_contact'=> $info_partenaire[0]['both']['civilite_contact_partenaire'],
				'nom_contact'=> $info_partenaire[0]['both']['nom_contact_partenaire'],
				'prenom_contact'=> $info_partenaire[0]['both']['prenom_contact_partenaire'],
				'fonction_contact'=> $info_partenaire[0]['both']['fonction_contact_partenaire'],
	 			'entreprise_anonym'=> $res_select_offre[0]['entreprise_anonym'],
                                'id_entreprise_partenaire' => $info_partenaire[0]['both']['id_entreprise_partenaire'],
                                'id_contact_partenaire' => $info_partenaire[0]['both']['id_contact_partenaire'],
	 			'id_contact'=> $res_select_offre[0]['id_contact_partenaire'],
	 			'id_entreprise'=> $res_select_offre[0]['id_entreprise_partenaire'],
                                'id_city'=> $res_select_offre[0]['id_city'],
                                'id_city_partenaire'=> $info_partenaire[0]['both']['id_city_partenaire'],
	 			'ideffectif'=> $info_partenaire[0]['both']['id_effectif_partenaire'],
	 	 		//'id'=> $res_select_offre[0]['id']
	 
	 );

//	 	$date_debut_offre=$data_offre['date_debut_offre'];

		$sql = "SELECT idniveauetude, idlangueexigee, idlanguesouhaitee ";
                $sql .=" FROM cv_offre_details ";
		$sql .= "WHERE idoffre=".$res_select_offre[0]['id_offre']." ";
		$sql .= "ORDER BY idniveauetude, idlangueexigee, idlanguesouhaitee ";

	    $data=$cnx->requeteSelect ($sql);

		if ($data)
		{
			foreach ($data as $row)
			{
				if ($row['idniveauetude'])   $niveauetude[] = $row['idniveauetude'];
				if ($row['idlangueexigee'])  $langueexigee[] = $row['idlangueexigee'];
				if ($row['idlanguesouhaitee'])  $languesouhaitee[] = $row['idlanguesouhaitee'];
			}
		} 


		 
	}
	


$form = new formulaire ();
$result .= $form->form_init('gestionoffreent','redirecteur.php?dest=offres_maj-offres','POST','formulaire-fiche');
$result .= $form->creer_une_checkbox('offre_partenaire','Partenaire','Offre partenaire','no',1,$data_offre['offre_partenaire'],'',2);
//$result .= $form->creer_bouton("",'btpartenaire','button','button');
$result .= "<h1 id='titre_page'>".$param["gestionoffreent"][$action]." ".$id."</h1>";


$result .= $form->creer_text('action','action','no','hidden',$action,'','',2,60,'',0);

$result .= $form->creer_text('id_offre','id_offre','no','hidden',$data_offre['id_offre'],'civilite','numeric',2,60,'',0);
$result .= $form->creer_text('id_contact','id_contact','no','hidden',$data_offre['id_contact'],'civilite','numeric',2,60,'',0);
$result .= $form->creer_text('id_entreprise','id_entreprise','no','hidden',$data_offre['id_entreprise'],'civilite','numeric',2,60,'',0);

$result .= $form->fieldset_new();

$value=$cnx->select_liste('cfg_type_offre',"id","nom",'','false');
$result .= $form->creer_radio('type_offre','Type','yes',$value,$data_offre['type_offre'],'type_offre','radio','',1);
$result .= $form->fieldset_end();

//-----
$result .= $form->fieldset_new("Partenaire","fldpartenaire");

$result .= $form->creer_text('raison_sociale_entreprise_partenaire','Entreprise','yes','text',$data_offre['entreprise'],'civilite','alpha',2,60,'','',3);
$value="../includes/listes/sel-secteurs-".$Langue.".xml";
$result .= $form->creer_select('id_secteur_activite_partenaire','Secteur d\'activité','no',$value,TRUE,$data_offre['secteur_activite_entreprise'],'','',4,TRUE,true,'contact');
$value=$cnx->select_liste('cfg_effectif',"id","nom",'','false');
$result .= $form->creer_select('id_effectif_partenaire','Effectif','no',$value,TRUE,$data_offre['ideffectif'],200,'',5,FALSE,true,'contact');

$result .= $form->creer_textarea('description_partenaire','Description de l\'entreprise','description_partenaire',$data_offre['description'],'',2,50,'','','',6,'contact',TRUE);

$result .= $form->creer_une_checkbox('entreprise_anonym','Anonyme','Mode contact','no',1,$data_offre['entreprise_anonym'],'',2);

if($action=='update'){
    $disable=true;
}
else{
    $disable=false;
}

$result .= $form->creer_text('id_contact_partenaire','','no','hidden',$data_offre['id_contact_partenaire'],'contact','num',2,60,'','',22);
$result .= $form->creer_text('id_entreprise_partenaire','','no','hidden',$data_offre['id_entreprise_partenaire'],'contact','num',2,60,'','',22);
$result .= $form->creer_text('id_city_partenaire','','no','hidden',$data_offre['id_city_partenaire'],'contact','num',0,60,'','',22);
$result .= $form->creer_radio('civilite_contact_partenaire','Civilite','no',$param['civilite'],$data_offre['civilite_contact'],'civilite_contact_partenaire','radio','',21,$disable);
$result .= $form->creer_text('nom_contact_partenaire','Nom','yes','text',$data_offre['nom_contact'],'civilite','alpha',2,60,'','',22);
$result .= $form->creer_text('prenom_contact_partenaire','Prénom','no','text',$data_offre['prenom_contact'],'civilite contact','alpha',2,60,'',$disable,23);
$result .= $form->creer_text('fonction_contact_partenaire','Fonction','no','text',$data_offre['fonction_contact'],'civilite contact','alphanumeric',2,60,'',$disable,24);
$result .= $form->creer_text('tel_partenaire','Tel','no','text',$data_offre['tel_contact'],'civilite contact','alphanumeric',2,60,'',$disable,24);
$result .= $form->creer_text('gsm_partenaire','Mob','no','text',$data_offre['gsm_contact'],'civilite contact','alphanumeric',2,60,'',$disable,24);
$result .= $form->creer_text('fax_partenaire','Fax','no','text',$data_offre['fax_contact'],'civilite contact','alphanumeric',2,60,'',$disable,24);
$result .= $form->creer_text('email_partenaire','email','no','text',$data_offre['email_contact'],'civilite contact','alphanumeric',2,60,'',$disable,24);

$result .= $form->creer_text('adresse1_partenaire','Adresse','no','text',$data_offre['adresse'],'civilite contact','alphanumeric',2,60,'',$disable,24);
$result .= $form->creer_text('adresse2_partenaire','Adresse suite','no','text',$data_offre['adresse2'],'civilite contact','alphanumeric',2,60,'',$disable,24);
$result .= $form->creer_text('cp_partenaire','CP','no','text',$data_offre['cp_contact'],'civilite contact','alphanumeric',2,60,'','',24);
$result .= $form->creer_text('ville_partenaire','Ville','no','text',$data_offre['ville_contact'],'civilite contact','alphanumeric',2,60,'','',24);


$result .= $form->fieldset_end();
//-----
$result .= $form->fieldset_new("Mission");
$result .= $form->creer_text('intitule_offre','Intitulé du poste','yes','text',$data_offre['intitule_offre'],'civilite','alpha',2,60,'','',7);
$result .= $form->creer_textarea('description_offre','Description de la mission','description_offre',$data_offre['description_offre'],'yes',2,50,7,'','',8);
$result .= $form->creer_date('date_debut_offre','Date début','non',$data_offre['date_debut_offre'],'civilite',9);
$result .= $form->creer_une_checkbox('despossible','Dès que possible','Ou','no','1',$data_offre['despossible'],'',10);
$value=$cnx->select_liste('cfg_duree',"id","nom",'','false');
$result .= $form->creer_radio('idduree','Durée','yes',$value,$data_offre['idduree'],'idduree','radio','',11);

/*$value=$cnx->select_liste('cv_province',"id_departement","name_province_uppercase");
$result .= $form->creer_select('region_offre','RÃ©gion','no',$value,TRUE,$data_offre['region_offre'],200,12);

$result .= $form->creer_text('hiddencp','id cp','no','hidden','','','','','','0');
$result .= $form->creer_text('hiddenlieu_offre','id ville','no','hidden', $data_offre['lieu_offre'],'','','','','0');
$result .= $form->creer_text('lieu_offre','Ville','non','text',$data_offre['lieu_offre'],'civilite','alnumhyph',2,60,'',13);*/
///////

$result .= $form->creer_text('cp_offre','CP','yes','text',$data_offre['cp_offre'],'','alnum','2','60','','',12);
$result .= $form->creer_text('ville_offre','Ville','yes','text',$data_offre['city_offre'],'','alnum','2','60','','',13);
$result .= $form->creer_text('id_city','','yes','hidden',$data_offre['id_city'],'','numeric',0,14);
$result .= $form->creer_text('departement_offre','Département','no','text',$data_offre['province_offre'],'','alnum','2','60','','true',15);
$result .= $form->creer_text('region_offre','Région','no','text',$data_offre['state_offre'],'','alnum','2','60','','true',16);
$result .= $form->creer_text('pays_offre','Pays','no','text',$data_offre['country_offre'],'','alpha','2','60','','true',17);
///////

$value=$param["reponse"];
$result .= $form->creer_radio('iddeplacement','Déplacements éventuels','non',$value,$data_offre['iddeplacement'],'iddeplacement','radio','',18);
                //creer_text($int_champ,$nametext,$require='NO',$type,$variable,$class='',$rules='',$minlength='',$maxlength='',$size='',$readOnly=false,$tabindex='')
$result .= $form->creer_text('salaire_max_offre','Rémunération proposée','','text',$data_offre['salaire_max_offre'],'civilite','alphanum',2,60,'','',19);
$result .= $form->creer_text('avantage','Avantages','no','text',$data_offre['avantage'],'civilite','alpha',2,60,'','',20);
$result .= $form->fieldset_end();
//-----
$result .= $form->fieldset_new("Profil candidat");
$value=$cnx->select_liste('cfg_niveau_etude',"id","nom");
//                                $int_champ,$nametext,$require,$valeur_case,$ichecked,$name='',$type='checkbox',$class='',$tabindex=''
$result .= $form->creer_checkbox('niveau_etude','Niveau d\'Etudes','no',$value,$niveauetude,'idniveauetude','checkbox','',21);
$value=$cnx->select_liste('cfg_langue',"id","nom");
$result .= $form->creer_checkbox('langue_exigee','Langue(s) exigée(s)','no',$value,$langueexigee,'idlangueexigee','checkbox','',22);
$result .= $form->creer_checkbox('langue_souhaitee','Langue(s) souhaitée(s)','no',$value,$languesouhaitee,'idlanguesouhaitee','checkbox','',23);

$result .= $form->creer_textarea('profil_candidat_offre','Autres précisions','profil_candidat_offre',$data_offre['profil_candidat_offre'],'',2,50,'','','',24);
$result .= $form->fieldset_end();

//-----
/*$result .= $form->fieldset_new("Contact");
$result .= $form->fieldset_end();*/

$result .= "<br />";

$result .= $form->creer_bouton($param["action"]["cancel"],'bt_cancel','button','button');
$result .= $form->creer_bouton($param["action"][$action],'','submit');


$result .= $form->form_end();

?>

<div id="formOffre">  
   <?php echo $result; ?>
</div>



