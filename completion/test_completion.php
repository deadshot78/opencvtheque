<html>
<head>
<meta charset="UTF-8" />

        <script type="text/javascript" src="../includes/js/jquery-1.4.2.min.js"></script>
        <script type="text/javascript" src="../includes/js/jquery-ui-1.8.2.custom.min.js"></script>
        <script type="text/javascript" src="../includes/js/jquery.ui.autocomplete.js"></script>






	<script type="text/javascript">
	$(function() {

		$("#ville").autocomplete({

                        source: "req_data.php?element=name_city", //element correspond au champ de selection dans la table

                        focus: function(event, ui){
                                $('#ville').val(ui.item.value);
                                return false;
                        },
			minLength: 2,
			select: function(event, ui) {
                                $("#ville").val(ui.item.value);
                                $('#cp').val(ui.item.both.cp);
                                if($('#region')){
                                    $('#region').val(ui.item.both.region);    
                                }
                                if($('#pays')){
                                    $('#pays').val(ui.item.both.pays);
                                }
                                
                                
				return false;


                            }
		});
	});
	</script>
	<style>
		.ui-autocomplete-loading { background: url(indicator.gif) no-repeat right; }
		#ville { width: 25em; }
	</style>

</head>
<body>
<div class="demo">

<div class="ui-widget">
	<label for="ville">Your city: </label>
	<input id="ville" />
	Powered by <a href="http://geonames.org">geonames.org</a>
</div>

<div class="ui-widget" style="margin-top:2em; font-family:Arial">
	Result:
        <input id="cp"/>
        <!--<input id="region"/>-->
        <input id="pays"/>


	<div id="log" style="height: 200px; width: 300px; overflow: auto;" class="ui-widget-content"></div>
</div>

</div><!-- End demo -->

<div class="demo-description">
<p>
The Autocomplete widgets provides suggestions while you type into the field. Here the suggestions are cities, displayed when at least two characters are entered into the field.
</p>
<p>
In this case, the datasource is the <a href="http://geonames.org">geonames.org webservice</a>. While only the city name itself ends up in the input after selecting an element, more info is displayed in the suggestions to help find the right entry. That data is also available in callbacks, as illustrated by the Result area below the input.
</p>
</div><!-- End demo-description -->
</body>
</html>