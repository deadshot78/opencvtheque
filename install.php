<?php
/*
* install.php Created 31 janv. 2011 at 13:51:34 by flenoble under Ocv-NG
* $Id$
*/
$etape = 0;

if(isset ($_POST['etape'])){
    $etape = $_POST['etape'];
}

$mailadmin = "";

if(isset($_POST['mail-admin'])) $mailadmin = $_POST['mail-admin'];

function logtable($table,$statut){
    if($statut[0] == 1) {
        $str_return = "<tr><td>".$table."</td><td colspan=2> OK </td></tr>";
    } else {
        $str_return = "<tr><td>".$table."</td><td> PAS OK </td><td>".$statut[1]."</td></tr>";
    }
    return $str_return;
}

$app_path =  constant("ABSPATH");
require_once $app_path.'/conf/_connect.php';

$str = "<div id='login'>";
$str .= "<h1><a href='#' >Installation OpenCvtheque NG</a></h1><br/>";
$form = new formulaire();
$str .= $form->form_init('installation','.','POST','formulaire');

switch ($etape){
    case 1 :

        $cnx = new actionsdata();
        $cnx->connect();

        //Création des tables
        $str .= "<table><thead><td>Table</td><td colspan=2>Statut</td></thead><tbody>";

        /*
        * Structure de la table cfg_duree
        */

        $req_create_table = "CREATE TABLE IF NOT EXISTS cfg_duree (
          id tinyint(5) NOT NULL AUTO_INCREMENT,
          nom varchar(20) COLLATE utf8_unicode_ci NOT NULL,
          PRIMARY KEY (id)
        ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci";
        $res_create_table = $cnx->requeteData($req_create_table);

        $str .= logtable("cfg_duree",$res_create_table);

        /*
        * Structure de la table cfg_effectif
        */

        $req_create_table = "CREATE TABLE IF NOT EXISTS cfg_effectif (
          id tinyint(5) NOT NULL AUTO_INCREMENT,
          nom varchar(20) COLLATE utf8_unicode_ci NOT NULL,
          PRIMARY KEY (id)
        ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci";
        $res_create_table = $cnx->requeteData($req_create_table);

        $str .= logtable("cfg_effectif",$res_create_table);

        /*
        * Structure de la table cfg_email
        */

        $req_create_table = "CREATE TABLE IF NOT EXISTS cfg_email (
          id int(5) NOT NULL AUTO_INCREMENT,
          sujet varchar(255) NOT NULL,
          body text NOT NULL,
          piece_jointe varchar(255) NOT NULL,
          priority text NOT NULL,
          receipt int(1) NOT NULL,
          PRIMARY KEY (id)
        ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci";
        $res_create_table = $cnx->requeteData($req_create_table);

        $str .= logtable("cfg_email",$res_create_table);

        /*
        * Structure de la table cfg_langue
        */

        $req_create_table = "CREATE TABLE IF NOT EXISTS cfg_langue (
          id tinyint(5) NOT NULL AUTO_INCREMENT,
          nom varchar(20) COLLATE utf8_unicode_ci NOT NULL,
          PRIMARY KEY (id)
        ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci";
        $res_create_table = $cnx->requeteData($req_create_table);

        $str .= logtable("cfg_langue",$res_create_table);

        /*
        * Structure de la table cfg_niveau_etude
        */

        $req_create_table = "CREATE TABLE IF NOT EXISTS cfg_niveau_etude (
          id tinyint(5) NOT NULL AUTO_INCREMENT,
          nom varchar(20) COLLATE utf8_unicode_ci NOT NULL,
          PRIMARY KEY (id)
        ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci";
        $res_create_table = $cnx->requeteData($req_create_table);

        $str .= logtable("cfg_niveau_etude",$res_create_table);

        /*
        * Structure de la table cfg_type_offre
        */

        $req_create_table = "CREATE TABLE IF NOT EXISTS cfg_type_offre (
          id tinyint(5) NOT NULL AUTO_INCREMENT,
          nom varchar(20) COLLATE utf8_unicode_ci NOT NULL,
          PRIMARY KEY (id)
        ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci";
        $res_create_table = $cnx->requeteData($req_create_table);

        $str .= logtable("cfg_type_offre",$res_create_table);

        /*
        * Structure de la table cv_acquerir_competence
        */

        $req_create_table = "CREATE TABLE IF NOT EXISTS cv_acquerir_competence (
          id_contact int(11) NOT NULL DEFAULT '0',
          id_competence int(11) NOT NULL DEFAULT '0',
          id_langue_competence int(11) NOT NULL,
          description_competence text character set utf8 collate utf8_unicode_ci NOT NULL,
          created_on int(11) NOT NULL,
          updated_on int(11) NOT NULL,
          UNIQUE KEY competence (id_contact,id_competence,id_langue_competence)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci";
        $res_create_table = $cnx->requeteData($req_create_table);

        $str .= logtable("cv_acquerir_competence",$res_create_table);

        /*
        * Structure de la table cv_administrateur
        */

        $req_create_table = "CREATE TABLE IF NOT EXISTS cv_administrateur (
          ID_ADMIN int(11) NOT NULL AUTO_INCREMENT,
          NOM_ADMIN varchar(250) NOT NULL DEFAULT '',
          PRENOM_ADMIN varchar(250) NOT NULL DEFAULT '',
          LOGIN_ADMIN varchar(250) NOT NULL DEFAULT '',
          PASSWORD_ADMIN varchar(250) NOT NULL DEFAULT '',
          EMAIL_ADMIN varchar(250) NOT NULL DEFAULT '',
          MESSAGE_SITE_ADMIN varchar(250) NOT NULL DEFAULT '',
          LOGO_SITE_ADMIN varchar(250) NOT NULL DEFAULT '',
          URL_WAP_ADMIN varchar(250) NOT NULL DEFAULT '',
          COPYRIGHT_SITE_ADMIN varchar(250) NOT NULL DEFAULT '',
          PREFIXE_OFFRE_ADMIN varchar(5) NOT NULL,
          SUFFIXE_OFFRE_ADMIN int(11) NOT NULL,
          created_on int(11) NOT NULL,
          updated_on int(11) NOT NULL,
          PRIMARY KEY (ID_ADMIN)
        ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci";
        $res_create_table = $cnx->requeteData($req_create_table);

        $str .= logtable("cv_administrateur",$res_create_table);

        /*
        * Structure de la table cv_candidat
        */

        $req_create_table = "CREATE TABLE IF NOT EXISTS cv_candidat (
          ID_CANDIDAT int(11) NOT NULL AUTO_INCREMENT,
          NB_ANNEE_EXPERIENCE_CANDIDAT varchar(250) NOT NULL DEFAULT '0',
          DATE_CREATION_CANDIDAT date NOT NULL DEFAULT '0000-00-00',
          DATE_MODIFICATION_CANDIDAT date NOT NULL DEFAULT '0000-00-00',
          CIVILITE_CANDIDAT varchar(250) DEFAULT NULL,
          NIVEAU_ETUDES_CANDIDAT varchar(250) NOT NULL,
          PASSWORD_CANDIDAT varchar(250) NOT NULL,
          LOGIN_CANDIDAT varchar(250) NOT NULL,
          LOISIR_CANDIDAT longtext,
          OBJECTIF_CANDIDAT longtext,
          DATE_DISPONIBILITE_CANDIDAT date NOT NULL DEFAULT '0000-00-00',
          PERMIS_VOITURE_CANDIDAT char(3) NOT NULL DEFAULT 'Non',
          SITUATION_CANDIDAT varchar(250) DEFAULT NULL,
          DATE_NAISSANCE_CANDIDAT date NOT NULL DEFAULT '0000-00-00',
          NOM_CANDIDAT varchar(250) NOT NULL,
          PRENOM_CANDIDAT varchar(250) NOT NULL,
          ADRESSE_CANDIDAT varchar(250) DEFAULT NULL,
          CODE_POSTAL_CANDIDAT int(11) NOT NULL DEFAULT '0',
          VILLE_CANDIDAT varchar(250) NOT NULL,
          EMAIL_CANDIDAT varchar(250) DEFAULT NULL,
          TELEPHONE_CANDIDAT varchar(250) DEFAULT NULL,
          SITE_WEB_CANDIDAT varchar(250) DEFAULT NULL,
          TELECOPIE_CANDIDAT varchar(250) DEFAULT NULL,
          TITRE_CANDIDAT varchar(250) NOT NULL,
          MOBILE_CANDIDAT varchar(250) DEFAULT NULL,
          NB_CONSULTATION_CANDIDAT int(11) NOT NULL DEFAULT '0',
          ACTIF_CANDIDAT varchar(250) NOT NULL DEFAULT 'inactive',
          AUTRE_COMPETENCE_CANDIDAT longtext,
          TOEIC_CANDIDAT int(11) NOT NULL,
          DATE_PERMIS_CANDIDAT date NOT NULL,
          PHOTOS_CANDIDAT int(11) DEFAULT NULL,
          CV_ACTIF_CANDIDAT int(11) DEFAULT NULL,
          ID_CONTACT int(11) NOT NULL,
          created_on int(11) NOT NULL,
          updated_on int(11) NOT NULL,
          PRIMARY KEY (ID_CANDIDAT))
          ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci";

        $res_create_table = $cnx->requeteData($req_create_table);

        $str .= logtable("cv_candidat",$res_create_table);

        /*
        * Structure de la table cv_categorie
        */

        $req_create_table = "CREATE TABLE IF NOT EXISTS cv_categorie (
          ID_CATEGORIE int(11) NOT NULL AUTO_INCREMENT,
          NOM_CATEGORIE varchar(250) NOT NULL DEFAULT '',
          created_on int(11) NOT NULL,
          updated_on int(11) NOT NULL,
          PRIMARY KEY (ID_CATEGORIE)
        ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci";
        $res_create_table = $cnx->requeteData($req_create_table);

        $str .= logtable("cv_categorie",$res_create_table);

        /*
        * Structure de la table cv_city
        */

        $req_create_table = "CREATE TABLE IF NOT EXISTS cv_city (
          id_province varchar(5) COLLATE utf8_unicode_ci NOT NULL,
          id int(255) NOT NULL AUTO_INCREMENT,
          name_city varchar(255) COLLATE utf8_unicode_ci NOT NULL,
          name_city_uppercase varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
          city_slug varchar(255) COLLATE utf8_unicode_ci NOT NULL,
          cp varchar(255) COLLATE utf8_unicode_ci NOT NULL,
          latitude decimal(10,5) NOT NULL,
          longitude decimal(10,5) NOT NULL,
          PRIMARY KEY (id),
          KEY city_slug (city_slug),
          KEY cp (cp),
          KEY id_province (id_province)
        ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci";
        $res_create_table = $cnx->requeteData($req_create_table);

        $str .= logtable("cv_city",$res_create_table);

        /*
        * Structure de la table cv_competence
        */

        $req_create_table = "CREATE TABLE IF NOT EXISTS cv_competence (
          ID_COMPETENCE int(11) NOT NULL AUTO_INCREMENT,
          ID_CATEGORIE int(11) NOT NULL DEFAULT '0',
          NOM_COMPETENCE varchar(250) NOT NULL DEFAULT '',
          created_on int(11) NOT NULL,
          updated_on int(11) NOT NULL,
          PRIMARY KEY (ID_COMPETENCE)
        ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci";
        $res_create_table = $cnx->requeteData($req_create_table);
        //print_r($res_create_table);
        //echo "<br/>";
        $str .= logtable("cv_competence",$res_create_table);

        /*
        * Structure de la table cv_contact
        */

        $req_create_table = "CREATE TABLE IF NOT EXISTS cv_contact (
          ID_CONTACT int(11) NOT NULL AUTO_INCREMENT,
          TYPE_CONTACT int(11) NOT NULL DEFAULT '0',
          LOGIN_CONTACT varchar(50) NOT NULL DEFAULT 'null',
          MDP_CONTACT varchar(255) NOT NULL DEFAULT 'null',
          CIVILITE_CONTACT int(11) NOT NULL DEFAULT '0',
          PRENOM_CONTACT varchar(60) NOT NULL DEFAULT 'null',
          NOM_CONTACT varchar(60) NOT NULL DEFAULT 'null',
          FONCTION_CONTACT varchar(40) DEFAULT 'null',
          FAX_CONTACT varchar(17) DEFAULT NULL,
          GSM_CONTACT varchar(17) DEFAULT NULL,
          TEL_CONTACT varchar(17) DEFAULT NULL,
          EMAIL_CONTACT varchar(70) NOT NULL DEFAULT 'null',
          ADRESSE1_CONTACT varchar(60) NOT NULL DEFAULT 'null',
          ADRESSE2_CONTACT varchar(60) DEFAULT NULL,
          VILLE_CONTACT varchar(60) NOT NULL DEFAULT 'null',
          CP_CONTACT varchar(11) NOT NULL DEFAULT 'null',
          PAYS_CONTACT varchar(70) NOT NULL DEFAULT 'null',
          ID_CITY int(11) NOT NULL DEFAULT '0',
          signature text NOT NULL,
          last_connect int(11) NOT NULL,
          connected_from text NOT NULL,
          created_on int(11) NOT NULL,
          updated_on int(11) NOT NULL,
          PRIMARY KEY (ID_CONTACT),
          UNIQUE KEY LOGIN (LOGIN_CONTACT)
        ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci";
        $res_create_table = $cnx->requeteData($req_create_table);

        //print_r($res_create_table);

        $str .= logtable("cv_contact",$res_create_table);

        /*
        * Structure de la table cv_contacts_partenaire
        */

        $req_create_table = "CREATE TABLE IF NOT EXISTS cv_contacts_partenaire (
          ID_CONTACT_PARTENAIRE int(11) NOT NULL AUTO_INCREMENT,
          ID_CONTACT int(11) NOT NULL,
          CIVILITE_CONTACT int(11) NOT NULL DEFAULT '0',
          PRENOM_CONTACT varchar(60) NOT NULL DEFAULT 'null',
          NOM_CONTACT varchar(60) NOT NULL DEFAULT 'null',
          FONCTION_CONTACT varchar(40) DEFAULT 'null',
          FAX_CONTACT varchar(17) DEFAULT NULL,
          GSM_CONTACT varchar(17) DEFAULT NULL,
          TEL_CONTACT varchar(17) DEFAULT NULL,
          EMAIL_CONTACT varchar(70) NOT NULL DEFAULT 'null',
          ADRESSE1_CONTACT varchar(60) NOT NULL DEFAULT 'null',
          ADRESSE2_CONTACT varchar(60) DEFAULT NULL,
          ID_CITY_CONTACT int(11) NOT NULL,
          VILLE_CONTACT varchar(60) NOT NULL DEFAULT 'null',
          CP_CONTACT varchar(11) NOT NULL DEFAULT 'null',
          PAYS_CONTACT varchar(70) NOT NULL DEFAULT 'null',
          state int(1) NOT NULL DEFAULT '0',
          id_entreprise_old int(11) NOT NULL,
          id_entreprise int(11) NOT NULL,
          created_on int(11) NOT NULL,
          updated_on int(11) NOT NULL,
          PRIMARY KEY (ID_CONTACT_PARTENAIRE)
        ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci";
        $res_create_table = $cnx->requeteData($req_create_table);

        $str .= logtable("cv_contacts_partenaire",$res_create_table);

        /*
        * Structure de la table cv_country
        */

        $req_create_table = "CREATE TABLE IF NOT EXISTS cv_country (
          id int(11) NOT NULL AUTO_INCREMENT,
          name_country varchar(80) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
          name_country_uppercase varchar(255) DEFAULT NULL,
          country_slug varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
          code varchar(2) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
          PRIMARY KEY (id),
          KEY country_slug (country_slug)
        ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci";
        $res_create_table = $cnx->requeteData($req_create_table);

        $str .= logtable("cv_country",$res_create_table);

        /*
        * Structure de la table cv_data
        */

        $req_create_table = "CREATE TABLE IF NOT EXISTS cv_data (
          ID_DATA int(11) NOT NULL AUTO_INCREMENT,
          NAME_DATA varchar(30) NOT NULL,
          TYPE_DATA varchar(30) NOT NULL,
          SIZE_DATA int(11) NOT NULL,
          CONTENT_DATA mediumblob NOT NULL,
          ID_CONTACT int(11) NOT NULL,
          DATE_DATA date NOT NULL,
          NOTES_DATA varchar(255) NOT NULL,
          TYPE_DOC_DATA varchar(3) NOT NULL,
          created_on int(11) NOT NULL,
          updated_on int(11) NOT NULL,
          PRIMARY KEY (ID_DATA)
        ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci";
        $res_create_table = $cnx->requeteData($req_create_table);

        $str .= logtable("cv_data",$res_create_table);

        /*
        * Structure de la table cv_entreprise
        */

        $req_create_table = "CREATE TABLE IF NOT EXISTS cv_entreprise (
          id int(11) NOT NULL AUTO_INCREMENT,
          RAISON_SOCIALE_ENTREPRISE varchar(50) NOT NULL DEFAULT '\"null\"',
          SECTEUR_ACTIVITE_ENTREPRISE varchar(3) NOT NULL DEFAULT 'nul',
          SITE_WEB_ENTREPRISE varchar(60) NOT NULL DEFAULT '\"null\"',
          LOGO_ENTREPRISE int(11) DEFAULT NULL,
          DESCRIPTION_ENTREPRISE longtext NOT NULL,
          ID_CONTACT int(11) NOT NULL DEFAULT '0',
          created_on int(11) NOT NULL,
          updated_on int(11) NOT NULL,
          PRIMARY KEY (id)
        ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci";
        $res_create_table = $cnx->requeteData($req_create_table);

        $str .= logtable("cv_entreprise",$res_create_table);

        /*
        * Structure de la table cv_entreprises_partenaire
        */

        $req_create_table = "CREATE TABLE IF NOT EXISTS cv_entreprises_partenaire (
          id int(11) NOT NULL AUTO_INCREMENT,
          id_entreprise_ancien int(11) NOT NULL,
          RAISON_SOCIALE_ENTREPRISE varchar(50) NOT NULL DEFAULT '\"null\"',
          SECTEUR_ACTIVITE_ENTREPRISE varchar(3) NOT NULL DEFAULT 'nul',
          SITE_WEB_ENTREPRISE varchar(60) NOT NULL DEFAULT '\"null\"',
          LOGO_ENTREPRISE int(11) DEFAULT NULL,
          ID_CONTACT int(11) NOT NULL DEFAULT '0',
          adresse text NOT NULL,
          adresse2 text NOT NULL,
          cp varchar(20) NOT NULL,
          ville varchar(150) NOT NULL,
          ID_CITY_ENTREPRISE int(11) NOT NULL,
          ideffectif int(5) NOT NULL,
          naf varchar(20) NOT NULL,
          pays varchar(100) NOT NULL,
          secteur varchar(255) NOT NULL,
          origine varchar(255) NOT NULL,
          tel varchar(50) NOT NULL,
          fax varchar(50) NOT NULL,
          email varchar(200) NOT NULL,
          ta int(1) NOT NULL,
          ta_contact varchar(100) NOT NULL,
          sponsor int(1) NOT NULL,
          sponsor_contact varchar(255) NOT NULL,
          stage_fr int(1) NOT NULL,
          cont_stage_fr varchar(255) NOT NULL,
          stage_eu int(1) NOT NULL,
          cont_stage_eu varchar(255) NOT NULL,
          stage_in int(1) NOT NULL,
          cont_stage_in varchar(255) NOT NULL,
          emploi_fr int(1) NOT NULL,
          cont_emploi_fr varchar(255) NOT NULL,
          emploi_eu int(1) NOT NULL,
          cont_emploi_eu varchar(255) NOT NULL,
          emploi_in int(1) NOT NULL,
          cont_emploi_in varchar(255) NOT NULL,
          created_on int(11) NOT NULL,
          updated_on int(11) NOT NULL,
          state tinyint(1) NOT NULL,
          description text NOT NULL,
          PRIMARY KEY (id)
        ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci";
        $res_create_table = $cnx->requeteData($req_create_table);

        $str .= logtable("cv_entreprises_partenaire",$res_create_table);

        /*
        * Structure de la table cv_formation
        */

        $req_create_table = "CREATE TABLE IF NOT EXISTS cv_formation (
          ID_FORMATION int(11) NOT NULL AUTO_INCREMENT,
          INTITULE_FORMATION varchar(250) NOT NULL DEFAULT '',
          DATE_DEBUT_FORMATION date NOT NULL DEFAULT '0000-00-00',
          DATE_FIN_FORMATION date NOT NULL DEFAULT '0000-00-00',
          DIPLOME_FORMATION varchar(250) NOT NULL DEFAULT '',
          LIEU_FORMATION varchar(250) NOT NULL,
          ECOLE_FORMATION varchar(250) NOT NULL DEFAULT '',
          DESCRIPTION_FORMATION longtext NOT NULL,
          created_on int(11) NOT NULL,
          updated_on int(11) NOT NULL,
          PRIMARY KEY (ID_FORMATION)
        ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci";
        $res_create_table = $cnx->requeteData($req_create_table);

        $str .= logtable("cv_formation",$res_create_table);

        /*
        * Structure de la table cv_intitule
        */

        $req_create_table = "CREATE TABLE IF NOT EXISTS cv_intitule (
          ID_INTITULE int(11) NOT NULL AUTO_INCREMENT,
          INTITULE varchar(250) NOT NULL,
          created_on int(11) NOT NULL,
          updated_on int(11) NOT NULL,
          PRIMARY KEY (ID_INTITULE)
        ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci";
        $res_create_table = $cnx->requeteData($req_create_table);

        $str .= logtable("cv_intitule",$res_create_table);

        /*
        * Structure de la table cv_langue
        */

        $req_create_table = "CREATE TABLE IF NOT EXISTS cv_langue (
          ID_LANGUE int(11) NOT NULL AUTO_INCREMENT,
          NOM_LANGUE varchar(250) NOT NULL DEFAULT '',
          created_on int(11) NOT NULL,
          updated_on int(11) NOT NULL,
          PRIMARY KEY (ID_LANGUE)
        ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci";
        $res_create_table = $cnx->requeteData($req_create_table);

        $str .= logtable("cv_langue",$res_create_table);

        /*
        * Structure de la table cv_EXPERIENCE
        */

        $req_create_table = "CREATE TABLE IF NOT EXISTS cv_EXPERIENCE (
          ID_EXPERIENCE int(11) NOT NULL AUTO_INCREMENT,
          ID_CANDIDAT int(11) NOT NULL,
          TITRE_EXPERIENCE varchar(250) NOT NULL DEFAULT '',
          DESCRIPTION_EXPERIENCE longtext NOT NULL,
          DATE_DEBUT_EXPERIENCE date NOT NULL DEFAULT '0000-00-00',
          DATE_FIN_EXPERIENCE date NOT NULL DEFAULT '0000-00-00',
          FONCTION_EXPERIENCE varchar(250) NOT NULL DEFAULT '',
          ENTREPRISE_EXPERIENCE varchar(250) NOT NULL DEFAULT '',
          DUREE_EXPERIENCE varchar(250) NOT NULL DEFAULT '',
          COMPETENCE_UTILISEE_EXPERIENCE longtext NOT NULL,
          LIEU_EXPERIENCE varchar(250) NOT NULL,
          created_on int(11) NOT NULL,
          updated_on int(11) NOT NULL,
          PRIMARY KEY (ID_EXPERIENCE,ID_CANDIDAT)
        ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci";
        $res_create_table = $cnx->requeteData($req_create_table);

        $str .= logtable("cv_EXPERIENCE",$res_create_table);

        /*
        * Structure de la table cv_modeliser
        */

        $req_create_table = "CREATE TABLE IF NOT EXISTS cv_modeliser (
          ID_CANDIDAT int(11) NOT NULL DEFAULT '0',
          ID_RUBRIQUE int(11) NOT NULL DEFAULT '0',
          ACTIF_RUBRIQUE varchar(250) NOT NULL DEFAULT '',
          NIVEAU_RUBRIQUE int(11) NOT NULL DEFAULT '0',
          created_on int(11) NOT NULL,
          updated_on int(11) NOT NULL
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci";
        $res_create_table = $cnx->requeteData($req_create_table);

        $str .= logtable("cv_modeliser",$res_create_table);

        /*
        * Structure de la table cv_niveau_langue
        */

        $req_create_table = "CREATE TABLE IF NOT EXISTS cv_niveau_langue (
          ID_NIVEAU_LANGUE int(11) NOT NULL AUTO_INCREMENT,
          NOM_NIVEAU varchar(250) NOT NULL DEFAULT '',
          created_on int(11) NOT NULL,
          updated_on int(11) NOT NULL,
          PRIMARY KEY (ID_NIVEAU_LANGUE)
        ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci";
        $res_create_table = $cnx->requeteData($req_create_table);

        $str .= logtable("cv_niveau_langue",$res_create_table);

        /*
        * Structure de la table cv_offre
        */

        $req_create_table = "CREATE TABLE IF NOT EXISTS cv_offre (
          ID_OFFRE int(11) NOT NULL AUTO_INCREMENT,
          OFFRE_PARTENAIRE int(1) NOT NULL,
          ID_ENTREPRISE_PARTENAIRE int(11) NOT NULL,
          ID_CONTACT_PARTENAIRE int(11) NOT NULL,
          ID_CONTACT int(11) NOT NULL DEFAULT '0',
          REFERENCE_OFFRE varchar(100) DEFAULT NULL,
          INTITULE_OFFRE varchar(100) DEFAULT NULL,
          LIEU_OFFRE varchar(100) DEFAULT NULL,
          TYPE_JOB varchar(20) DEFAULT NULL,
          DATE_DEBUT_OFFRE date DEFAULT NULL,
          DESCRIPTION_OFFRE text,
          PROFIL_CANDIDAT_OFFRE longtext,
          NB_CONSULTATION_OFFRE int(11) NOT NULL DEFAULT '0',
          SALAIRE_MIN_OFFRE varchar(255) DEFAULT NULL,
          SALAIRE_MAX_OFFRE varchar(255) DEFAULT NULL,
          REGION_OFFRE varchar(250) NOT NULL DEFAULT '',
          NIVEAU_QUALIFICATION_OFFRE varchar(250) NOT NULL DEFAULT '',
          MISSION_OFFRE longtext NOT NULL,
          NOM_CONTACT varchar(255) NOT NULL,
          DATE_LIMIT_OFFRE date DEFAULT '0000-00-00',
          SECTEUR_OFFRE longtext NOT NULL,
          ENTREPRISE_OFFRE longtext NOT NULL,
          CONTACT_OFFRE longtext NOT NULL,
          SECTEUR_POSTE varchar(255) NOT NULL,
          entreprise_anonym int(1) NOT NULL,
          TYPE_OFFRE int(11) NOT NULL,
          idduree int(11) NOT NULL,
          iddeplacement int(11) NOT NULL,
          avantage varchar(255) NOT NULL,
          complement text NOT NULL,
          despossible int(1) NOT NULL,
          statut_offre int(1) NOT NULL,
          ID_CITY int(11) NOT NULL,
          created_on int(11) NOT NULL,
          updated_on int(11) NOT NULL,
          PRIMARY KEY (ID_OFFRE)
        ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci";
        $res_create_table = $cnx->requeteData($req_create_table);

        $str .= logtable("cv_offre",$res_create_table);

        /*
        * Structure de la table cv_offre_details
        */

        $req_create_table = "CREATE TABLE IF NOT EXISTS cv_offre_details (
          idoffre int(11) NOT NULL,
          idniveauetude int(5) NOT NULL,
          idlangueexigee int(5) NOT NULL,
          idlanguesouhaitee int(5) NOT NULL,
          created_on int(11) NOT NULL,
          updated_on int(11) NOT NULL
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci";
        $res_create_table = $cnx->requeteData($req_create_table);

        $str .= logtable("cv_offre_details",$res_create_table);

        /*
        * Structure de la table cv_posseder_langue
        */

        $req_create_table = "CREATE TABLE IF NOT EXISTS cv_posseder_langue (
          ID_CANDIDAT int(11) NOT NULL DEFAULT '0',
          ID_NIVEAU_LANGUE int(11) NOT NULL DEFAULT '0',
          ID_LANGUE int(11) NOT NULL DEFAULT '0',
          created_on int(11) NOT NULL,
          updated_on int(11) NOT NULL,
          PRIMARY KEY (ID_CANDIDAT,ID_NIVEAU_LANGUE,ID_LANGUE)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci";
        $res_create_table = $cnx->requeteData($req_create_table);

        $str .= logtable("cv_posseder_langue",$res_create_table);

        /*
        * Structure de la table cv_province
        */

        $req_create_table = "CREATE TABLE IF NOT EXISTS cv_province (
          id_departement int(11) NOT NULL AUTO_INCREMENT,
          id_region varchar(3) NOT NULL DEFAULT '0',
          code varchar(3) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
          name_province varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
          name_province_uppercase varchar(255) DEFAULT NULL,
          province_slug varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
          PRIMARY KEY (id_departement),
          KEY province_slug (province_slug),
          KEY id_region (id_region)
        ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci";
        $res_create_table = $cnx->requeteData($req_create_table);

        $str .= logtable("cv_province",$res_create_table);

        /*
        * Structure de la table cv_realiser_experience
        */

        $req_create_table = "CREATE TABLE IF NOT EXISTS cv_realiser_experience (
          ID_CANDIDAT int(11) NOT NULL DEFAULT '0',
          ID_EXPERIENCE int(11) NOT NULL DEFAULT '0',
          created_on int(11) NOT NULL,
          updated_on int(11) NOT NULL,
          PRIMARY KEY (ID_CANDIDAT,ID_EXPERIENCE)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci";
        $res_create_table = $cnx->requeteData($req_create_table);

        $str .= logtable("cv_realiser_experience",$res_create_table);

        /*
        * Structure de la table cv_reference
        */

        $req_create_table = "CREATE TABLE IF NOT EXISTS cv_reference (
          ID_REFERENCE int(11) NOT NULL AUTO_INCREMENT,
          PREFIXE varchar(3) NOT NULL,
          REFERENCE int(11) NOT NULL,
          created_on int(11) NOT NULL,
          updated_on int(11) NOT NULL,
          PRIMARY KEY (ID_REFERENCE)
        ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Table des references'";
        $res_create_table = $cnx->requeteData($req_create_table);

        $str .= logtable("cv_reference",$res_create_table);

        /*
        * Structure de la table cv_rubrique
        */

        $req_create_table = "CREATE TABLE IF NOT EXISTS cv_rubrique (
          ID_RUBRIQUE int(11) NOT NULL AUTO_INCREMENT,
          NOM_RUBRIQUE varchar(250) NOT NULL DEFAULT '',
          created_on int(11) NOT NULL,
          updated_on int(11) NOT NULL,
          PRIMARY KEY (ID_RUBRIQUE)
        ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci";
        $res_create_table = $cnx->requeteData($req_create_table);

        $str .= logtable("cv_rubrique",$res_create_table);

        /*
        * Structure de la table cv_secteur
        */

        $req_create_table = "CREATE TABLE IF NOT EXISTS cv_secteur (
          ID_SECTEUR int(11) NOT NULL AUTO_INCREMENT,
          SECTEUR varchar(250) NOT NULL,
          CODENAF varchar(4) NOT NULL,
          created_on int(11) NOT NULL,
          updated_on int(11) NOT NULL,
          PRIMARY KEY (ID_SECTEUR)
        ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci";
        $res_create_table = $cnx->requeteData($req_create_table);

        $str .= logtable("cv_secteur",$res_create_table);

        /*
        * Structure de la table cv_state
        */

        $req_create_table = "CREATE TABLE IF NOT EXISTS cv_state (
          id_region int(11) NOT NULL AUTO_INCREMENT,
          name_state varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
          name_state_uppercase varchar(255) DEFAULT NULL,
          state_slug varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
          id_country int(11) NOT NULL,
          state_code varchar(255) DEFAULT NULL,
          PRIMARY KEY (id_region),
          KEY state_slug (state_slug),
          KEY id_country (id_country)
        ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci";
        $res_create_table = $cnx->requeteData($req_create_table);

        $str .= logtable("cv_state",$res_create_table);

        /*
        * Structure de la table cv_statistiques
        */

        $req_create_table = "CREATE TABLE IF NOT EXISTS cv_statistiques (
          ID_STATS int(11) NOT NULL AUTO_INCREMENT,
          ID_CANDIDAT int(11) NOT NULL,
          ID_OFFRE int(11) NOT NULL,
          ID_CV int(11) default NULL,
          ID_CONTACT int(11) default NULL,
          adresse_ip varchar(128) character set utf8 collate utf8_unicode_ci NOT NULL,
          referrer varchar(255) character set utf8 collate utf8_unicode_ci NOT NULL,
          created_on int(11) NOT NULL,
          updated_on int(11) NOT NULL,
          PRIMARY KEY (ID_STATS)
        ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci";
        $res_create_table = $cnx->requeteData($req_create_table);

        $str .= logtable("cv_statistiques",$res_create_table);

        /*
        * Structure de la table cv_suivre_formation
        */

        $req_create_table = "CREATE TABLE IF NOT EXISTS cv_suivre_formation (
          ID_CANDIDAT int(11) NOT NULL DEFAULT '0',
          ID_FORMATION int(11) NOT NULL DEFAULT '0',
          created_on int(11) NOT NULL,
          updated_on int(11) NOT NULL,
          PRIMARY KEY (ID_CANDIDAT,ID_FORMATION)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci";
        $res_create_table = $cnx->requeteData($req_create_table);

        $str .= logtable("cv_suivre_formation",$res_create_table);

        /*
        * Structure de la table light_forum_tbl
        */

        $req_create_table = "CREATE TABLE IF NOT EXISTS light_forum_tbl (
          id int(11) NOT NULL AUTO_INCREMENT,
          nom varchar(255) NOT NULL DEFAULT '',
          email varchar(255) NOT NULL DEFAULT '',
          date_verif datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
          date varchar(10) NOT NULL DEFAULT '',
          heure varchar(5) NOT NULL DEFAULT '',
          texte text NOT NULL,
          reponse_a_id int(11) NOT NULL DEFAULT '0',
          addr varchar(255) NOT NULL DEFAULT '',
          lect int(11) NOT NULL DEFAULT '0',
          titre varchar(255) NOT NULL DEFAULT '- no title -',
          PRIMARY KEY (id),
          UNIQUE KEY id_2 (id),
          KEY id (id)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci";
        $res_create_table = $cnx->requeteData($req_create_table);

        $str .= logtable("light_forum_tbl",$res_create_table);

        /*
        * Structure de la vue cv_localisation
        */

        $req_create_table = "CREATE VIEW cv_localisation AS select
            cv_city.name_city AS name_city,cv_state.name_state AS name_state,
            cv_country.name_country AS name_country,
            cv_province.name_province AS name_province,cv_city.cp AS cp,
            cv_city.id AS id_city from (((cv_city join cv_province) join cv_state)
            join cv_country) where ((cv_city.id_province = cv_province.code) and
            (cv_province.id_region = cv_state.state_code) and
            (cv_state.id_country = cv_country.id))";
        $res_create_table = $cnx->requeteData($req_create_table);

        $str .= logtable("cv_localisation",$res_create_table);

        /*
        * Structure de la vue cv_partenaires
        */

        $req_create_table = "CREATE VIEW cv_partenaires AS (select
            cv_contacts_partenaire.ID_CONTACT AS proprietaire_contact,
            cv_entreprises_partenaire.ID_CONTACT AS proprietaire_entreprise,
            cv_contacts_partenaire.ID_CONTACT_PARTENAIRE AS id_contact_partenaire,
            cv_contacts_partenaire.CIVILITE_CONTACT AS civilite_contact,
            cv_contacts_partenaire.PRENOM_CONTACT AS prenom_contact,
            cv_contacts_partenaire.NOM_CONTACT AS nom_contact,
            cv_contacts_partenaire.FONCTION_CONTACT AS fonction_contact,
            cv_contacts_partenaire.FAX_CONTACT AS fax_contact,
            cv_contacts_partenaire.GSM_CONTACT AS gsm_contact,
            cv_contacts_partenaire.TEL_CONTACT AS tel_contact,
            cv_contacts_partenaire.EMAIL_CONTACT AS email_contact,
            cv_contacts_partenaire.ADRESSE1_CONTACT AS adresse1_contact,
            cv_contacts_partenaire.ADRESSE2_CONTACT AS adresse2_contact,
            cv_contacts_partenaire.VILLE_CONTACT AS ville_contact,
            cv_contacts_partenaire.CP_CONTACT AS cp_contact,
            cv_contacts_partenaire.PAYS_CONTACT AS pays_contact,
            cv_contacts_partenaire.ID_CITY_CONTACT AS id_city_contact,
            cv_contacts_partenaire.id_entreprise AS id_entreprise,
            cv_entreprises_partenaire.id AS id,
            cv_entreprises_partenaire.RAISON_SOCIALE_ENTREPRISE AS raison_sociale_entreprise,
            cv_entreprises_partenaire.SECTEUR_ACTIVITE_ENTREPRISE AS secteur_activite_entreprise,
            cv_entreprises_partenaire.ideffectif AS ideffectif,
            cv_entreprises_partenaire.description AS description,
            cv_entreprises_partenaire.adresse AS adresse,
            cv_entreprises_partenaire.adresse2 AS adresse2,
            cv_entreprises_partenaire.cp AS cp,cv_entreprises_partenaire.ville AS ville,
            cv_entreprises_partenaire.ID_CITY_ENTREPRISE AS id_city_entreprise,
            cv_entreprises_partenaire.tel AS tel,cv_entreprises_partenaire.fax AS fax,
            cv_entreprises_partenaire.email AS email from (cv_contacts_partenaire left
            join cv_entreprises_partenaire on((cv_entreprises_partenaire.id =
            cv_contacts_partenaire.id_entreprise))) where
            ((cv_entreprises_partenaire.state <> 2) and
            (cv_contacts_partenaire.state <> 2))) union
            (select cv_contacts_partenaire.ID_CONTACT AS proprietaire_contact,
            cv_entreprises_partenaire.ID_CONTACT AS proprietaire_entreprise,
            cv_contacts_partenaire.ID_CONTACT_PARTENAIRE AS id_contact_partenaire,
            cv_contacts_partenaire.CIVILITE_CONTACT AS civilite_contact,
            cv_contacts_partenaire.PRENOM_CONTACT AS prenom_contact,
            cv_contacts_partenaire.NOM_CONTACT AS nom_contact,
            cv_contacts_partenaire.FONCTION_CONTACT AS fonction_contact,
            cv_contacts_partenaire.FAX_CONTACT AS fax_contact,
            cv_contacts_partenaire.GSM_CONTACT AS gsm_contact,
            cv_contacts_partenaire.TEL_CONTACT AS tel_contact,
            cv_contacts_partenaire.EMAIL_CONTACT AS email_contact,
            cv_contacts_partenaire.ADRESSE1_CONTACT AS adresse1_contact,
            cv_contacts_partenaire.ADRESSE2_CONTACT AS adresse2_contact,
            cv_contacts_partenaire.VILLE_CONTACT AS ville_contact,
            cv_contacts_partenaire.CP_CONTACT AS cp_contact,
            cv_contacts_partenaire.PAYS_CONTACT AS pays_contact,
            cv_contacts_partenaire.ID_CITY_CONTACT AS id_city_contact,
            cv_contacts_partenaire.id_entreprise AS id_entreprise,
            cv_entreprises_partenaire.id AS id,
            cv_entreprises_partenaire.RAISON_SOCIALE_ENTREPRISE AS
            raison_sociale_entreprise,
            cv_entreprises_partenaire.SECTEUR_ACTIVITE_ENTREPRISE AS
            secteur_activite_entreprise,cv_entreprises_partenaire.ideffectif AS
            ideffectif,cv_entreprises_partenaire.description AS description,
            cv_entreprises_partenaire.adresse AS adresse,
            cv_entreprises_partenaire.adresse2 AS adresse2,
            cv_entreprises_partenaire.cp AS cp,cv_entreprises_partenaire.ville AS ville,
            cv_entreprises_partenaire.ID_CITY_ENTREPRISE AS id_city_entreprise,
            cv_entreprises_partenaire.tel AS tel,cv_entreprises_partenaire.fax AS fax,
            cv_entreprises_partenaire.email AS email from (cv_entreprises_partenaire
            left join cv_contacts_partenaire on((
            cv_entreprises_partenaire.id = cv_contacts_partenaire.id_entreprise))) where
            ((cv_entreprises_partenaire.state <> 2) AND
            (cv_contacts_partenaire.state <> 2))) ORDER BY raison_sociale_entreprise";
        $res_create_table = $cnx->requeteData($req_create_table);

        $str .= logtable("cv_partenaires",$res_create_table);

        /*
        * Contraintes pour les tables exportées
        */

        /*
        * Contraintes pour la table cv_candidat
        */

        $req_create_table = "ALTER TABLE cv_candidat ADD CONSTRAINT fk_cv_actif
            FOREIGN KEY (CV_ACTIF_CANDIDAT) REFERENCES cv_data (ID_DATA)";
        $res_create_table = $cnx->requeteData($req_create_table);

        $str .= logtable("Contrainte",$res_create_table);

        /*
         * Génération du mot de passe
         */

        $mdp = '';
        $tmpmdp = str_split(str_shuffle(time().strrev(time())) ,2);

        foreach($tmpmdp as $car){
            if(($car < 47 || $car>91)){
                $car = rand(97, 122);
            }
            $mdp .= chr($car);
        }

        $req_insert_admin = "INSERT INTO cv_contact (type_contact,login_contact,";
        $req_insert_admin .= "mdp_contact) VALUE (3,'".$mailadmin."','".md5($mdp)."')";

        $res_insert_admin = $cnx->requeteData($req_insert_admin);

        $str .= logtable("Création compte admin : ",$res_insert_admin);

        $str .= "<tr><td> Mot de passe admin</td><td><b>".utf8_encode($mdp)."<b/></td></tr>";
        $str .= "</tbody></table>";
        $etape++;
        $lblbouton = "Continuer";

        break;
    case 2:
        $str .= "Installation terminée :D <br/>Vous pouvez maintenant supprimer ";
        $str .= "le fichier install.php. Ensuite cliquez sur le bouton terminer.";
        $etape++;
        $lblbouton = "Terminer";
        break;

    default :
        $str .= $form->creer_text('mail-admin', 'Mail administrateur', 'yes', 'text', '', '','email','','',50);
        $etape++;
        $lblbouton = "Installer";
        break;
}

$str .= $form->creer_text('etape', 'etape', 'yes', 'hidden', $etape, '', '');
$str .= $form->creer_bouton($lblbouton, 'btinstall', 'submit');
$str .= $form->form_end();
$str .= "<div/>";

echo $str;

?>
