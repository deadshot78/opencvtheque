USE `temp_cvtheque`;

-- CREER LA TABLE cv_contacts_partenaire
-- CREER LA TABLE cv_entreprises_partenaire
-- --------------------------------------------------------

--
-- modifications sur la table `cv_etudiant` --> `cv_candidat`
--

ALTER TABLE `cv_etudiant` RENAME TO `cv_candidat`;

ALTER TABLE `cv_candidat` CHANGE COLUMN `ID_ETUDIANT` `ID_CANDIDAT` INTEGER  NOT NULL DEFAULT NULL AUTO_INCREMENT,
 CHANGE COLUMN `NB_ANNEE_EXPERIENCE_ETUDIANT` `NB_ANNEE_EXPERIENCE_CANDIDAT` VARCHAR(250)  CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT 0,
 CHANGE COLUMN `DATE_CREATION_ETUDIANT` `DATE_CREATION_CANDIDAT` DATE  NOT NULL DEFAULT '0000-00-00',
 CHANGE COLUMN `DATE_MODIFICATION_ETUDIANT` `DATE_MODIFICATION_CANDIDAT` DATE  NOT NULL DEFAULT '0000-00-00',
 CHANGE COLUMN `CIVILITE_ETUDIANT` `CIVILITE_CANDIDAT` VARCHAR(250)  CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
 CHANGE COLUMN `NIVEAU_ETUDES_ETUDIANT` `NIVEAU_ETUDES_CANDIDAT` VARCHAR(250)  CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
 CHANGE COLUMN `PASSWORD_ETUDIANT` `PASSWORD_CANDIDAT` VARCHAR(250)  CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
 CHANGE COLUMN `LOGIN_ETUDIANT` `LOGIN_CANDIDAT` VARCHAR(250)  CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
 CHANGE COLUMN `LOISIR_ETUDIANT` `LOISIR_CANDIDAT` LONGTEXT  CHARACTER SET latin1 COLLATE latin1_swedish_ci,
 CHANGE COLUMN `OBJECTIF_ETUDIANT` `OBJECTIF_CANDIDAT` LONGTEXT  CHARACTER SET latin1 COLLATE latin1_swedish_ci,
 CHANGE COLUMN `DATE_DISPONIBILITE_ETUDIANT` `DATE_DISPONIBILITE_CANDIDAT` DATE  NOT NULL DEFAULT '0000-00-00',
 CHANGE COLUMN `PERMIS_VOITURE_ETUDIANT` `PERMIS_VOITURE_CANDIDAT` CHAR(3)  CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT 'Non',
 CHANGE COLUMN `SITUATION_ETUDIANT` `SITUATION_CANDIDAT` VARCHAR(250)  CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
 CHANGE COLUMN `DATE_NAISSANCE_ETUDIANT` `DATE_NAISSANCE_CANDIDAT` DATE  NOT NULL DEFAULT '0000-00-00',
 CHANGE COLUMN `NOM_ETUDIANT` `NOM_CANDIDAT` VARCHAR(250)  CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
 CHANGE COLUMN `PRENOM_ETUDIANT` `PRENOM_CANDIDAT` VARCHAR(250)  CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
 CHANGE COLUMN `ADRESSE_ETUDIANT` `ADRESSE_CANDIDAT` VARCHAR(250)  CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
 CHANGE COLUMN `CODE_POSTAL_ETUDIANT` `CODE_POSTAL_CANDIDAT` INTEGER  NOT NULL DEFAULT 0,
 CHANGE COLUMN `VILLE_ETUDIANT` `VILLE_CANDIDAT` VARCHAR(250)  CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
 CHANGE COLUMN `EMAIL_ETUDIANT` `EMAIL_CANDIDAT` VARCHAR(250)  CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
 CHANGE COLUMN `TELEPHONE_ETUDIANT` `TELEPHONE_CANDIDAT` VARCHAR(250)  CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
 CHANGE COLUMN `SITE_WEB_ETUDIANT` `SITE_WEB_CANDIDAT` VARCHAR(250)  CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
 CHANGE COLUMN `TELECOPIE_ETUDIANT` `TELECOPIE_CANDIDAT` VARCHAR(250)  CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
 CHANGE COLUMN `TITRE_ETUDIANT` `TITRE_CANDIDAT` VARCHAR(250)  CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
 CHANGE COLUMN `MOBILE_ETUDIANT` `MOBILE_CANDIDAT` VARCHAR(250)  CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
 CHANGE COLUMN `NB_CONSULTATION_ETUDIANT` `NB_CONSULTATION_CANDIDAT` INTEGER  NOT NULL DEFAULT 0,
 CHANGE COLUMN `ACTIF_ETUDIANT` `ACTIF_CANDIDAT` VARCHAR(250)  CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT 'inactive',
 CHANGE COLUMN `AUTRE_COMPETENCE` `AUTRE_COMPETENCE_CANDIDAT` LONGTEXT  CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
 CHANGE COLUMN `TOEIC` `TOEIC_CANDIDAT` INTEGER  NOT NULL,
 CHANGE COLUMN `DATE_PERMIS_ETUDIANT` `DATE_PERMIS_CANDIDAT` DATE  NOT NULL,
 CHANGE COLUMN `PHOTOS_ETUDIANT` `PHOTOS_CANDIDAT` INTEGER DEFAULT NULL,
 ADD COLUMN `CV_ACTIF_CANDIDAT` int(11)  DEFAULT NULL AFTER `PHOTOS_CANDIDAT`,
 ADD COLUMN `ID_CONTACT` int(11)  NOT NULL AFTER `CV_ACTIF_CANDIDAT`,
 DROP PRIMARY KEY,
 ADD PRIMARY KEY  USING BTREE(`ID_CANDIDAT`);

-- --------------------------------------------------------

--
-- creation de la table `cv_entreprise`
--

CREATE TABLE IF NOT EXISTS `cv_entreprise` (
  `ID_ENTREPRISE` int(11) NOT NULL auto_increment,
  `RAISON_SOCIALE_ENTREPRISE` varchar(50) NOT NULL default '"null"',
  `SECTEUR_ACTIVITE_ENTREPRISE` varchar(3) NOT NULL default 'nul',
  `SITE_WEB_ENTREPRISE` varchar(60) NOT NULL default '"null"',
  `LOGO_ENTREPRISE` int(11) default NULL,
  `TAILLE_ENTREPRISE` varchar(10) default NULL,
  `ID_CONTACT` int(11) NOT NULL default '0',
  PRIMARY KEY  (`ID_ENTREPRISE`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- modifications de la table `cv_contact`
--

ALTER TABLE `cv_contact` CHANGE COLUMN `PASSWORD_CONTACT` `MDP_CONTACT` VARCHAR(255)  CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT 'null',
 CHANGE COLUMN `CIVILITE_CONTACT` `CIVILITE_CONTACT_OLD` VARCHAR(20)  CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
 MODIFY COLUMN `PRENOM_CONTACT` VARCHAR(60)  CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT 'null',
 MODIFY COLUMN `NOM_CONTACT` VARCHAR(60)  CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT 'null',
 MODIFY COLUMN `FONCTION_CONTACT` VARCHAR(40)  CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
 CHANGE COLUMN `PORTABLE_CONTACT` `GSM_CONTACT` VARCHAR(17)  CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
 CHANGE COLUMN `TELEPHONE_CONTACT` `TEL_CONTACT` VARCHAR(17)  CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
 MODIFY COLUMN `EMAIL_CONTACT` VARCHAR(70)  CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT 'null',
 ADD COLUMN `TYPE_CONTACT` int(11)  NOT NULL DEFAULT 0 AFTER `ID_CONTACT`,
 ADD COLUMN `FAX_CONTACT` VARCHAR(17)  DEFAULT NULL AFTER `FONCTION_CONTACT`,
 ADD COLUMN `ADRESSE1_CONTACT` VARCHAR(60)  NOT NULL DEFAULT 'null' AFTER `EMAIL_CONTACT`,
 ADD COLUMN `ADRESSE2_CONTACT` VARCHAR(60)  DEFAULT NULL AFTER `ADRESSE1_CONTACT`,
 ADD COLUMN `VILLE_CONTACT` varchar(60)  NOT NULL DEFAULT 'null' AFTER `ADRESSE2_CONTACT`,
 ADD COLUMN `CP_CONTACT` VARCHAR(11)  NOT NULL DEFAULT 'null' AFTER `VILLE_CONTACT`,
 ADD COLUMN `PAYS_CONTACT` VARCHAR(70)  NOT NULL DEFAULT 'null' AFTER `CP_CONTACT`,
 ADD COLUMN `ID_CITY` INTEGER  NOT NULL DEFAULT 0 AFTER `PAYS_CONTACT`;

-- --------------------------------------------------------

--
-- Structure de la table `cv_city`
--

CREATE TABLE IF NOT EXISTS `cv_city` (
  `id_province` varchar(5) collate utf8_unicode_ci NOT NULL,
  `id` int(255) NOT NULL auto_increment,
  `name_city` varchar(255) collate utf8_unicode_ci NOT NULL,
  `name_city_uppercase` varchar(255) collate utf8_unicode_ci default NULL,
  `city_slug` varchar(255) collate utf8_unicode_ci NOT NULL,
  `cp` varchar(255) collate utf8_unicode_ci NOT NULL,
  `latitude` decimal(10,5) NOT NULL,
  `longitude` decimal(10,5) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `city_slug` (`city_slug`),
  KEY `cp` (`cp`),
  KEY `id_province` (`id_province`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `cv_country`
--

CREATE TABLE IF NOT EXISTS `cv_country` (
  `id` int(11) NOT NULL auto_increment,
  `name_country` varchar(80) character set utf8 collate utf8_unicode_ci NOT NULL,
  `name_country_uppercase` varchar(255) default NULL,
  `country_slug` varchar(255) character set utf8 collate utf8_unicode_ci NOT NULL,
  `code` varchar(2) character set utf8 collate utf8_unicode_ci NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `country_slug` (`country_slug`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `cv_province`
--

CREATE TABLE IF NOT EXISTS `cv_province` (
  `id_departement` int(11) NOT NULL auto_increment,
  `id_region` varchar(3) character set utf8 collate utf8_unicode_ci NOT NULL,
  `code` varchar(3) character set utf8 collate utf8_unicode_ci NOT NULL,
  `name_province` varchar(250) character set utf8 collate utf8_unicode_ci NOT NULL,
  `name_province_uppercase` varchar(255) default NULL,
  `province_slug` varchar(255) character set utf8 collate utf8_unicode_ci NOT NULL,
  PRIMARY KEY  (`id_departement`),
  KEY `province_slug` (`province_slug`),
  KEY `id_region` (`id_region`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `cv_state`
--

CREATE TABLE IF NOT EXISTS `cv_state` (
  `id_region` int(11) NOT NULL auto_increment,
  `name_state` varchar(250) character set utf8 collate utf8_unicode_ci NOT NULL,
  `name_state_uppercase` varchar(255) default NULL,
  `state_slug` varchar(255) character set utf8 collate utf8_unicode_ci NOT NULL,
  `id_country` int(11) NOT NULL,
  `state_code` varchar(255) default NULL,
  PRIMARY KEY  (`id_region`),
  KEY `state_slug` (`state_slug`),
  KEY `id_country` (`id_country`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;


--
-- Modifications de la table `cv_statistiques`
--

UPDATE cv_statistiques,cv_candidat SET cv_statistiques.id_contact = cv_candidat.id_contact
WHERE cv_candidat.id_candidat=cv_statistiques.id_etudiant ;

ALTER TABLE `cv_statistiques` DROP `ID_ETUDIANT` ;
ALTER TABLE `cv_statistiques` CHANGE `created_on` `created_on` DATETIME NOT NULL ,
CHANGE `updated_on` `updated_on` DATETIME NOT NULL ;

UPDATE cv_statistiques SET created_on = date_stat ;

ALTER TABLE `cv_statistiques` DROP `DATE_STAT` ;
ALTER TABLE `cv_statistiques` CHANGE `ID_CONTACT` `ID_CONTACT` INT( 11 ) NULL DEFAULT NULL ;
ALTER TABLE `cv_statistiques` CHANGE `ORIGINE_STAT` `ORIGINE_STAT` VARCHAR( 20 )
CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL ;
ALTER TABLE `cv_statistiques` CHANGE `ID_OFFRE` `ID_OFFRE` INT( 11 ) NULL DEFAULT NULL ;
ALTER TABLE `cv_statistiques` ADD `ID_CV` INT NULL DEFAULT NULL AFTER `ID_OFFRE` ;
ALTER TABLE `cv_statistiques` DROP `ORIGINE_STAT` ;
ALTER TABLE `cv_statistiques` ADD `adresse_ip` VARCHAR( 128 ) CHARACTER
SET utf8 COLLATE utf8_unicode_ci NOT NULL AFTER `ID_CONTACT` ,
ADD `referrer` VARCHAR( 255 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci
NOT NULL AFTER `adresse_ip` ;


-- --------------------------------------------------------

--
-- Modifications de la table `cv_offre`
--

ALTER TABLE `cv_offre` ADD COLUMN `ID_ENTREPRISE` int(11)  NOT NULL DEFAULT 0 AFTER `ID_OFFRE`,
 ADD COLUMN `STATUT_OFFRE` int(11)  NOT NULL DEFAULT 0 AFTER `SECTEUR_POSTE`,
 ADD COLUMN `ID_CITY` INT(11) NOT NULL DEFAULT 0 AFTER `statut_offre`,
 CHANGE `TYPE_OFFRE` `TYPE_JOB` VARCHAR( 20 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
 ADD COLUMN `TYPE_OFFRE` INT(11) NOT NULL DEFAULT 0 AFTER `ID_CITY`,
ADD COLUMN `created_on` INT(11) NOT NULL DEFAULT 0 AFTER `TYPE_OFFRE`
ADD COLUMN `updated_on` INT(11) NOT NULL DEFAULT 0 AFTER `created_on`;


--

--
-- Migration des contacts
--
-- 1/ les contact entreprise ==> type 2
-- 2/ les candidats ==> type 1


UPDATE `cv_contact` set type_contact=2;

INSERT INTO `cv_entreprise` (raison_sociale_entreprise,secteur_activite_entreprise,site_web_entreprise,
taille_entreprise,id_contact) SELECT nom_entreprise,activite_entreprise,site_web_entreprise,
taille_entreprise,cv_contact.id_contact FROM `cv_contact` WHERE type_contact=2;

UPDATE `cv_contact` set cp_contact=code_postal_entreprise, adresse1_contact=adresse_entreprise, ville_contact=ville_entreprise WHERE type_contact=2;

ALTER TABLE `cv_contact` DROP COLUMN `NOM_ENTREPRISE`,
 DROP COLUMN `ADRESSE_ENTREPRISE`,
 DROP COLUMN `VILLE_ENTREPRISE`,
 DROP COLUMN `CODE_POSTAL_ENTREPRISE`,
 DROP COLUMN `ACTIVITE_ENTREPRISE`,
 DROP COLUMN `TAILLE_ENTREPRISE`,
 DROP COLUMN `SITE_WEB_ENTREPRISE`;

INSERT INTO `cv_contact` (type_contact,login_contact,mdp_contact,civilite_contact,nom_contact,
prenom_contact,tel_contact,adresse1_contact,ville_contact,cp_contact,email_contact)
SELECT '1',login_candidat,password_candidat,civilite_candidat,nom_candidat,prenom_candidat,
telephone_candidat,adresse_candidat,ville_candidat,code_postal_candidat,email_candidat FROM `cv_candidat`
WHERE login_candidat != ''

UPDATE `cv_candidat`,`cv_contact` SET `cv_candidat`.id_contact=cv_contact.id_contact WHERE cv_contact.type_contact=1 AND login_contact=login_candidat;

-- Mise à jour de l'id de la ville sur les contacts
UPDATE cv_contact,cv_city SET cv_contact.id_city=cv_city.id WHERE cv_city.cp LIKE concat('%',cp_contact,'%') AND cv_city.name_city LIKE concat('%',LEFT(ville_contact,3),'%') AND cp_contact != ''
-- update cv_offre set statut_offre=2 where date_limit_offre!='0000-00-00';

-- Mise à jour du type offre
UPDATE cv_offre,cfg_type_offre SET cv_offre.idtypeoffre=cfg_type_offre.id WHERE type_contrat_offre LIKE cfg_type_offre.nom

-- Suppression de la colonne type_contrat_offre devenue inutile
ALTER TABLE `cv_offre` DROP COLUMN `type_contrat_offre`;

UPDATE `cv_offre` SET created_on=UNIX_TIMESTAMP(date_depot_offre) WHERE created_on=0;
ALTER TABLE `cv_offre` DROP COLUMN `date_depot_offre`;

-- Mise à jour table cv_realiser_experience id_candidat --> id_contact
ALTER TABLE `cv_realiser_experience` ADD `ID_CONTACT` INT NOT NULL FIRST ;
DELETE FROM `cv_realiser_experience` WHERE ID_CANDIDAT=0;
UPDATE cv_realiser_experience,cv_candidat SET cv_realiser_experience.id_contact=cv_candidat.id_contact WHERE cv_realiser_experience.id_candidat=cv_candidat.id_candidat;
ALTER TABLE `cvtheque`.`cv_realiser_experience` DROP PRIMARY KEY ,
ADD PRIMARY KEY ( `ID_CONTACT` , `ID_EXPERIENCE` );
ALTER TABLE `cv_realiser_experience` DROP `ID_CANDIDAT`;

-- Mise à jour table cv_suivre_formation id_candidat --> id_contact
ALTER TABLE `cv_suivre_formation` ADD `ID_CONTACT` INT NOT NULL FIRST ;
DELETE FROM `cv_suivre_formation` WHERE ID_CANDIDAT=0;
UPDATE cv_suivre_formation,cv_candidat SET cv_suivre_formation.id_contact=cv_candidat.id_contact WHERE cv_suivre_formation.id_candidat=cv_candidat.id_candidat;
ALTER TABLE `cvtheque`.`cv_suivre_formation` DROP PRIMARY KEY ,
ADD PRIMARY KEY ( `ID_CONTACT` , `ID_FORMATION` );
ALTER TABLE `cv_suivre_formation` DROP `ID_CANDIDAT`;

-- Mise à jour table cv_acquerir_competence id_candidat --> id_contact
ALTER TABLE `cv_acquerir_competence` ADD `ID_CONTACT` INT NOT NULL FIRST ;
DELETE FROM `cv_acquerir_competence` WHERE ID_CANDIDAT=0;
UPDATE cv_acquerir_competence,cv_candidat SET cv_acquerir_competence.id_contact=cv_candidat.id_contact WHERE cv_acquerir_competence.id_candidat=cv_candidat.id_candidat;
ALTER TABLE `cvtheque`.`cv_acquerir_competence` DROP PRIMARY KEY ,
ADD PRIMARY KEY ( `ID_CONTACT` , `ID_COMPETENCE` );
ALTER TABLE `cv_acquerir_competence` DROP `ID_CANDIDAT`;


--Creation de la vue partenaires
CREATE VIEW cv_partenaires AS SELECT ID_CONTACT_PARTENAIRE, CIVILITE_CONTACT, PRENOM_CONTACT, NOM_CONTACT, FONCTION_CONTACT,
FAX_CONTACT, GSM_CONTACT, TEL_CONTACT, EMAIL_CONTACT, ADRESSE1_CONTACT, ADRESSE2_CONTACT, VILLE_CONTACT, CP_CONTACT, PAYS_CONTACT,
id_entreprise, entreprise_temp, id, id_entreprise_ancien, RAISON_SOCIALE_ENTREPRISE, SECTEUR_ACTIVITE_ENTREPRISE,
SITE_WEB_ENTREPRISE, LOGO_ENTREPRISE, cv_contacts_partenaire.ID_CONTACT, adresse, adresse2, cp, ville, effectif, ideffectif,
naf, pays, secteur, origine, tel, fax, email, ta, ta_contact, sponsor, sponsor_contact, stage_fr, cont_stage_fr, stage_eu,
cont_stage_eu, stage_in, cont_stage_in, emploi_fr, cont_emploi_fr, emploi_eu, cont_emploi_eu, emploi_in, cont_emploi_in,
created_on, update_on, actif, description
FROM cv_entreprises_partenaire
LEFT JOIN cv_contacts_partenaire ON cv_contacts_partenaire.id_entreprise = cv_entreprises_partenaire.id;

-- Creation de la vue localisation
CREATE VIEW `cv_localisation` AS select `cv_city`.`name_city` AS `name_city`,`cv_state`.`name_state` AS `name_state`,
`cv_country`.`name_country` AS `name_country`,`cv_province`.`name_province` AS `name_province`,`cv_city`.`cp` AS `cp`,
`cv_city`.`id` AS `id_city` from (((`cv_city` join `cv_province`) join `cv_state`) join `cv_country`) where
((`cv_city`.`id_province` = `cv_province`.`code`) and (`cv_province`.`id_region` = `cv_state`.`state_code`) and
(`cv_state`.`id_country` = `cv_country`.`id`));


